mod async_cache;
mod buck_query;
mod call_hierarchy;
mod code_actions;
mod code_lens;
mod commands;
mod completions;
mod config;
mod cpp_file;
mod debounce;
mod diagnostics;
mod document;
mod file_cache;
mod formatting;
mod include_profiling;
mod include_resolver;
mod indexer;
mod language_server;
mod progress;
mod reply_buffer;
mod semantic_tokens;
mod span;
mod symbols;
mod task_tracker;
mod text_util;
mod thrift_file;
mod utils;
mod workspace;

use anyhow::Result;
use lsp_server::Connection;

use crate::language_server::LanguageServer;

#[macro_use]
extern crate pest_derive;

#[tokio::main]
async fn main() -> Result<()> {
    pretty_env_logger::init_timed();
    log::info!("starting server");

    let (connection, io_threads) = Connection::stdio();

    // Run the server and wait for the two threads to end (typically by trigger LSP Exit event).
    let server = LanguageServer::init(connection);
    server.main_loop().await?;
    io_threads.join().unwrap();

    Ok(())
}
