use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc,
};

use tokio::sync::Notify;

pub struct TaskTracker {
    outstanding_tasks: AtomicUsize,
    signal: Notify,
}

pub struct TaskTrackerGuard {
    tracker: Arc<TaskTracker>,
}

impl Drop for TaskTrackerGuard {
    fn drop(&mut self) {
        let old_rc = self
            .tracker
            .outstanding_tasks
            .fetch_sub(1, Ordering::SeqCst);
        if old_rc == 1 {
            self.tracker.signal.notify_one()
        }
    }
}

impl TaskTracker {
    pub fn new() -> Arc<Self> {
        Arc::new(TaskTracker {
            outstanding_tasks: AtomicUsize::new(0),
            signal: Notify::new(),
        })
    }

    pub async fn wait(&self) {
        self.signal.notified().await
    }

    pub fn start_task(self: &Arc<Self>) -> TaskTrackerGuard {
        self.outstanding_tasks.fetch_add(1, Ordering::SeqCst);
        TaskTrackerGuard {
            tracker: self.clone(),
        }
    }
}
