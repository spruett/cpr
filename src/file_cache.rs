use crate::span::SourceFile;
use anyhow::Result;
use std::collections::HashMap;
use std::fs::read_to_string;
use std::sync::Arc;
use std::sync::RwLock;
use std::time::{Duration, Instant};

#[derive(Clone, PartialEq, Eq)]
pub enum CachedFileType {
    FileSystemBacked,
    EditorBacked,
}
#[derive(Clone)]
pub struct CachedFile {
    content: Arc<SourceFile>,
    file_type: CachedFileType,
    read_time: Instant,
}

impl CachedFile {
    fn new(content: SourceFile, file_type: CachedFileType) -> Self {
        Self {
            content: Arc::new(content),
            file_type,
            read_time: Instant::now(),
        }
    }

    fn expired(&self) -> bool {
        const MAX_EXPIRE_TIME: Duration = Duration::from_secs(60);

        self.file_type == CachedFileType::FileSystemBacked
            && self.read_time + MAX_EXPIRE_TIME < Instant::now()
    }
}

#[derive(Default)]
pub struct FileCache {
    files: RwLock<HashMap<String, CachedFile>>,
}

impl FileCache {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn set_from_editor(&self, filename: String, content: String) {
        let cached_file = CachedFile {
            content: Arc::new(SourceFile::new(filename.clone(), content)),
            file_type: CachedFileType::EditorBacked,
            read_time: Instant::now(),
        };
        let mut files = self.files.write().unwrap();
        if let Some(existing) = files.get_mut(&filename) {
            *existing = cached_file;
        } else {
            files.insert(filename, cached_file);
        }
    }

    pub fn release_from_editor(&self, filename: &str) {
        self.files.write().unwrap().remove(filename);
    }

    pub async fn get_or_fetch(&self, filename: String) -> Result<Arc<SourceFile>> {
        {
            let files = self.files.read().unwrap();
            if let Some(existing) = files.get(&filename) {
                if !existing.expired() {
                    return Ok(existing.content.clone());
                }
            }
        }
        // TODO: using tokio:: here is actually slower
        let content = read_to_string(&filename)?;
        let cached_file = CachedFile::new(
            SourceFile::new(filename.clone(), content),
            CachedFileType::FileSystemBacked,
        );
        let mut files = self.files.write().unwrap();
        files.insert(filename, cached_file.clone());
        Ok(cached_file.content)
    }
}
