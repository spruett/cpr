use crate::config::Config;
use crate::document::ScopeSpan;
use crate::include_resolver::{is_header, matching_header};
use crate::indexer::{IncludeDepth, Indexer, RankedItem};
use crate::progress::ProgressItem;
use crate::symbols::{
    get_scope_suggestion, scope_match, symbol_needs_scope, LocalSymbolEntry, SymbolEntry,
    SymbolEntryType, TypeReference,
};
use crate::text_util::{identifier_before, include_file_before, IdentifierResult};
use crate::workspace::WorkspaceManager;
use anyhow::Result;
use lsp_types::{
    CompletionItem, CompletionItemKind, CompletionList, CompletionResponse, Documentation,
    InsertTextFormat, MarkupContent, Position,
};
use multimap::MultiMap;
use rayon::iter::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};
use rayon::slice::ParallelSliceMut;
use std::collections::{BTreeSet, HashMap, HashSet, VecDeque};
use std::fs::read_dir;
use std::path::Path;
use std::sync::{Arc, Mutex};
use tokio::sync::OnceCell;

pub fn symbol_filter_text(symbol: &str) -> String {
    symbol.to_ascii_lowercase()
}

fn is_subsequence(symbol: &str, query: &str) -> bool {
    let query_chars: Vec<_> = query.chars().collect();
    let mut query_index = 0;
    for c in symbol.chars() {
        if query_chars[query_index] == c {
            query_index += 1;
            if query_index == query_chars.len() {
                return true;
            }
        }
    }
    false
}

fn to_camel_case(snake_case: &str) -> String {
    if !snake_case.contains('_') {
        snake_case.to_owned()
    } else {
        let mut buffer = Vec::new();
        let mut start_word = true;
        for c in snake_case.chars() {
            if start_word {
                buffer.push(c.to_ascii_uppercase());
                start_word = false;
            } else if c == '_' {
                start_word = true;
            } else {
                buffer.push(c);
            }
        }
        buffer.into_iter().collect()
    }
}

fn acronym(symbol: &str) -> String {
    let symbol = to_camel_case(symbol);

    let mut buffer = String::new();
    let mut last_idx = 0;
    for (idx, c) in symbol.char_indices() {
        if c.is_ascii_uppercase() || idx == 0 {
            buffer.push(c);
            last_idx = idx;
        }
    }
    if last_idx + 1 < symbol.len() {
        buffer += &symbol[last_idx + 1..];
    }
    buffer
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum CompletionKind {
    Symbol,
    Local,
    Reference,
}

impl CompletionKind {
    fn rank(&self) -> usize {
        match self {
            Self::Symbol => 0,
            Self::Local => Config::get().completion.local_score.unwrap_or(0),
            Self::Reference => Config::get().completion.reference_score.unwrap_or(60),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum CompletionMatchKind {
    Prefix,
    CaseInsensitivePrefix,
    FullAcronym,
    Acronym,
    CaseInsensitiveAcronym,
    Fuzzy,
    CaseInsensitiveFuzzy,
}
impl CompletionMatchKind {
    fn rank(&self) -> usize {
        match self {
            CompletionMatchKind::Prefix => 0,
            CompletionMatchKind::CaseInsensitivePrefix => {
                Config::get().completion.case_score.unwrap_or(21)
            }
            CompletionMatchKind::FullAcronym => 5,
            CompletionMatchKind::Acronym => 10,
            CompletionMatchKind::CaseInsensitiveAcronym => {
                Config::get().completion.case_score.unwrap_or(21) + 5
            }
            CompletionMatchKind::Fuzzy => Config::get().completion.fuzzy_score.unwrap_or(25),
            CompletionMatchKind::CaseInsensitiveFuzzy => {
                Config::get().completion.fuzzy_score.unwrap_or(25)
                    + Config::get().completion.case_score.unwrap_or(21)
            }
        }
    }

    fn filter_text(&self, symbol: &str) -> Option<String> {
        match self {
            Self::Acronym => Some(acronym(symbol)),
            Self::CaseInsensitiveAcronym => Some(acronym(symbol).to_ascii_lowercase()),
            Self::Prefix => Some(symbol.to_owned()),
            Self::CaseInsensitivePrefix => Some(symbol.to_ascii_lowercase()),
            _ => None,
        }
    }

    fn from_query(symbol: &str, query: &str) -> Option<Self> {
        let symbol_lower = symbol.to_ascii_lowercase();
        let query_lower = query.to_ascii_lowercase();
        let acronym = acronym(symbol);
        let acronym_lower = acronym.to_ascii_lowercase();
        if symbol.starts_with(query) {
            Some(Self::Prefix)
        } else if acronym == query {
            Some(Self::FullAcronym)
        } else if acronym.starts_with(query) && !symbol.starts_with('_') {
            Some(Self::Acronym)
        } else if symbol_lower.starts_with(&query_lower) {
            Some(Self::CaseInsensitivePrefix)
        } else if is_subsequence(symbol, query) && !symbol.starts_with('_') {
            Some(Self::Fuzzy)
        } else if acronym_lower.starts_with(&query.to_ascii_lowercase()) {
            Some(Self::CaseInsensitiveAcronym)
        } else if is_subsequence(&symbol_lower, &query_lower) {
            Some(Self::CaseInsensitiveFuzzy)
        } else {
            None
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CompletionRank {
    kind: CompletionKind,
    usages: usize,
    include_depth: IncludeDepth,
    match_kind: CompletionMatchKind,
    is_scope_match: bool,
    is_type_match: bool,
    symbol_entry_type: Option<SymbolEntryType>,
}
impl CompletionRank {
    pub fn rank(&self) -> usize {
        let mut rank = 0;
        if !self.is_type_match {
            rank += Config::get().completion.type_score.unwrap_or(100);
        }
        if !self.is_scope_match {
            rank += Config::get().completion.scope_score.unwrap_or(100);
        }
        rank += self.match_kind.rank();
        rank += 5_usize.saturating_sub(self.usages);
        rank += self.include_depth;
        rank += self.kind.rank();
        rank += match self.symbol_entry_type {
            Some(SymbolEntryType::MemberDeclaration) => 2,
            Some(SymbolEntryType::ClassConstructorDeclaration)
            | Some(SymbolEntryType::ClassConstructorDefinition) => 15,
            _ => 0,
        };
        rank
    }
}
impl PartialOrd for CompletionRank {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for CompletionRank {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.rank().cmp(&other.rank())
    }
}

fn build_multi_map<I, F, T>(items: I, name_fn: F) -> MultiMap<String, T>
where
    I: Iterator<Item = T>,
    F: Fn(&T) -> &str,
{
    let mut symbols = MultiMap::new();
    for item in items {
        let name = name_fn(&item);
        symbols.insert(name.to_owned(), item);
    }
    symbols
}

struct CachedFile {
    symbols: MultiMap<String, RankedItem<SymbolEntry, IncludeDepth>>,
    local_symbols: MultiMap<String, LocalSymbolEntry>,
    symbol_names: BTreeSet<String>,
    reference_counts: HashMap<String, usize>,
    types: MultiMap<String, TypeReference>,
    inferred_local_types: MultiMap<LocalSymbolEntry, TypeReference>,
    type_aliases: MultiMap<String, TypeReference>,
    visible_scopes: Vec<ScopeSpan>,
}
impl CachedFile {
    fn new(
        entries: Vec<RankedItem<SymbolEntry, IncludeDepth>>,
        locals: Vec<LocalSymbolEntry>,
        refs: HashMap<String, usize>,
        visible_scopes: Vec<ScopeSpan>,
    ) -> Self {
        let mut symbol_names = BTreeSet::new();

        let mut reference_freq = HashMap::new();
        for (ref_name, count) in &refs {
            *reference_freq.entry(ref_name.clone()).or_insert(0) += count;
            symbol_names.insert(ref_name.clone());
        }

        let mut types = MultiMap::new();
        let mut type_aliases = MultiMap::new();
        for entry in &entries {
            if let Some(type_name) = &entry.value.type_name {
                types.insert(entry.value.symbol.clone(), type_name.clone());
            }
            symbol_names.insert(entry.value.symbol.clone());
            match entry.value.entry_type {
                SymbolEntryType::ClassDefinition => {
                    for base_class in &entry.value.base_classes {
                        type_aliases.insert(
                            entry.value.symbol.clone(),
                            TypeReference::new(base_class.symbol.clone()),
                        );
                    }
                }
                SymbolEntryType::TypeAlias => {
                    for alias in &entry.value.type_name {
                        type_aliases.insert(entry.value.symbol.clone(), alias.clone());
                    }
                }
                _ => {}
            }
        }
        let mut inferred_local_types = MultiMap::new();
        for entry in &locals {
            for to_infer in &entry.infer_types_from {
                let inferred: Vec<_> = types
                    .get_vec(to_infer)
                    .cloned()
                    .into_iter()
                    .flatten()
                    .collect();
                if !inferred.is_empty() {
                    inferred_local_types.insert_many(entry.clone(), inferred.clone().into_iter());
                    types.insert_many(entry.symbol.clone(), inferred.into_iter());
                }
            }
            if let Some(type_name) = &entry.type_name {
                types.insert(entry.symbol.clone(), type_name.clone());
            }
            symbol_names.insert(entry.symbol.clone());
        }

        Self {
            symbols: build_multi_map(entries.into_iter(), |entry| &entry.value.symbol),
            local_symbols: build_multi_map(locals.into_iter(), |local| &local.symbol),
            symbol_names,
            reference_counts: reference_freq,
            types,
            type_aliases,
            inferred_local_types,
            visible_scopes,
        }
    }

    fn search_symbol_names(
        &self,
        symbol: &IdentifierResult,
    ) -> HashMap<String, CompletionMatchKind> {
        self.symbol_names
            .par_iter()
            .filter_map(|symbol_name| {
                CompletionMatchKind::from_query(symbol_name, &symbol.identifier)
                    .map(|kind| (symbol_name.clone(), kind))
            })
            .collect()
    }

    fn scopes_visible(&self, position: &Position) -> Vec<&ScopeSpan> {
        self.visible_scopes
            .iter()
            .filter(|scope| scope.span.contains(position))
            .collect()
    }
}

#[derive(Default)]
pub struct CompletionCache {
    items: Mutex<HashMap<String, Arc<OnceCell<Arc<CachedFile>>>>>,
}

impl CompletionCache {
    pub fn new() -> Self {
        Default::default()
    }

    pub async fn find(
        &self,
        filename: &str,
        full_symbol: IdentifierResult,
        max_results: usize,
        progress: Arc<ProgressItem>,
    ) -> Vec<RankedItem<SymbolEntry, CompletionRank>> {
        let cached = self.lookup(filename, progress).await;
        let mut matches = HashMap::new();
        matches.insert(full_symbol.identifier.clone(), CompletionMatchKind::Prefix);
        self.search(
            &*cached,
            &matches,
            &full_symbol,
            false,
            max_results,
            &Vec::new(),
        )
    }

    pub async fn scan(
        &self,
        filename: &str,
        partial_symbol: IdentifierResult,
        symbol_names: &HashMap<String, CompletionMatchKind>,
        max_results: usize,
        progress: Arc<ProgressItem>,
    ) -> Vec<RankedItem<SymbolEntry, CompletionRank>> {
        let cached = self.lookup(filename, progress).await;
        let field_types = if let Some(field) = &partial_symbol.field_name_before {
            self.get_type_names(filename, field).await
        } else {
            Vec::new()
        };
        self.search(
            &*cached,
            symbol_names,
            &partial_symbol,
            true,
            max_results,
            &field_types,
        )
    }

    pub async fn scan_references(
        &self,
        filename: &str,
        symbol_names: &HashMap<String, CompletionMatchKind>,
        max_results: usize,
        progress: Arc<ProgressItem>,
    ) -> Vec<RankedItem<String, CompletionRank>> {
        let cached = self.lookup(filename, progress).await;
        let mut results = Vec::new();
        for (symbol_name, match_kind) in symbol_names {
            if let Some(freq) = cached.reference_counts.get(symbol_name) {
                let rank = CompletionRank {
                    kind: CompletionKind::Reference,
                    usages: *freq,
                    match_kind: *match_kind,
                    include_depth: 0,
                    is_scope_match: false,
                    is_type_match: false,
                    symbol_entry_type: None,
                };
                results.push(RankedItem {
                    value: symbol_name.clone(),
                    rank,
                });
            }
        }
        results.sort_by_key(|item| item.rank.clone());
        results.into_iter().take(max_results).collect()
    }

    pub async fn scan_locals(
        &self,
        filename: &str,
        symbol_names: &HashMap<String, CompletionMatchKind>,
        max_results: usize,
    ) -> Vec<RankedItem<LocalSymbolEntry, CompletionRank>> {
        let cached = self.lookup(filename, ProgressItem::noop()).await;
        let mut dedup_cache = HashSet::new();
        let mut results = Vec::new();
        for (symbol_name, match_kind) in symbol_names {
            for local in cached
                .local_symbols
                .get_vec(symbol_name)
                .into_iter()
                .flatten()
            {
                let freq = cached
                    .reference_counts
                    .get(&local.symbol)
                    .cloned()
                    .unwrap_or(0);
                let rank = CompletionRank {
                    kind: CompletionKind::Local,
                    usages: freq,
                    match_kind: *match_kind,
                    include_depth: 0,
                    is_scope_match: false,
                    is_type_match: false,
                    symbol_entry_type: None,
                };
                results.push(RankedItem {
                    value: local.clone(),
                    rank,
                });
            }
        }
        results.sort_by_key(|item| item.rank.clone());
        results
            .into_iter()
            .filter(|RankedItem { value: symbol, .. }| dedup_cache.insert(symbol.symbol.clone()))
            .take(max_results)
            .collect()
    }

    pub async fn get_inferred_types(
        &self,
        filename: &str,
        local: &LocalSymbolEntry,
    ) -> Vec<TypeReference> {
        let cached = self.lookup(filename, ProgressItem::noop()).await;
        let mut dedup = HashSet::new();
        cached
            .inferred_local_types
            .get_vec(local)
            .cloned()
            .into_iter()
            .flatten()
            .filter(|item| dedup.insert(item.clone()))
            .collect()
    }

    pub async fn get_type_names(&self, filename: &str, identifier: &str) -> Vec<String> {
        let cached = self.lookup(filename, ProgressItem::noop()).await;

        let initial_types = cached
            .types
            .get_vec(identifier)
            .cloned()
            .unwrap_or_default()
            .into_iter()
            .flat_map(|ty| ty.expand())
            .collect::<Vec<_>>();
        let mut to_process = VecDeque::from(initial_types);

        let mut processed = HashSet::new();
        let mut types = Vec::new();
        while let Some(type_name) = to_process.pop_front() {
            if processed.insert(type_name.clone()) {
                types.push(type_name.clone());
            }
            for alias in cached
                .type_aliases
                .get_vec(&type_name)
                .into_iter()
                .flatten()
                .map(|ty| ty.expand())
                .flatten()
            {
                if !processed.contains(&alias) {
                    to_process.push_back(alias.clone());
                }
            }
        }
        types
    }

    pub fn clear(&self, filename: &str) {
        let mut locked = self.items.lock().unwrap();
        locked.remove(filename);
    }

    #[allow(clippy::too_many_arguments)]
    fn search(
        &self,
        file: &CachedFile,
        symbol_names: &HashMap<String, CompletionMatchKind>,
        identifier: &IdentifierResult,
        dedup: bool,
        max_results: usize,
        type_names: &[String],
    ) -> Vec<RankedItem<SymbolEntry, CompletionRank>> {
        let mut results: Vec<_> = symbol_names
            .par_iter()
            .flat_map(|(symbol_name, match_kind)| {
                let match_kind = *match_kind;
                file.symbols
                    .get_vec(symbol_name)
                    .into_par_iter()
                    .flatten()
                    .map(
                        move |RankedItem {
                                  value,
                                  rank: include_depth,
                              }| {
                            let freq = file
                                .reference_counts
                                .get(&value.symbol)
                                .cloned()
                                .unwrap_or(0);
                            let is_scope_match = if identifier.is_member {
                                value.entry_type.is_member_or_method()
                            } else {
                                identifier.scope.is_some()
                                    && scope_match(&value.scope, &identifier.scope)
                            };
                            let is_type_match = type_names.iter().any(|type_name| {
                                scope_match(&value.scope, &Some(type_name.clone()))
                            });
                            let rank = CompletionRank {
                                kind: CompletionKind::Symbol,
                                usages: freq,
                                include_depth: *include_depth,
                                match_kind,
                                is_scope_match,
                                is_type_match,
                                symbol_entry_type: Some(value.entry_type.clone()),
                            };
                            RankedItem {
                                value: value.clone(),
                                rank,
                            }
                        },
                    )
            })
            .collect();
        let mut dedup_cache = HashSet::new();
        results.par_sort_unstable_by_key(|item| item.rank.rank());
        results
            .into_iter()
            .filter(|RankedItem { value: symbol, .. }| {
                !dedup || dedup_cache.insert((symbol.symbol.clone(), symbol.entry_type.clone()))
            })
            .take(max_results)
            .collect()
    }

    async fn lookup(&self, filename: &str, progress: Arc<ProgressItem>) -> Arc<CachedFile> {
        let cell = {
            let mut locked = self.items.lock().unwrap();
            locked
                .entry(filename.to_owned())
                .or_insert_with(|| Arc::new(OnceCell::new()))
                .clone()
        };
        cell.get_or_init(|| async {
            let symbols = Indexer::get()
                .get_all_symbols(filename.to_owned(), progress)
                .await
                .into_iter()
                .map(|item| RankedItem::new(item.value.1, item.rank))
                .collect();
            let mut refs = Indexer::get()
                .get_file_symbol_references(filename.to_owned())
                .await;
            if let Some(matching_header) = matching_header(Path::new(filename)) {
                refs.extend(
                    Indexer::get()
                        .get_file_symbol_references(matching_header.to_string_lossy().to_string())
                        .await,
                );
            }
            let index = Indexer::get().get_index(filename.to_owned()).await;
            let (locals, scopes) = match index {
                Ok(doc) => {
                    let locals = doc
                        .document()
                        .local_symbols
                        .iter_all()
                        .flat_map(|(_, locals)| locals.to_owned())
                        .collect();
                    let scopes = doc.document().scopes.clone();
                    (locals, scopes)
                }
                Err(_) => (Vec::new(), Vec::new()),
            };
            Arc::new(CachedFile::new(symbols, locals, refs, scopes))
        })
        .await
        .clone()
    }
}

pub fn cpp_keywords() -> Vec<&'static str> {
    vec![
        "alignas",
        "alignof",
        "and",
        "and_eq",
        "asm",
        "atomic_cancel",
        "atomic_commit",
        "atomic_noexcept",
        "auto",
        "bitand",
        "bitor",
        "bool",
        "break",
        "case",
        "catch",
        "char",
        "class",
        "concept",
        "const",
        "consteval",
        "constexpr",
        "constinit",
        "const_cast",
        "continue",
        "co_await",
        "co_return",
        "co_yield",
        "decltype",
        "default",
        "delete",
        "do",
        "double",
        "dynamic_cast",
        "else",
        "enum",
        "explicit",
        "export",
        "extern",
        "false",
        "float",
        "for",
        "friend",
        "goto",
        "if",
        "inline",
        "int",
        "long",
        "mutable",
        "namespace",
        "new",
        "noexcept",
        "nullptr",
        "operator",
        "private",
        "protected",
        "public",
        "reinterpret_cast",
        "requires",
        "return",
        "short",
        "signed",
        "sizeof",
        "static",
        "static_assert",
        "static_cast",
        "struct",
        "switch",
        "synchronized",
        "template",
        "this",
        "thread_local",
        "throw",
        "true",
        "try",
        "typedef",
        "typeid",
        "typename",
        "union",
        "unsigned",
        "using",
        "virtual",
        "void",
        "volatile",
        "while",
        "include",
        "define",
    ]
}

fn build_cpp_keywords(identifier: &IdentifierResult) -> Vec<CompletionItem> {
    if identifier.is_member || identifier.scope.is_some() {
        return Vec::new();
    }
    let mut completions = Vec::new();
    for keyword in cpp_keywords() {
        if keyword.starts_with(&identifier.identifier) {
            completions.push(CompletionItem {
                label: keyword.to_owned(),
                kind: Some(CompletionItemKind::Keyword),
                ..Default::default()
            });
        }
    }
    completions
}

fn complete_includes(filename: &str, partial_include: &str) -> Result<CompletionResponse> {
    let mut completions = Vec::new();
    if let Some(workspace) = WorkspaceManager::new().get_workspace(filename) {
        let root = workspace.project_root();
        let (folder_prefix, suffix) = match partial_include.rsplit_once('/') {
            Some((folder, suffix)) => (folder, suffix),
            None => ("", ""),
        };
        let directory = Path::new(&root).join(folder_prefix);
        let mut entries = Vec::new();
        for dir_entry in read_dir(&directory)? {
            let dir_entry = dir_entry?;
            let filename = dir_entry.file_name().to_string_lossy().to_string();
            if !symbol_filter_text(&filename).starts_with(&symbol_filter_text(suffix)) {
                continue;
            }

            let file_type = dir_entry.file_type()?;
            if file_type.is_dir() {
                entries.push(filename + "/");
            } else if file_type.is_file() && is_header(&dir_entry.path()) {
                entries.push(filename);
            }
        }
        for entry in entries {
            completions.push(CompletionItem {
                label: entry.clone(),
                kind: Some(CompletionItemKind::File),
                filter_text: Some(symbol_filter_text(&entry)),
                ..Default::default()
            });
        }
    }

    Ok(CompletionResponse::List(CompletionList {
        is_incomplete: false,
        items: completions,
    }))
}

async fn complete_locals(
    cache: &CompletionCache,
    filename: &str,
    identifier: &IdentifierResult,
    position: Position,
    matching_names: &HashMap<String, CompletionMatchKind>,
    max_rank: usize,
) -> Vec<CompletionItem> {
    let mut completions = Vec::new();
    if identifier.is_member || identifier.scope.is_some() {
        return completions;
    }

    for ranked_local in cache.scan_locals(filename, matching_names, 20).await {
        if !ranked_local.value.valid_span.contains(&position) || ranked_local.rank.rank() > max_rank
        {
            continue;
        }
        let type_name = if ranked_local.value.type_string == Some("auto".to_owned()) {
            Some(
                cache
                    .get_inferred_types(filename, &ranked_local.value)
                    .await
                    .iter()
                    .map(|ty| ty.type_string())
                    .collect::<Vec<_>>()
                    .join(" | "),
            )
        } else {
            ranked_local.value.type_string
        };
        completions.push(CompletionItem {
            label: ranked_local.value.symbol.clone(),
            kind: Some(ranked_local.value.entry_type.to_completion_kind()),
            detail: type_name,
            filter_text: ranked_local
                .rank
                .match_kind
                .filter_text(&ranked_local.value.symbol),
            sort_text: Some(format!(
                "{:06}-{}",
                ranked_local.rank.rank(),
                ranked_local.value.symbol
            )),
            ..Default::default()
        });
    }
    completions
}

async fn complete_refs(
    cache: &CompletionCache,
    filename: &str,
    matching_names: &HashMap<String, CompletionMatchKind>,
    progress: &Arc<ProgressItem>,
    max_rank: usize,
) -> Vec<CompletionItem> {
    let mut completion_items = Vec::new();
    let refs = cache
        .scan_references(filename, matching_names, 20, progress.clone())
        .await;
    for reference in refs {
        if reference.rank.rank() > max_rank {
            continue;
        }
        completion_items.push(CompletionItem {
            label: reference.value.clone(),
            kind: Some(CompletionItemKind::Unit),
            filter_text: reference.rank.match_kind.filter_text(&reference.value),
            sort_text: Some(format!("{:06}-{}", reference.rank.rank(), reference.value,)),
            ..Default::default()
        });
    }
    completion_items
}

fn complete_symbols(
    symbols: Vec<RankedItem<SymbolEntry, CompletionRank>>,
    max_rank: usize,
    is_function_call: bool,
    is_already_scoped: bool,
    visible_scopes: &[&ScopeSpan],
) -> Vec<CompletionItem> {
    let mut completion_items = Vec::new();
    for RankedItem {
        value: symbol,
        rank,
    } in symbols
    {
        if rank.rank() > max_rank {
            continue;
        }
        let docs = format!(
            "```\n{}\n```\n{}",
            symbol.type_string().unwrap_or_default(),
            symbol.docs.unwrap_or_default()
        );
        let mut insert_text = if Config::get().features.autocomplete_parameter_template
            && symbol.entry_type.is_callable()
            && !is_function_call
        {
            let params_text = symbol
                .params
                .iter()
                .enumerate()
                .map(|(i, param)| format!("${{{}:{}}}", i + 1, param.name))
                .collect::<Vec<String>>()
                .join(", ");
            format!("{}({})", symbol.symbol, params_text)
        } else {
            symbol.symbol.clone()
        };
        if Config::get().features.autocomplete_scope
            && !is_already_scoped
            && symbol_needs_scope(&symbol.entry_type)
        {
            if let Some(suggestion) = get_scope_suggestion(visible_scopes, &[symbol.scope.clone()])
            {
                insert_text = format!("{}::{}", suggestion, insert_text);
            }
        }
        completion_items.push(CompletionItem {
            label: symbol.symbol.clone(),
            kind: Some(symbol.entry_type.to_completion_kind()),
            detail: symbol.scope,
            documentation: Some(Documentation::MarkupContent(MarkupContent {
                kind: lsp_types::MarkupKind::Markdown,
                value: docs,
            })),
            filter_text: rank.match_kind.filter_text(&symbol.symbol),
            sort_text: Some(format!("{:06}-{}", rank.rank(), symbol.symbol)),
            insert_text: Some(insert_text),
            insert_text_format: Some(InsertTextFormat::Snippet),
            tags: Some(symbol.tags.iter().map(|t| t.to_completion_tag()).collect()),
            ..Default::default()
        });
    }
    completion_items
}

pub async fn build_completions(
    cache: &CompletionCache,
    filename: String,
    position: Position,
    progress: Arc<ProgressItem>,
) -> Result<CompletionResponse> {
    let content = Indexer::get().get_file_content(filename.clone()).await?;
    if let Some(include) = include_file_before(&content.content, position) {
        return complete_includes(&filename, &include);
    }

    let symbol_name = identifier_before(&content.content, position);
    let cached_completions = cache.lookup(&filename, progress.clone()).await;
    let matching_names = cached_completions.search_symbol_names(&symbol_name);
    let visible_scopes = cached_completions.scopes_visible(&position);
    let is_function_call = symbol_name.is_function_call;
    let symbols = cache
        .scan(
            &filename,
            symbol_name.clone(),
            &matching_names,
            20,
            progress.clone(),
        )
        .await;
    let mut completion_items = Vec::new();
    let min_rank = symbols
        .iter()
        .map(|sym| sym.rank.rank())
        .min()
        .unwrap_or(usize::MAX / 2);
    let max_rank = min_rank
        + Config::get()
            .completion
            .truncate_after_rank_diff
            .unwrap_or(95);
    completion_items.extend(complete_symbols(
        symbols,
        max_rank,
        is_function_call,
        symbol_name.is_member || symbol_name.scope.is_some(),
        &visible_scopes,
    ));
    completion_items
        .extend(complete_refs(cache, &filename, &matching_names, &progress, max_rank).await);
    completion_items.extend(
        complete_locals(
            cache,
            &filename,
            &symbol_name,
            position,
            &matching_names,
            max_rank,
        )
        .await,
    );
    for keyword_item in build_cpp_keywords(&symbol_name) {
        completion_items.push(keyword_item);
    }
    completion_items.sort_by_key(|item| item.sort_text.clone().unwrap_or_default());
    if let Some(first) = completion_items.first_mut() {
        first.preselect = Some(true);
    }
    progress.complete();
    Ok(CompletionResponse::List(CompletionList {
        is_incomplete: true,
        items: completion_items,
    }))
}
