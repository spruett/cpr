use multimap::MultiMap;
use tree_sitter::{Node, Parser, Point, Tree};

use crate::config::Config;
use crate::diagnostics::{Diagnostic, DiagnosticType};
use crate::document::{Document, IncludedFile, ParsedFile, ScopeSpan};
use crate::span::{SourceFile, SourceSpan};
use crate::symbols::{
    IncludeFile, LocalSymbolEntry, ParameterEntry, SymbolAtPoint, SymbolEntry, SymbolEntryType,
    SymbolReference, TypeReference,
};
use regex::{Captures, Regex};
use std::sync::Arc;

fn hacky_sanitize(content: String) -> String {
    lazy_static::lazy_static! {
        static ref NAMESPACE_REGEX: Regex = Regex::new("namespace ([a-z0-9_:]+)").unwrap();
    }
    let sanitized_macros = [
        "FOLLY_NODISCARD",
        "FOLLY_ALWAYS_INLINE",
        "FOLLY_NOINLINE",
        "FOLLY_INLINE_VARIABLE",
        "GLIBCXX_VISIBILITY(default)",
        "_GLIBCXX_VISIBILITY(default)",
        "co_await ",
    ];
    let mut content = content;
    let mut sanitize_content = |pattern: &str| {
        content = content.replace(pattern, &" ".repeat(pattern.len()));
    };
    for sanitize in &sanitized_macros {
        sanitize_content(sanitize);
    }
    for sanitize in &Config::get().macros.ignore {
        sanitize_content(sanitize);
    }

    content = content.replace("co_return", "return   ");
    content = content.replace("co_yield ", "return   ");

    let content = NAMESPACE_REGEX.replace_all(&content, |captures: &Captures| {
        return format!(
            "namespace {}",
            captures.get(1).unwrap().as_str().replace("::", "__")
        );
    });
    content.to_string()
}

fn join_scope(namespace: &Option<String>, scope: &Option<String>) -> Option<String> {
    match (namespace, scope) {
        (Some(ns), Some(s)) => Some(format!("{}::{}", ns, s)),
        (Some(ns), None) => Some(ns.to_owned()),
        (None, Some(s)) => Some(s.to_owned()),
        (None, None) => None,
    }
}

pub struct CppFile {
    doc: Document,

    syntax_tree: Tree,
    current_scope: Option<String>,
}

impl ParsedFile for CppFile {
    fn parse_from_file(source: Arc<SourceFile>) -> Arc<Self> {
        let mut parser = Parser::new();
        parser
            .set_language(tree_sitter_cpp::language())
            .expect("failed to set cpp language");
        let tree = parser
            .parse(hacky_sanitize(source.content.clone()), None)
            .expect("failed to parse");
        let mut index = Self {
            doc: Document {
                source,
                symbols: MultiMap::new(),
                symbol_references: MultiMap::new(),
                local_symbols: MultiMap::new(),
                includes: Vec::new(),
                diagnostics: Vec::new(),
                scopes: Vec::new(),
            },
            syntax_tree: tree.clone(),
            current_scope: None,
        };
        let root_node = tree.root_node();
        index.process_translation_unit(root_node);
        index.walk_node(root_node);
        log::debug!("full tree: {}", index.syntax_tree.root_node().to_sexp());
        Arc::new(index)
    }

    fn get_symbol_at_point(&self, line: usize, column: usize) -> SymbolAtPoint {
        let begin = Point::new(line, column);
        let end = Point::new(line, column);
        let node = match self
            .syntax_tree
            .root_node()
            .descendant_for_point_range(begin, end)
        {
            Some(n) => n,
            None => return SymbolAtPoint::Missing,
        };

        let text = node
            .utf8_text(self.doc.source.content.as_bytes())
            .unwrap()
            .to_owned();
        let parent = match node.parent() {
            Some(p) => p,
            None => return SymbolAtPoint::Missing,
        };
        match parent.kind() {
            "preproc_include" => match self.parse_preproc_include(parent) {
                Some(include) => SymbolAtPoint::Include(include),
                None => SymbolAtPoint::Missing,
            },
            _ => SymbolAtPoint::Identifier(text),
        }
    }

    fn document(&self) -> &Document {
        &self.doc
    }

    fn ast(&self) -> Option<Node> {
        Some(self.syntax_tree.root_node())
    }
}

fn remove_template_args(scope: &str) -> String {
    let mut output = Vec::new();
    let mut template_index = 0;
    for c in scope.chars() {
        match c {
            '<' => template_index += 1,
            '>' => template_index -= 1,
            x if template_index == 0 => output.push(x),
            _ => {}
        }
    }
    output.into_iter().collect()
}

fn resolve_name_from_declarator(node: Node) -> Option<Node> {
    let mut cur = Some(node);
    while let Some(node) = cur {
        match node.kind() {
            "pointer_declarator" | "init_declarator" => {
                cur = node.child_by_field_name("declarator");
            }
            "parenthesized_declarator" | "reference_declarator" | "variadic_declarator" => {
                cur = node.named_child(0);
            }
            "template_type" | "scoped_identifier" | "scoped_type_identifier" => {
                cur = node.child_by_field_name("name");
            }
            "array_declarator" | "function_declarator" => {
                cur = node.child_by_field_name("declarator");
            }
            "template_function" => {
                cur = node.child_by_field_name("name");
            }
            "identifier" | "type_identifier" | "field_identifier" => {
                break;
            }
            "abstract_reference_declarator" | "abstract_pointer_declarator" | "operator_name" => {
                return None
            }
            _ => {
                log::error!("cannot resolve type: {} - {}", node.kind(), node.to_sexp());
                break;
            }
        }
    }
    cur
}

impl CppFile {
    fn get_scope_from_identifier(&self, node: &Node) -> Option<String> {
        let parent = node.parent()?;
        if node.kind() != "namespace_identifier"
            && (parent.kind() == "scoped_identifier" || parent.kind() == "scoped_type_identifier")
        {
            let namespace = parent.child_by_field_name("namespace")?;
            let scope = namespace
                .utf8_text(self.doc.source.content.as_bytes())
                .unwrap();
            Some(remove_template_args(scope))
        } else {
            None
        }
    }

    fn add_local_parameter(
        &mut self,
        node: &Node,
        valid_scope: SourceSpan,
        is_param_declaration: bool,
    ) {
        let (node, type_name, type_names, entry_type) = match node.kind() {
            "type_parameter_declaration" | "variadic_type_parameter_declaration" => {
                if let Some(identifier) = node.named_child(0) {
                    (identifier, None, None, SymbolEntryType::TemplateType)
                } else {
                    return;
                }
            }
            "optional_type_parameter_declaration" => {
                if let Some(identifier) = node.child_by_field_name("name") {
                    (identifier, None, None, SymbolEntryType::TemplateType)
                } else {
                    return;
                }
            }
            "parameter_declaration"
            | "optional_parameter_declaration"
            | "variadic_parameter_declaration" => {
                let identifier = node
                    .child_by_field_name("declarator")
                    .and_then(resolve_name_from_declarator);
                let mut type_name = None;
                let type_string = node.child_by_field_name("type").map(|node| {
                    type_name = self.get_type_name(&node);
                    node.utf8_text(self.doc.source.content.as_bytes())
                        .unwrap()
                        .to_owned()
                });
                if let Some(identifier) = identifier {
                    (
                        identifier,
                        type_string,
                        type_name,
                        SymbolEntryType::Parameter,
                    )
                } else {
                    return;
                }
            }
            _ => return,
        };
        let symbol_name = node
            .utf8_text(self.doc.source.content.as_bytes())
            .unwrap()
            .to_owned();
        let local_symbol = LocalSymbolEntry {
            symbol: symbol_name.clone(),
            span: SourceSpan::from_node(&node),
            valid_span: valid_scope,
            type_string: type_name,
            type_name: type_names,
            infer_types_from: Vec::new(),
            entry_type,
            is_param_declaration,
        };
        self.doc.local_symbols.insert(symbol_name, local_symbol);
    }

    fn get_symbols_to_infer_type(&self, expression: Node) -> Vec<String> {
        match expression.kind() {
            "call_expression" => {
                if let Some(function) = expression.child_by_field_name("function") {
                    return self.get_symbols_to_infer_type(function);
                }
            }
            "scoped_identifier" => {
                if let Some(name) = expression.child_by_field_name("name") {
                    return self.get_symbols_to_infer_type(name);
                }
            }
            "field_identifier" | "identifier" => {
                return vec![expression
                    .utf8_text(self.doc.source.content.as_bytes())
                    .unwrap()
                    .to_owned()];
            }
            "field_expression" => {
                if let Some(field) = expression.child_by_field_name("field") {
                    return self.get_symbols_to_infer_type(field);
                }
            }
            _ => {}
        }
        Vec::new()
    }

    fn add_local_declaration(&mut self, node: &Node, valid_span: SourceSpan) {
        let symbol_node = node
            .child_by_field_name("declarator")
            .and_then(resolve_name_from_declarator);
        let symbol_nodes = match symbol_node {
            Some(s) if s.kind() == "structured_binding_declarator" => {
                let mut cursor = s.walk();
                s.named_children(&mut cursor).collect()
            }
            Some(s) => vec![s],
            None => return,
        };
        let mut type_name = None;
        let type_string = node.child_by_field_name("type").map(|type_node| {
            type_name = self.get_type_name(&type_node);
            type_node
                .utf8_text(self.doc.source.content.as_bytes())
                .unwrap()
                .to_owned()
        });
        let mut symbols_to_infer_type = Vec::new();
        if type_name.is_none() {
            if let Some(definition_value) = node.child_by_field_name("value") {
                symbols_to_infer_type = self.get_symbols_to_infer_type(definition_value);
            } else if let Some(declarator) = node.child_by_field_name("declarator") {
                if declarator.kind() == "init_declarator" {
                    if let Some(definition_value) = declarator.child_by_field_name("value") {
                        symbols_to_infer_type = self.get_symbols_to_infer_type(definition_value);
                    }
                }
            }
        }
        for symbol_node in symbol_nodes {
            let symbol_name = symbol_node
                .utf8_text(self.doc.source.content.as_bytes())
                .unwrap()
                .to_owned();
            let entry = LocalSymbolEntry {
                symbol: symbol_name.clone(),
                span: SourceSpan::from_node(&symbol_node),
                valid_span: valid_span.clone(),
                type_string: type_string.clone(),
                type_name: type_name.clone(),
                infer_types_from: symbols_to_infer_type.clone(),
                entry_type: SymbolEntryType::VariableDefinition,
                is_param_declaration: false,
            };
            self.doc.local_symbols.insert(symbol_name, entry);
        }
    }

    fn walk_node(&mut self, node: Node) {
        self.handle_node(node);

        let mut cursor = node.walk();
        for child in node.children(&mut cursor) {
            self.walk_node(child);
        }
    }
    fn handle_node(&mut self, node: Node) {
        if node.is_error() {
            self.doc.diagnostics.push(Diagnostic {
                diagnostic_type: DiagnosticType::SyntaxError,
                span: SourceSpan::from_node(&node),
                message: "syntax error".to_owned(),
            })
        } else if node.is_missing() {
            self.doc.diagnostics.push(Diagnostic {
                diagnostic_type: DiagnosticType::SyntaxError,
                span: SourceSpan::from_node(&node),
                message: format!("missing {}", node.kind()),
            })
        }

        match node.kind() {
            "using_declaration" => {
                if let Some(child) = node.named_child(0) {
                    if child.kind() == "identifier" {
                        let scope_name =
                            child.utf8_text(self.doc.source.content.as_bytes()).unwrap();
                        self.doc.scopes.push(ScopeSpan {
                            scope: scope_name.to_owned(),
                            span: SourceSpan::from_node(&self.syntax_tree.root_node()),
                        });
                    }
                }
            }
            "identifier" | "type_identifier" | "field_identifier" | "namespace_identifier" => {
                let symbol_ref = self.make_symbol_reference(node);
                if !symbol_ref.symbol.contains("::") {
                    self.doc
                        .symbol_references
                        .insert(symbol_ref.symbol.clone(), symbol_ref);
                }
            }
            "template_declaration" | "function_declarator" | "abstract_function_declarator" => {
                let valid_span = if node.kind() == "template_declaration" {
                    SourceSpan::from_node(&node)
                } else {
                    SourceSpan::from_node(&node.parent().unwrap())
                };
                let is_param_declaration = match node.kind() {
                    "function_declarator" => {
                        matches!(
                            node.parent().unwrap().kind(),
                            "declaration" | "field_declaration"
                        )
                    }
                    "template_declaration" => {
                        if let Some(templated_type) = node.named_child(1) {
                            let has_body = templated_type.child_by_field_name("body").is_some();
                            match templated_type.kind() {
                                "enum_specifier" | "class_specifier" | "struct_specifier" => {
                                    !has_body
                                }
                                _ => false,
                            }
                        } else {
                            false
                        }
                    }
                    _ => false,
                };
                if let Some(parameters) = node.child_by_field_name("parameters") {
                    let mut cursor = parameters.walk();
                    let parameters = parameters.named_children(&mut cursor);
                    for parameter in parameters {
                        self.add_local_parameter(
                            &parameter,
                            valid_span.clone(),
                            is_param_declaration,
                        );
                    }
                }
            }
            "lambda_expression" => {
                let valid_span = SourceSpan::from_node(&node);
                if let Some(captures) = node.child_by_field_name("captures") {
                    let mut cursor = captures.walk();
                    for capture in captures.children(&mut cursor) {
                        if capture.kind() == "assignment_expression" {
                            if let Some(lhs) = capture.child_by_field_name("left") {
                                let lhs = if lhs.kind() == "pointer_expression" {
                                    lhs.child_by_field_name("argument")
                                } else {
                                    Some(lhs)
                                };

                                if let Some(identifier) = lhs {
                                    let symbol_name = identifier
                                        .utf8_text(self.doc.source.content.as_bytes())
                                        .unwrap()
                                        .to_owned();
                                    let entry = LocalSymbolEntry {
                                        symbol: symbol_name.clone(),
                                        span: SourceSpan::from_node(&identifier),
                                        valid_span: valid_span.clone(),
                                        type_string: None,
                                        type_name: None,
                                        infer_types_from: Vec::new(),
                                        entry_type: SymbolEntryType::VariableDefinition,
                                        is_param_declaration: false,
                                    };
                                    self.doc.local_symbols.insert(symbol_name, entry);
                                }
                            }
                        }
                    }
                }
            }
            "catch_clause" => {
                let valid_span = SourceSpan::from_node(&node);
                if let Some(parameters) = node.child_by_field_name("parameters") {
                    let mut cursor = parameters.walk();
                    let parameters = parameters.named_children(&mut cursor);
                    for parameter in parameters {
                        self.add_local_parameter(&parameter, valid_span.clone(), false);
                    }
                }
            }
            "compound_statement" => {
                let valid_span = SourceSpan::from_node(&node);
                let mut cursor = node.walk();
                for child in node.named_children(&mut cursor) {
                    if child.kind() == "declaration" {
                        self.add_local_declaration(&child, valid_span.clone());
                    } else if child.kind() == "alias_declaration" {
                        let local_symbol = || -> Option<LocalSymbolEntry> {
                            let name_node = child.child_by_field_name("name")?;
                            let type_node = child.child_by_field_name("type")?;
                            let name = name_node
                                .utf8_text(self.doc.source.content.as_bytes())
                                .unwrap()
                                .to_owned();

                            let type_string = type_node
                                .utf8_text(self.doc.source.content.as_bytes())
                                .unwrap()
                                .to_owned();
                            let type_name = self.get_type_name(&type_node);

                            Some(LocalSymbolEntry {
                                symbol: name,
                                span: SourceSpan::from_node(&name_node),
                                valid_span: valid_span.clone(),
                                type_string: Some(type_string),
                                type_name,
                                infer_types_from: Vec::new(),
                                entry_type: SymbolEntryType::TypeAlias,
                                is_param_declaration: false,
                            })
                        }();
                        if let Some(symbol) = local_symbol {
                            self.doc.local_symbols.insert(symbol.symbol.clone(), symbol);
                        }
                    }
                }
            }
            "for_range_loop" => {
                let valid_span = SourceSpan::from_node(&node);
                self.add_local_declaration(&node, valid_span);
            }
            "for_statement" => {
                let valid_span = SourceSpan::from_node(&node);
                if let Some(declaration) = node.child_by_field_name("initializer") {
                    self.add_local_declaration(&declaration, valid_span);
                }
            }
            "if_statement" | "while_statement" => {
                let valid_span = SourceSpan::from_node(&node);
                if let Some(condition_clause) = node.child_by_field_name("condition") {
                    if let Some(condition_value) = condition_clause.child_by_field_name("value") {
                        if condition_value.kind() == "declaration" {
                            self.add_local_declaration(&condition_value, valid_span);
                        }
                    }
                }
            }
            "call_expression" => {
                if let Some(function) = node.child_by_field_name("function") {
                    let function_name = function
                        .utf8_text(self.doc.source.content.as_bytes())
                        .unwrap()
                        .to_owned();
                    if let Some(argument_list) = node.named_child(1) {
                        let mut cursor = argument_list.walk();
                        let arguments = argument_list.named_children(&mut cursor).collect();
                        self.expand_macro(&node, function_name, arguments);
                    }
                }
            }
            _ => {}
        }
    }

    fn make_symbol_reference(&self, node: Node) -> SymbolReference {
        let symbol_name = node
            .utf8_text(self.doc.source.content.as_bytes())
            .unwrap()
            .to_owned();
        let scope = self.get_scope_from_identifier(&node);
        let span = SourceSpan::from_node(&node);
        let is_call = is_call_expression(&node);
        let local_reference = scope.is_none()
            && self
                .doc
                .find_local_symbol(&symbol_name)
                .iter()
                .any(|local| local.valid_span.contains_span(&span));
        SymbolReference {
            symbol: symbol_name,
            span,
            scope,
            local_reference,
            is_call,
            is_field: node.kind() == "field_identifier",
            is_namespace: node.kind() == "namespace_identifier",
        }
    }

    fn get_type_name(&mut self, node: &Node) -> Option<TypeReference> {
        match node.kind() {
            "type_identifier" | "namespace_identifier" => {
                let type_name = node.utf8_text(self.doc.source.content.as_bytes()).unwrap();
                Some(TypeReference {
                    scope: None,
                    name: type_name.to_owned(),
                    params: Vec::new(),
                })
            }
            "scoped_type_identifier" => {
                if let Some(name) = node.child_by_field_name("name") {
                    let mut type_name = self.get_type_name(&name)?;
                    let scope_name = self.get_type_name(&node.child_by_field_name("namespace")?)?;
                    type_name.scope = Some(Box::new(scope_name));
                    Some(type_name)
                } else {
                    None
                }
            }
            "template_type" => {
                if let Some(name) = node.child_by_field_name("name") {
                    if let Some(type_name) = self.get_type_name(&name) {
                        let args = if let Some(args) = node.child_by_field_name("arguments") {
                            let mut arguments = Vec::new();
                            let mut cursor = args.walk();
                            for child in args.named_children(&mut cursor) {
                                if let Some(arg) = self.get_type_name(&child) {
                                    arguments.push(arg)
                                }
                            }
                            arguments
                        } else {
                            Vec::new()
                        };
                        return Some(TypeReference {
                            scope: type_name.scope,
                            name: type_name.name,
                            params: args,
                        });
                    }
                }
                None
            }
            "type_descriptor" => {
                if let Some(child) = node.child_by_field_name("type") {
                    self.get_type_name(&child)
                } else {
                    None
                }
            }
            _ => None,
        }
    }

    fn expand_macro(&mut self, node: &Node, function_name: String, args: Vec<Node>) {
        let macro_expansion = match Config::get()
            .macros
            .expand_to_definition
            .get(&function_name)
        {
            Some(patterns) => patterns,
            None => return,
        };

        for expansion in macro_expansion {
            let mut expanded_name = expansion.clone();
            for (i, arg) in args.iter().enumerate() {
                let value = arg.utf8_text(self.doc.source.content.as_bytes()).unwrap();
                expanded_name = expanded_name.replace(&format!("${}", i + 1), value);
            }
            let span = SourceSpan::from_node(node);
            let symbol_entry = SymbolEntry {
                symbol: expanded_name.clone(),
                span,
                entry_type: SymbolEntryType::VariableDefinition,
                span_with_params: None,
                docs: None,
                signature: None,
                scope: None,
                params: Vec::new(),
                base_classes: Vec::new(),
                type_string: None,
                type_name: None,
                tags: Vec::new(),
            };
            self.doc.symbols.insert(expanded_name, symbol_entry);
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn add_symbol_with_info(
        &mut self,
        node: &Node,
        entry_type: SymbolEntryType,
        with_params: Option<SourceSpan>,
        docs: Option<String>,
        params: Vec<ParameterEntry>,
        type_string: Option<String>,
        mut type_name: Option<TypeReference>,
        symbol_name: Option<String>,
        base_classes: Vec<SymbolReference>,
    ) {
        let content = symbol_name.unwrap_or_else(|| {
            node.utf8_text(self.doc.source.content.as_bytes())
                .unwrap()
                .to_owned()
        });
        if content.contains(' ')
            || content.contains('{')
            || content.contains("::")
            || content.contains('>')
        {
            log::debug!(
                "invalid symbol added, type: {}, parent: {}, entry: {:?}, sexp: {}, content: {}",
                node.kind(),
                node.parent().unwrap().kind(),
                entry_type,
                node.to_sexp(),
                content
            );
            return;
        }

        let extra_scope = self.get_scope_from_identifier(node);
        let scope = join_scope(&self.current_scope, &extra_scope);
        if matches!(
            entry_type,
            SymbolEntryType::ClassConstructorDeclaration
                | SymbolEntryType::ClassConstructorDefinition
                | SymbolEntryType::ClassDefinition
                | SymbolEntryType::ClassDeclaration
        ) {
            type_name = Some(TypeReference {
                scope: scope.map(|scope| Box::new(TypeReference::new(scope))),
                name: content.to_owned(),
                params: Vec::new(),
            });
        }
        let params_string: Option<String> = with_params
            .as_ref()
            .map(|span| span.content(&self.doc.source.content).into_owned());
        let entry = SymbolEntry {
            symbol: content.to_owned(),
            span: SourceSpan::from_node(node),
            span_with_params: with_params,
            docs,
            signature: params_string,
            entry_type,
            scope: join_scope(&self.current_scope, &extra_scope),
            params,
            base_classes,
            type_string,
            type_name,
            tags: Vec::new(),
        };

        self.doc.symbols.insert(content, entry);
    }

    fn add_symbol(&mut self, node: &Node, entry_type: SymbolEntryType) {
        fn find_parent_with_docs(node: Node) -> Option<Node> {
            let mut node = node;
            while let Some(parent) = node.parent() {
                match parent.kind() {
                    "pointer_declarator"
                    | "parenthesized_declarator"
                    | "reference_declarator"
                    | "template_type"
                    | "template_declaration"
                    | "scoped_identifier"
                    | "scoped_type_identifier"
                    | "array_declarator"
                    | "function_declarator"
                    | "template_function"
                    | "identifier"
                    | "type_identifier"
                    | "field_identifier" => {
                        node = parent;
                    }
                    _ => return Some(parent),
                }
            }
            None
        }
        fn find_base_classes(parser: &CppFile, node: Node) -> Vec<SymbolReference> {
            let parent = match find_parent_with_docs(node) {
                Some(node) => node,
                None => return Vec::new(),
            };

            let mut cursor = parent.walk();
            let base_class_clause = parent
                .named_children(&mut cursor)
                .find(|child| child.kind() == "base_class_clause");
            let base_class_clause = match base_class_clause {
                Some(clause) => clause,
                None => return Vec::new(),
            };

            let mut base_classes = Vec::new();
            let mut cursor = base_class_clause.walk();
            for base_class in base_class_clause.named_children(&mut cursor) {
                if let Some(resolved_child) = resolve_name_from_declarator(base_class) {
                    base_classes.push(parser.make_symbol_reference(resolved_child));
                }
            }
            base_classes
        }
        fn find_parent_function_declarator(node: Node) -> Option<Node> {
            let mut node = Some(node);
            while let Some(n) = node {
                match n.kind() {
                    "function_declarator" => return Some(n),
                    "pointer_declarator"
                    | "parenthesized_declarator"
                    | "reference_declarator"
                    | "template_type"
                    | "template_declaration"
                    | "scoped_identifier"
                    | "scoped_type_identifier"
                    | "array_declarator"
                    | "template_function"
                    | "identifier"
                    | "type_identifier"
                    | "field_identifier" => {
                        node = n.parent();
                    }
                    _ => return None,
                }
            }
            None
        }
        let docs = || -> Option<String> {
            let parent = find_parent_with_docs(*node)?;
            let mut doc_text = Vec::new();
            let mut doc_start = parent;

            while let Some(prev) = doc_start.prev_sibling() {
                if prev.kind() == "comment" {
                    doc_start = prev;
                    doc_text.push(
                        doc_start
                            .utf8_text(self.doc.source.content.as_bytes())
                            .unwrap()
                            .to_owned(),
                    );
                } else {
                    break;
                }
            }
            if !doc_text.is_empty() {
                doc_text.reverse();
                let docs = doc_text.join("\n");
                let docs = docs
                    .split('\n')
                    .map(|line| {
                        line.trim()
                            .trim_start_matches("//")
                            .trim_start_matches("/*")
                            .trim_start_matches("*/")
                            .trim_start_matches('*')
                            .trim()
                    })
                    .collect::<Vec<_>>()
                    .join("\n");
                Some(docs)
            } else {
                None
            }
        }();

        let params = match find_parent_function_declarator(*node) {
            Some(p) => self.parse_parameters(p),
            None => Vec::new(),
        };
        let mut type_name = None;
        let type_string = find_parent_with_docs(*node)
            .and_then(|node| node.child_by_field_name("type"))
            .map(|node| {
                type_name = self.get_type_name(&node);
                node.utf8_text(self.doc.source.content.as_bytes())
                    .unwrap()
                    .to_owned()
            });
        self.add_symbol_with_info(
            node,
            entry_type,
            find_parent_with_docs(*node).map(|n| SourceSpan::from_node(&n)),
            docs,
            params,
            type_string,
            type_name,
            None,
            find_base_classes(self, *node),
        )
    }

    fn parse_parameters(&self, node: Node) -> Vec<ParameterEntry> {
        if node.kind() != "function_declarator" {
            return Vec::new();
        }
        let params = match node.child_by_field_name("parameters") {
            Some(param_list) => param_list,
            None => return Vec::new(),
        };

        let mut parsed = Vec::new();
        let mut cursor = params.walk();
        fn text_or_empty(source: &Arc<SourceFile>, node: Option<Node>) -> String {
            match node {
                Some(n) => n.utf8_text(source.content.as_bytes()).unwrap().to_owned(),
                None => "".to_owned(),
            }
        }
        for param in params.named_children(&mut cursor) {
            parsed.push(ParameterEntry {
                content: text_or_empty(&self.doc.source, Some(param)),
                name: text_or_empty(
                    &self.doc.source,
                    param
                        .child_by_field_name("declarator")
                        .and_then(resolve_name_from_declarator),
                ),
                param_type: text_or_empty(&self.doc.source, param.child_by_field_name("type")),
            })
        }
        parsed
    }

    fn process_translation_unit(&mut self, node: Node) {
        let mut cursor = node.walk();

        for child in node.named_children(&mut cursor) {
            self.process_toplevel(child);
        }
    }

    fn process_toplevel(&mut self, node: Node) {
        if node.is_extra() {
            return;
        }

        match node.kind() {
            "preproc_include" => {
                if let Some(include) = self.parse_preproc_include(node) {
                    self.doc.includes.push(IncludedFile {
                        include,
                        span: SourceSpan::from_node(&node),
                    });
                }
            }
            "preproc_call" => {}
            "namespace_definition" => self.process_namespace_definition(node),
            "struct_specifier" | "class_specifier" | "union_specifier" => {
                self.process_class_specifier(node)
            }
            "function_definition" => {
                self.process_function_definition(node, /* is_class */ false)
            }
            "preproc_function_def" => self.process_preproc_function(node),
            "preproc_def" => self.process_preproc_def(node),
            "enum_specifier" => self.process_enum_specifier(node, None),
            "expression_statement" => {}
            "declaration" => self.process_declaration(node),
            "alias_declaration" => self.process_alias_declaration(node),
            "type_definition" => self.process_typedef(node),
            "template_declaration" => {
                if let Some(body) = node.named_child(1) {
                    self.process_toplevel(body);
                }
            }
            "labeled_statement" | "static_assert_declaration" => {
                /* intentionally ignored for now */
            }
            "preproc_if" | "preproc_elif" | "preproc_ifdef" => {
                let mut cursor = node.walk();
                let children: Vec<Node> = node.named_children(&mut cursor).collect();
                for child in &children[1..children.len()] {
                    self.process_toplevel(*child);
                }
            }
            "preproc_else" => {
                let mut cursor = node.walk();
                let children: Vec<Node> = node.named_children(&mut cursor).collect();
                for child in children {
                    self.process_toplevel(child);
                }
            }
            "linkage_specification" => {
                if let Some(body) = node.child_by_field_name("body") {
                    self.process_toplevel(body);
                }
            }
            "declaration_list" => {
                let mut cursor = node.walk();
                for child in node.named_children(&mut cursor) {
                    self.process_toplevel(child);
                }
            }
            _ => {
                log::debug!(
                    "unknown top-level type: {} (parent: {}): {}",
                    node.kind(),
                    node.parent().unwrap().kind(),
                    node.to_sexp()
                );
            }
        }
    }

    fn process_preproc_def(&mut self, node: Node) {
        if let Some(name) = node.child_by_field_name("name") {
            self.add_symbol(&name, SymbolEntryType::MacroDefinition);
        }
    }

    fn process_enum_specifier(&mut self, node: Node, default_value: Option<Node>) {
        if let Some(name) = node.child_by_field_name("name") {
            let body = node.child_by_field_name("body");
            let entry_type = if body.is_some() || default_value.is_some() {
                SymbolEntryType::EnumDefinition
            } else {
                SymbolEntryType::EnumDeclaration
            };
            self.add_symbol(&name, entry_type);

            if let Some(body) = body {
                let name = name
                    .utf8_text(self.doc.source.content.as_bytes())
                    .unwrap()
                    .to_owned();
                let old_scope = self.append_scope(&name, &body);
                self.process_enum_body(body);
                self.current_scope = old_scope;
            } else if let Some(default_value) = default_value {
                let name = name
                    .utf8_text(self.doc.source.content.as_bytes())
                    .unwrap()
                    .to_owned();
                let old_scope = self.append_scope(&name, &default_value);
                self.process_enum_initializer(default_value);
                self.current_scope = old_scope;
            }
        }
    }

    fn process_enum_body(&mut self, body: Node) {
        let mut cursor = body.walk();
        for child in body.named_children(&mut cursor) {
            if child.kind() == "enumerator" {
                if let Some(name) = child.child_by_field_name("name") {
                    self.add_symbol(&name, SymbolEntryType::EnumValue);
                }
            }
        }
    }

    fn process_enum_initializer(&mut self, body: Node) {
        if body.kind() != "initializer_list" {
            return;
        }
        let mut cursor = body.walk();
        for child in body.named_children(&mut cursor) {
            if child.kind() == "assignment_expression" {
                if let Some(name) = child.child_by_field_name("left") {
                    self.add_symbol(&name, SymbolEntryType::EnumValue);
                }
            } else if child.kind() == "identifier" {
                self.add_symbol(&child, SymbolEntryType::EnumValue);
            }
        }
    }

    fn process_alias_declaration(&mut self, node: Node) {
        assert_eq!(node.kind(), "alias_declaration");
        if let Some(name) = node.child_by_field_name("name") {
            self.add_symbol(&name, SymbolEntryType::TypeAlias);
        }
    }

    fn process_declaration(&mut self, node: Node) {
        if let Some(declarator) = node.child_by_field_name("declarator") {
            self.process_declaration_declarator(declarator)
        }
    }

    fn process_declaration_declarator(&mut self, declarator: Node) {
        match declarator.kind() {
            "identifier" => {
                self.add_symbol(&declarator, SymbolEntryType::VariableDeclaration);
            }
            "init_declarator" => {
                if let Some(declarator) = declarator.child_by_field_name("declarator") {
                    if let Some(symbol) = resolve_name_from_declarator(declarator) {
                        self.add_symbol(&symbol, SymbolEntryType::VariableDefinition);
                    }
                }
            }
            "function_declarator" => {
                if let Some(declarator) = declarator.child_by_field_name("declarator") {
                    self.add_symbol(&declarator, SymbolEntryType::FunctionDeclaration);
                }
            }
            "parenthesized_declarator"
            | "reference_declarator"
            | "pointer_declarator"
            | "array_declarator " => {
                if let Some(child) = declarator.named_child(0) {
                    self.process_declaration_declarator(child);
                }
            }
            _ => {
                log::debug!("unknown declaration declarator: {}", declarator.to_sexp())
            }
        }
    }

    fn process_preproc_function(&mut self, node: Node) {
        if let Some(name) = node.child_by_field_name("name") {
            self.add_symbol(&name, SymbolEntryType::MacroDefinition);
        }
        if let Some(params) = node.child_by_field_name("parameters") {
            let mut cursor = params.walk();
            for param in params.children(&mut cursor) {
                if param.kind() == "identifier" {
                    let name = param.utf8_text(self.doc.source.content.as_bytes()).unwrap();
                    let symbol = LocalSymbolEntry {
                        symbol: name.to_owned(),
                        span: SourceSpan::from_node(&param),
                        valid_span: SourceSpan::from_node(&node),
                        type_string: None,
                        type_name: None,
                        infer_types_from: Vec::new(),
                        entry_type: SymbolEntryType::Parameter,
                        is_param_declaration: true,
                    };
                    self.doc.local_symbols.insert(symbol.symbol.clone(), symbol);
                }
            }
        }
    }

    fn process_class_specifier(&mut self, node: Node) {
        let name = match node.child_by_field_name("name") {
            Some(n) => n,
            None => return,
        };
        let name = match resolve_name_from_declarator(name) {
            Some(n) => n,
            None => return,
        };
        let body = node.child_by_field_name("body");
        let entry_type = if body.is_some() {
            SymbolEntryType::ClassDefinition
        } else {
            SymbolEntryType::ClassDeclaration
        };
        self.add_symbol(&name, entry_type);

        let name = name
            .utf8_text(self.doc.source.content.as_bytes())
            .unwrap()
            .to_owned();
        if let Some(body) = body {
            let old_scope = self.append_scope(&name, &body);
            let mut cursor = body.walk();
            for child in body.named_children(&mut cursor) {
                self.process_class_node(child);
            }
            self.current_scope = old_scope;
        }
    }

    fn process_class_node(&mut self, node: Node) {
        if node.is_extra() {
            return;
        }

        match node.kind() {
            "access_specifier" => {}
            "declaration" | "field_declaration" => {
                self.process_field_declaration(node);
            }
            "function_definition" => {
                self.process_function_definition(node, /* is_class */ true);
            }
            "alias_declaration" => self.process_alias_declaration(node),
            "type_definition" => self.process_typedef(node),
            "template_declaration" => {
                let mut cursor = node.walk();
                let mut children = node.named_children(&mut cursor);
                if let Some(body) = children.nth(1) {
                    self.process_toplevel(body);
                }
            }
            "preproc_if"
            | "preproc_else"
            | "preproc_elif"
            | "using_declaration"
            | "static_assert_declaration"
            | "friend_declaration" => {}
            _ => {
                log::debug!(
                    "unknown class node: {} (parent: {}): {} - {}",
                    node.kind(),
                    node.parent().unwrap().kind(),
                    node.to_sexp(),
                    node.utf8_text(self.doc.source.content.as_bytes()).unwrap()
                );
            }
        }
    }

    fn process_typedef(&mut self, node: Node) {
        if let Some(type_name) = node.child_by_field_name("declarator") {
            if let Some(name) = resolve_name_from_declarator(type_name) {
                self.add_symbol(&name, SymbolEntryType::TypeAlias);
            }
        }
    }

    fn process_field_declaration_declarator(&mut self, parent: Node, declarator: Node) {
        match declarator.kind() {
            "field_identifier" => {
                self.add_symbol(&declarator, SymbolEntryType::MemberDeclaration);
            }
            "function_declarator" => {
                let type_name = parent.child_by_field_name("type");
                if let Some(func_declaration) = declarator.child_by_field_name("declarator") {
                    let entry_type = if type_name.is_some() {
                        SymbolEntryType::MethodDeclaration
                    } else {
                        SymbolEntryType::ClassConstructorDeclaration
                    };
                    self.add_symbol(&func_declaration, entry_type);
                }
            }
            "reference_declarator" | "pointer_declarator" => {
                if let Some(child) = resolve_name_from_declarator(declarator) {
                    self.process_field_declaration_declarator(declarator, child)
                }
            }
            _ => {
                log::debug!(
                    "unknown field declarator type: {} (parent: {}): {}",
                    declarator.kind(),
                    declarator.parent().unwrap().kind(),
                    declarator.to_sexp()
                );
            }
        }
    }

    fn process_field_declaration(&mut self, node: Node) {
        if let Some(declarator) = node.child_by_field_name("declarator") {
            self.process_field_declaration_declarator(node, declarator)
        } else if let Some(decl_type) = node.child_by_field_name("type") {
            let default_value = node.child_by_field_name("default_value");
            match decl_type.kind() {
                "enum_specifier" => self.process_enum_specifier(decl_type, default_value),
                "class_specifier" | "struct_specifier" => self.process_class_specifier(decl_type),
                _ => {
                    log::debug!(
                        "unknown field declaration with type: {}: {}",
                        node.kind(),
                        node.to_sexp()
                    );
                }
            }
        } else {
            log::debug!(
                "unknown field declaration: {}: {}",
                node.kind(),
                node.to_sexp()
            );
        }
    }
    fn process_function_definition(&mut self, node: Node, is_class: bool) {
        let return_type = node.child_by_field_name("type");
        if let Some(return_type) = &return_type {
            if return_type.kind() == "class_specifier" || return_type.kind() == "struct_specifier" {
                // FIXME: weird parse?
                return;
            }
        }

        let mut function_declarator = node;
        while function_declarator.kind() != "function_declarator" {
            let first_child = function_declarator.named_child(0);
            let child = function_declarator.child_by_field_name("declarator");
            match child.or(first_child) {
                Some(decl) => {
                    function_declarator = decl;
                }
                None => {
                    log::debug!(
                        "could not find declarator for function definition: {}: {} - {}",
                        node.kind(),
                        node.to_sexp(),
                        node.utf8_text(self.doc.source.content.as_bytes()).unwrap(),
                    );
                    return;
                }
            }
        }

        let mut name_declarator = function_declarator
            .child_by_field_name("declarator")
            .unwrap();
        let mut old_scope = self.current_scope.clone();
        if name_declarator.kind() == "scoped_identifier" {
            let scope = remove_template_args(
                &name_declarator
                    .child_by_field_name("namespace")
                    .map(|node| {
                        node.utf8_text(self.doc.source.content.as_bytes())
                            .unwrap()
                            .to_owned()
                    })
                    .unwrap_or_default(),
            );
            name_declarator = name_declarator.child_by_field_name("name").unwrap();
            if let Some(body) = node.child_by_field_name("body") {
                old_scope = self.append_scope(&scope, &body);
                // add_symbol also handles scoping, can reset here
                self.current_scope = old_scope.clone();
            }
            if let Some(params) = function_declarator.child_by_field_name("parameters") {
                old_scope = self.append_scope(&scope, &params);
                self.current_scope = old_scope.clone();
            }
        }

        let entry_type = if is_class && return_type.is_some() {
            SymbolEntryType::MethodDefinition
        } else if return_type.is_none() {
            SymbolEntryType::ClassConstructorDefinition
        } else {
            SymbolEntryType::FunctionDefinition
        };
        self.add_symbol(&name_declarator, entry_type);
        self.current_scope = old_scope;
    }

    fn process_namespace_definition(&mut self, node: Node) {
        assert_eq!("namespace_definition", node.kind());
        let name = match node.child_by_field_name("name") {
            Some(n) => {
                let old_scope = self.current_scope.clone();
                let text = n
                    .utf8_text(self.doc.source.content.as_bytes())
                    .unwrap()
                    .to_owned();
                for namespace_part in text.split("::") {
                    self.add_symbol_with_info(
                        &n,
                        SymbolEntryType::Namespace,
                        None,
                        None,
                        Vec::new(),
                        None,
                        None,
                        Some(namespace_part.to_owned()),
                        Vec::new(),
                    );
                    if let Some(body) = node.child_by_field_name("body") {
                        self.append_scope(namespace_part, &body);
                    }
                }
                self.current_scope = old_scope;
                text
            }
            None => "".to_owned(),
        };
        if let Some(body) = node.child_by_field_name("body") {
            let old_scope = self.append_scope(&name, &body);
            self.process_toplevel_declaration_list(body);
            self.current_scope = old_scope;
        }
    }
    fn process_toplevel_declaration_list(&mut self, node: Node) {
        assert_eq!(node.kind(), "declaration_list");
        let mut cursor = node.walk();
        for child in node.named_children(&mut cursor) {
            self.process_toplevel(child);
        }
    }

    fn parse_preproc_include(&self, node: Node) -> Option<IncludeFile> {
        let path = node.child_by_field_name("path")?;
        let filename = path.utf8_text(self.doc.source.content.as_bytes()).unwrap();
        let filename = filename
            .trim_start_matches(|c| c == '<' || c == '"')
            .trim_end_matches(|c| c == '>' || c == '"');

        match path.kind() {
            "string_literal" => Some(IncludeFile::Source(filename.to_owned())),
            "system_lib_string" => Some(IncludeFile::System(filename.to_owned())),
            _ => None,
        }
    }

    fn append_scope(&mut self, name: &str, node: &Node) -> Option<String> {
        if name.is_empty() {
            self.current_scope.clone()
        } else {
            let old_scope = self.current_scope.take();
            let new_scope = match &old_scope {
                Some(s) => format!("{}::{}", s, name.replace("__", "::")),
                None => name.to_owned(),
            };
            self.doc.scopes.push(ScopeSpan {
                scope: new_scope.clone(),
                span: SourceSpan::from_node(node),
            });
            self.current_scope = Some(new_scope);
            old_scope
        }
    }
}

fn is_call_expression(node: &Node) -> bool {
    let mut node = *node;
    while let Some(parent) = node.parent() {
        if !matches!(parent.kind(), "field_expression" | "scoped_identifier") {
            return parent.kind() == "call_expression";
        }
        node = parent;
    }
    false
}

#[cfg(test)]
mod test {
    use lsp_types::Position;

    use crate::cpp_file::{CppFile, IncludeFile, SymbolEntryType};
    use crate::diagnostics::DiagnosticType;
    use crate::document::ParsedFile;
    use crate::span::SourceFile;
    use std::sync::Arc;

    fn make_index(content: &str) -> Arc<CppFile> {
        CppFile::parse_from_file(Arc::new(SourceFile::new(
            "test_file.cpp".to_owned(),
            content.to_owned(),
        )))
    }

    #[test]
    fn parse_simple() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let symbols = index.doc.find_symbol("method");

        assert_eq!(symbols.len(), 2);
        assert_eq!(symbols[0].entry_type, SymbolEntryType::MethodDeclaration);
        assert_eq!(symbols[0].span.begin.row, 11);

        // TODO: this is a method, but parsed as a function
        assert_eq!(symbols[1].entry_type, SymbolEntryType::FunctionDefinition);
        assert_eq!(symbols[1].scope, Some("Foo".to_owned()));
        assert_eq!(symbols[1].span.begin.row, 17);

        assert_eq!(index.doc.includes.len(), 3);
        assert_eq!(
            index.doc.includes[0].include,
            IncludeFile::System("iostream".to_owned())
        );
    }

    #[test]
    fn parse_docs() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));

        let symbols = index.doc.find_symbol("funcWithDocs");
        assert_eq!(symbols.len(), 1);
        assert!(symbols[0].docs.as_ref().unwrap().contains("Doc block"));
        assert!(symbols[0]
            .signature
            .as_ref()
            .unwrap()
            .contains("std::string p1"));
        assert!(!symbols[0].signature.as_ref().unwrap().contains("Doc block"));
    }

    #[test]
    fn nested_namespace() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let symbols = index.doc.find_symbol("inNamespace2");
        assert_eq!(symbols.len(), 1);
        assert_eq!(symbols[0].scope, Some("nest::ed".to_owned()));

        let symbols = index.doc.find_symbol("fooBar");
        assert_eq!(symbols.len(), 1);
        assert_eq!(symbols[0].scope, Some("a::b::AbFoo".to_owned()));
    }

    #[test]
    fn type_names() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let symbols = index.doc.find_symbol("foo_");
        assert_eq!(symbols.len(), 1);
        let symbol = &symbols[0];
        assert_eq!(
            symbol.type_name.as_ref().unwrap().type_string(),
            "std::shared_ptr<Foo>",
        );
    }

    #[test]
    fn enum_values() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let symbols = index.doc.find_symbol("ENUM_A");
        assert_eq!(symbols.len(), 1);
        assert_eq!(symbols[0].scope, Some("Status".to_owned()));

        let symbols = index.doc.find_symbol("ERROR_CODE");
        assert_eq!(symbols.len(), 1);
        assert_eq!(symbols[0].scope, Some("Foo::Inner".to_owned()));
    }

    #[test]
    fn signatures() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let symbols = index.doc.find_symbol("funcWithDocs");
        assert_eq!(symbols.len(), 1);
        let symbol = symbols[0].clone();
        assert_eq!(symbol.params.len(), 3);
        assert_eq!(symbol.params[0].name, "p1");
        assert_eq!(symbol.params[1].name, "p2");
        assert_eq!(symbol.params[2].name, "f");
        assert_eq!(symbol.type_string, Some("void".to_owned()));

        assert_eq!(symbol.params[0].param_type, "std::string");
        assert_eq!(symbol.params[1].param_type, "size_t");
        // TBD: parses as Foo instead of Foo&
        assert_eq!(symbol.params[2].param_type, "Foo");
    }

    #[test]
    fn base_classes() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let with_bases = index.doc.find_symbol("WithBases");
        assert_eq!(with_bases.len(), 1);
        let with_bases = with_bases.first().unwrap();
        assert_eq!(with_bases.base_classes.len(), 2);
        assert_eq!(with_bases.base_classes[0].symbol, "Foo");
        assert_eq!(with_bases.base_classes[1].symbol, "Bar");
    }

    #[test]
    fn type_aliases() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let type_aliases = index.doc.find_symbol("TypeAlias");
        assert_eq!(type_aliases.len(), 1);
        assert_eq!(
            type_aliases[0].type_name.as_ref().unwrap().name,
            "WithBases"
        );
    }

    #[test]
    fn syntax_errors() {
        let index = make_index(include_str!("test_files/test_errors.cpp"));
        let errors = &index.doc.diagnostics;
        assert_eq!(errors.len(), 2);
        assert_eq!(errors[0].diagnostic_type, DiagnosticType::SyntaxError);
        assert!(errors[1].message.contains("missing ;"));
    }

    #[test]
    fn symbol_references() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let refs = index.doc.find_symbol_references("inNamespace");
        assert_eq!(refs.len(), 4);

        let call_refs = refs.iter().filter(|reference| reference.is_call).count();
        assert_eq!(call_refs, 3);

        let refs = index.doc.find_symbol_references("method");
        assert_eq!(refs.len(), 3);

        let call_refs = refs.iter().filter(|reference| reference.is_call).count();
        assert_eq!(call_refs, 1);

        let refs = index.doc.find_symbol_references("function");
        assert_eq!(refs.len(), 2);

        let call_refs = refs.iter().filter(|reference| reference.is_call).count();
        assert_eq!(call_refs, 1);

        let refs = index.doc.find_symbol_references("A1");
        assert_eq!(refs.len(), 3);
        for reference in refs {
            assert!(reference.local_reference);
        }
    }

    #[test]
    fn scopes_at_point() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let scopes = index.doc.find_scopes(&Position::new(39, 0));
        assert_eq!(scopes[0].scope, "n1");
    }

    #[test]
    fn local_symbols() {
        let index = make_index(include_str!("test_files/test_simple.cpp"));
        let locals = index.doc.find_local_symbol("Type");
        assert_eq!(locals.len(), 1);

        let locals = index.doc.find_local_symbol("templateMethodParam");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::Parameter);
        assert!(locals[0].type_string.as_ref().unwrap().contains("A1"));

        let locals = index.doc.find_local_symbol("lambdaParam");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::Parameter);
        assert!(locals[0].type_string.as_ref().unwrap().contains("bool"));

        let locals = index.doc.find_local_symbol("localVariable2");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::VariableDefinition);
        assert!(locals[0].type_string.as_ref().unwrap().contains("bool"));

        let locals = index.doc.find_local_symbol("forEachVar");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::VariableDefinition);
        assert!(locals[0].type_string.as_ref().unwrap().contains("auto"));

        let locals = index.doc.find_local_symbol("forVar");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::VariableDefinition);
        assert!(locals[0].type_string.as_ref().unwrap().contains("int"));

        let locals = index.doc.find_local_symbol("TypeWithDefault");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::TemplateType);

        let locals = index.doc.find_local_symbol("ifStatementVar");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::VariableDefinition);

        let locals = index.doc.find_local_symbol("lambdaCapture");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].entry_type, SymbolEntryType::VariableDefinition);

        let locals = index.doc.find_local_symbol("inferType");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].infer_types_from, vec!["inNamespace".to_owned()]);

        let locals = index.doc.find_local_symbol("inferType2");
        assert_eq!(locals.len(), 1);
        assert_eq!(locals[0].infer_types_from, vec!["foo_".to_owned()]);
    }
}
