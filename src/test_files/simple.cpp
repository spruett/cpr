// leading comment

#include "src/test_files/gen-thrift/Simple.h"
#include <folly/Synchronized.h>
#include <folly/executors/CPUThreadPoolExecutor.h>
#include <iostream>

struct Foo {
  int code_ref() { return 0; }
  int code_ref(int);
};

std::string baz2() { auto client = std::make_unique<RpcServiceAsyncClient>(); }

std::string baz(ThriftStruct t) {
  T2 t;
  Inner i;
}

void baz3() {
  std::string Foo;
  use(Foo);
}

std::shared_ptr<Foo> makeFoo();

int main() {
  baz();
  return 0;
}