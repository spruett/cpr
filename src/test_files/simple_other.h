namespace other {

struct Simple {
    void foo() {}
    std::string member;
};

Simple makeSimple(int i, byte b) {
    return Simple::foo();
}
}