// @generated
include "my/thrift.thrift"
cpp_include "my/lib.h"

namespace cpp2 test.file


struct ThriftStruct {
  1: optional i32 (cpp2.template = 'folly::F14FastMap') my_int,
  2: StatusCode code,
  3: binary data = "with default",
  4: list<string> list_name = []
}

/**
 * Documentation
 */
enum StatusCode {
	OK = 1,
	BAD = 2
  THIRD = 3
} (
  cpp.test,
  cpp.test2 = "three",
)

union U {
  1: i32 opt1,
  2: string opt2,
  3: i32 more
  4: i64 options
}

const i32 MY_CONST = "string";


exception Ex {}

/* comment */
service RpcService {
	StatusCode foo(1: ThriftStruct t) throws (1: Ex e);

	StatusCode foo2(1: ThriftStruct t) throws (1: Ex e)
	StatusCode foo3(1: ThriftStruct t) throws (1: Ex e);
	Payload, sink<StatusCode, Payload throws (1: Ex ex)> foo4(1: ThriftStruct t, 2: ThriftStruct t2) throws (1: Ex e),
  stream<StatusCode> stream(1: ThriftStruct t1);
}