// @generated
struct Inner {
  1: list<string> vals,
}
struct ThriftStruct {
    1: optional i32 field1,
    1: binary field2 = "",
    2: Inner i
    3: Inner i2
}

struct T2{}

enum StatusCode {
  OK = 1,
  BAD = 2
  THIRD = 3
}