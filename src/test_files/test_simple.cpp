#include <iostream>
#include <string>
#include <vector>

struct Bar;

struct Foo : FooBar<Bar> {
  enum Inner {
    ERROR_CODE = 1,
  };
  using B = Bar;
  int method();
  int x_{0};
};

std::string function(std::vector<int> x);

int Foo::method() {
  AbFoo::fooBar();
  return 0;
}

// Has documentation in the form
// of many lines
//
// of comments
struct WithDocumentation {
};

/**
 * Doc block style comments
 */
void funcWithDocs(
   std::string p1,
   size_t p2,
   Foo& f
);

namespace n1 {
std::string inNamespace(int p) {}
}

namespace nest::ed {
std::string inNamespace2(int p) {}
} // namespace nest::ed

namespace a::b {
class AbFoo {
  int fooBar() { 
    auto x = n1::inNamespace(nest::ed::inNamespace2(3));
    n1::inNamespace(2);
    Foo f;
    f.method();
    function({});
    return 0;
  }
};
}

struct FooBar;

namespace z {
class FOLLY_NODISCARD TaskZ {
    void methodZ();
};
}


enum class Status {
  ENUM_A = 1,
  ENUM_B = 2,
};

template <typename Type, size_t Size>
class TemplateClass {
  template <typename A1>
  A1 templateMethod(A1&& templateMethodParam, std::string bar) {
    auto localVariable = 1;
    if (localVariable) {
      bool localVariable2 = false;
      templateMethodParam.foo();
    }
    auto localLambda = [&lambdaCapture = localVariable, lambdaCap2 = bar](bool lambdaParam, std::string lP2) {
      auto localVariable3 = "string";
      lambdaCapture;
      lambdaCap2;
    };
    for (auto forEachVar : y) {
      auto z = 2;
      forEachVar();
    }
    for (int forVar = 0; i < 3;) {
      if (forVar < 3) { bar(); }
    }

    auto& [a, b] = foo();

    project_my_macro_var;
  }
};

template <typename TypeWithDefault = void>
class TemplateClassWithDefault {
  void methodWithDefaults(size_t value = 0, std::string var = foo()) {
    using String = std::string;
    value++;
    if (auto ifStatementVar = 2) {
    } else if (auto elifStatementVar = 3) {
    }
    auto inferType = n1::inNamespace();
    auto inferType2 = thing->foo_;
    auto inferType3 = thing->baz_;
    try {
      foo();
    } catch (const std::exception& exc) {
      exc.what();
    }
    thing->foo_()->foo();

    while (auto inLoop = methodDecl()) {
      inLoop();
    }
  }

  void methodDecl(int x);
};

DEFINE_VARIABLE(project, my_macro_var, std::string, "", "");

using namespace foo::bar;
using foo::bar;

class WithBases : public Foo, private Bar<Foo> {
  std::shared_ptr<Foo> foo_;
  TemplateType<T>::Foo<D>::Bar baz_;
};

using TypeAlias = WithBases;

#define MACRO_FN(p, p2) p + p2
