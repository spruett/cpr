use lsp_types::{Position, TextDocumentContentChangeEvent};

#[allow(dead_code)]
pub struct LineIndex {
    char_offsets: Vec<usize>,
    byte_offsets: Vec<usize>,
}
#[allow(dead_code)]
pub enum LineOffsetIndex {
    Char(usize),
    Byte(usize),
}

#[allow(dead_code)]
impl LineIndex {
    pub fn new(content: &str) -> Self {
        let mut char_offsets = Vec::new();
        let mut byte_offsets = Vec::new();

        let mut char_idx = 0;
        let mut last_byte_idx = 0;
        char_offsets.push(0);
        byte_offsets.push(0);
        for (byte_idx, c) in content.char_indices() {
            char_idx += 1;
            last_byte_idx = byte_idx + 1;
            if c == '\n' {
                char_offsets.push(char_idx);
                byte_offsets.push(byte_idx);
            }
        }
        char_offsets.push(char_idx);
        byte_offsets.push(last_byte_idx);

        Self {
            char_offsets,
            byte_offsets,
        }
    }

    fn lines(&self) -> usize {
        self.char_offsets.len()
    }

    fn line_at<'a>(&self, line: usize, content: &'a str) -> &'a str {
        let start_pos = self.char_offsets[line];
        let end_pos = self.char_offsets[line + 1];

        &content[start_pos..end_pos]
    }

    pub fn find_char_offset(&self, pos: &Position) -> usize {
        let line = pos.line as usize;
        let col = pos.character as usize;
        if let Some(offset) = self.char_offsets.get(line) {
            *offset + col
        } else {
            *self.char_offsets.last().unwrap() + col
        }
    }

    pub fn find_position(&self, offset: LineOffsetIndex) -> Position {
        let (offset_vec, offset) = match offset {
            LineOffsetIndex::Byte(b) => (&self.byte_offsets, b),
            LineOffsetIndex::Char(c) => (&self.char_offsets, c),
        };
        let line_number = offset_vec.partition_point(|line_offset| *line_offset <= offset) - 1;
        let line_offset = offset_vec[line_number];

        Position {
            line: line_number as u32,
            character: (offset - line_offset) as u32,
        }
    }

    fn line_containing_char_offset<'a>(&self, content: &'a str, offset: usize) -> &'a str {
        let pos = self.find_position(LineOffsetIndex::Char(offset));
        let start_pos = self.char_offsets[pos.line as usize];
        let end_pos = if let Some(pos) = self.char_offsets.get((pos.line + 1) as usize) {
            *pos
        } else {
            usize::MAX
        };

        if end_pos >= content.len() {
            &content[start_pos..]
        } else {
            &content[start_pos..end_pos]
        }
    }

    fn line_containing_pointer_to_char(&self, offset: usize) -> String {
        let pos = self.find_position(LineOffsetIndex::Char(offset));
        " ".repeat(pos.character as usize) + "^"
    }
}

pub fn apply_text_change(
    content: &str,
    change_events: &mut Vec<TextDocumentContentChangeEvent>,
) -> String {
    change_events.sort_by_key(|event| {
        event
            .range
            .map(|range| (range.start.line, range.start.character))
    });
    let line_index = LineIndex::new(content);

    let chars: Vec<char> = content.chars().collect();
    let mut output = Vec::new();

    let mut current_index = 0;
    for change_event in change_events {
        match change_event.range {
            Some(edit_range) => {
                let start_index = line_index.find_char_offset(&edit_range.start);
                let end_index = line_index.find_char_offset(&edit_range.end);

                output.extend_from_slice(&chars[current_index..start_index]);
                output.extend_from_slice(&change_event.text.chars().collect::<Vec<char>>());

                current_index = end_index;
            }
            None => return change_event.text.clone(),
        }
    }
    output.extend_from_slice(&chars[current_index..chars.len()]);
    output.into_iter().collect()
}

#[derive(Debug, Default, Clone)]
pub struct IdentifierResult {
    pub identifier: String,
    pub full_identifier: String,
    pub scope: Option<String>,
    pub is_function_call: bool,
    pub is_member: bool,
    pub commas: usize,
    pub field_name_before: Option<String>,
}

fn is_valid_for_identifier(c: char) -> bool {
    c.is_alphanumeric() || c == '_'
}

fn scope_before(chars: &[char], start_index: usize) -> Option<String> {
    if start_index <= 2 {
        return None;
    }
    let current = chars.get(start_index)?;
    let prev = chars.get(start_index - 1)?;
    if *current != ':' || *prev != ':' {
        return None;
    }

    let mut index = start_index - 2;
    while index > 0 && is_valid_for_identifier(chars[index - 1]) {
        index -= 1;
    }
    let scope: String = chars[index..start_index - 1].iter().collect();
    let scope = match scope_before(chars, index - 1) {
        Some(prev) => format!("{}::{}", prev, scope),
        None => scope,
    };
    Some(scope)
}

fn identifier_after(chars: &[char], start_index: usize) -> (String, char) {
    let mut index = start_index;
    while index < chars.len() {
        if !is_valid_for_identifier(chars[index]) {
            break;
        }
        index += 1;
    }
    (
        chars[start_index..index].iter().collect(),
        chars.get(index).cloned().unwrap_or_default(),
    )
}

fn field_before(
    content: &str,
    chars: &[char],
    index: usize,
    line_index: &LineIndex,
) -> Option<String> {
    let pos = line_index.find_position(LineOffsetIndex::Char(index));
    let result = if chars.get(index) == Some(&')') && index > 0 {
        identifier_before_paren(content, pos)
    } else {
        identifier_before(content, pos)
    };
    if result.full_identifier.is_empty() {
        None
    } else {
        Some(result.full_identifier)
    }
}

pub fn identifier_before(content: &str, pos: Position) -> IdentifierResult {
    let line_index = LineIndex::new(content);
    let chars: Vec<char> = content.chars().collect();
    let start_index = line_index.find_char_offset(&pos);
    match chars.get(start_index.saturating_sub(1)) {
        Some('.') => {
            return IdentifierResult {
                is_member: true,
                field_name_before: field_before(
                    content,
                    &chars,
                    start_index.saturating_sub(2),
                    &line_index,
                ),
                ..Default::default()
            }
        }
        Some(':') => {
            let scope = scope_before(&chars, start_index.saturating_sub(1));
            return match scope {
                Some(s) => IdentifierResult {
                    scope: Some(s),
                    ..Default::default()
                },
                None => IdentifierResult {
                    identifier: ":".to_owned(),
                    ..Default::default()
                },
            };
        }
        Some('>') => {
            if chars.get(start_index.saturating_sub(2)) != Some(&'-') {
                return IdentifierResult {
                    identifier: ">".to_owned(),
                    ..Default::default()
                };
            }
        }
        None => return IdentifierResult::default(),
        _ => {}
    };

    let mut index = start_index;
    while index > 0 && is_valid_for_identifier(chars[index.saturating_sub(1)]) {
        index -= 1;
    }
    let identifier: String = chars[index..start_index].iter().collect();
    let (is_member, field_before) = if index >= 2 && chars.get(index - 1) == Some(&'.') {
        (true, field_before(content, &chars, index - 2, &line_index))
    } else if index >= 3 && chars.get(index - 2) == Some(&'-') && chars.get(index - 1) == Some(&'>')
    {
        (true, field_before(content, &chars, index - 3, &line_index))
    } else {
        (false, None)
    };

    let (after, char_after) = identifier_after(&chars, start_index);

    let full_identifier = identifier.clone() + &after;
    IdentifierResult {
        identifier,
        full_identifier,
        is_function_call: char_after == '(',
        is_member,
        scope: scope_before(&chars, index.saturating_sub(1)),
        field_name_before: field_before,
        commas: 0,
    }
}
pub fn identifier_before_paren(content: &str, pos: Position) -> IdentifierResult {
    let line_index = LineIndex::new(content);
    let chars: Vec<char> = content.chars().collect();
    let mut start_index = line_index.find_char_offset(&pos);
    if chars[start_index] == ')' {
        start_index -= 1;
    }

    let mut paren_balance = 0;
    let mut commas = 0;
    while start_index > 0 {
        match chars[start_index] {
            '(' if paren_balance == 0 => break,
            '(' => paren_balance -= 1,
            ')' => paren_balance += 1,
            '{' | '}' | ';' => return IdentifierResult::default(),
            ',' if paren_balance == 0 => commas += 1,
            _ => {}
        }
        start_index -= 1;
    }
    let result = identifier_before(
        content,
        line_index.find_position(LineOffsetIndex::Char(start_index)),
    );
    IdentifierResult { commas, ..result }
}

pub fn include_file_before(content: &str, pos: Position) -> Option<String> {
    let line_index = LineIndex::new(content);
    let line = line_index.line_containing_char_offset(content, line_index.find_char_offset(&pos));
    if line.starts_with("#include") {
        let start_of_include = line.chars().position(|c| c == '<' || c == '"')?;
        Some(
            line[start_of_include + 1..]
                .trim_end_matches(|c| c == '>' || c == '"' || c == '\n')
                .to_owned(),
        )
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use lsp_types::Position;

    use crate::text_util::identifier_before;

    #[test]
    pub fn field_test() {
        let content = "\
this->is_a_method(foo()).submethod();
        ";
        let result = identifier_before(content, Position::new(0, 8));
        assert_eq!(result.full_identifier, "is_a_method");
        assert_eq!(result.field_name_before, Some("this".to_owned()));
        let result = identifier_before(content, Position::new(0, 32));
        assert_eq!(result.full_identifier, "submethod");
        assert_eq!(result.field_name_before, Some("is_a_method".to_owned()));
    }
}
