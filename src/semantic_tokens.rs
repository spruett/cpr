use std::collections::HashMap;

use lazy_static::lazy_static;
use lsp_types::SemanticToken as LspSemanticToken;
use lsp_types::{Range, SemanticTokenType};
use multimap::MultiMap;

use crate::{
    document::Document,
    indexer::Indexer,
    progress::ProgressItem,
    span::SourceSpan,
    symbols::{scope_match, LocalSymbolEntry, SymbolEntry, SymbolEntryType, SymbolReference},
};

pub struct SemanticToken {
    span: SourceSpan,
    token_type: SymbolEntryType,
}

fn resolve_local_reference<'a>(
    document: &'a Document,
    reference: &SymbolReference,
) -> Option<&'a LocalSymbolEntry> {
    let local_symbols = document.local_symbols.get_vec(&reference.symbol)?;
    local_symbols
        .iter()
        .filter(|local_symbol| local_symbol.valid_span.contains_span(&reference.span))
        .max_by_key(|local_symbol| local_symbol.span.begin)
}

fn resolve_global_symbol_reference<'a>(
    symbols: &'a MultiMap<String, SymbolEntry>,
    reference: &SymbolReference,
) -> Option<&'a SymbolEntry> {
    let global_symbols = symbols.get_vec(&reference.symbol)?;
    global_symbols
        .iter()
        .filter(|symbol| scope_match(&symbol.scope, &reference.scope))
        .max_by_key(|symbol| symbol.entry_type.declaration_match_rank())
}

async fn resolve_semantic_tokens<F>(document: &Document, reference_filter: F) -> Vec<SemanticToken>
where
    F: Fn(&SymbolReference) -> bool,
{
    let all_symbols = Indexer::get()
        .get_all_symbols(document.source.filename.clone(), ProgressItem::noop())
        .await;
    let mut symbols_by_name = MultiMap::new();
    for ranked_symbol in all_symbols {
        symbols_by_name.insert(ranked_symbol.value.1.symbol.clone(), ranked_symbol.value.1);
    }

    let mut semantic_tokens = Vec::new();
    for (_name, references) in document.symbol_references.iter_all() {
        for reference in references {
            if !reference_filter(reference) {
                continue;
            }

            if reference.local_reference {
                if let Some(local_symbol) = resolve_local_reference(document, reference) {
                    semantic_tokens.push(SemanticToken {
                        span: reference.span.clone(),
                        token_type: local_symbol.entry_type.clone(),
                    });
                }
            } else if let Some(global_symbol) =
                resolve_global_symbol_reference(&symbols_by_name, reference)
            {
                semantic_tokens.push(SemanticToken {
                    span: reference.span.clone(),
                    token_type: global_symbol.entry_type.clone(),
                });
            }
        }
    }
    semantic_tokens.sort_by_key(|token| (token.span.begin.row, token.span.begin.column));
    semantic_tokens
}

pub async fn semantic_tokens(document: &Document) -> Vec<SemanticToken> {
    resolve_semantic_tokens(document, |_| true).await
}

pub async fn semantic_tokens_for_range(document: &Document, range: &Range) -> Vec<SemanticToken> {
    resolve_semantic_tokens(document, |reference| reference.span.in_range(range)).await
}

fn get_token_type(entry_type: &SymbolEntryType) -> SemanticTokenType {
    match entry_type {
        SymbolEntryType::ClassDeclaration
        | SymbolEntryType::ClassDefinition
        | SymbolEntryType::ClassConstructorDeclaration
        | SymbolEntryType::ClassConstructorDefinition => SemanticTokenType::CLASS,
        SymbolEntryType::EnumDeclaration | SymbolEntryType::EnumDefinition => {
            SemanticTokenType::ENUM
        }
        SymbolEntryType::EnumValue => SemanticTokenType::ENUM_MEMBER,
        SymbolEntryType::VariableDefinition | SymbolEntryType::VariableDeclaration => {
            SemanticTokenType::VARIABLE
        }
        SymbolEntryType::MacroDefinition => SemanticTokenType::MACRO,
        SymbolEntryType::FunctionDefinition | SymbolEntryType::FunctionDeclaration => {
            SemanticTokenType::FUNCTION
        }
        SymbolEntryType::MethodDeclaration | SymbolEntryType::MethodDefinition => {
            SemanticTokenType::METHOD
        }
        SymbolEntryType::MemberDeclaration => SemanticTokenType::PROPERTY,
        SymbolEntryType::Namespace => SemanticTokenType::NAMESPACE,
        SymbolEntryType::TypeAlias => SemanticTokenType::CLASS,
        SymbolEntryType::TemplateType => SemanticTokenType::TYPE_PARAMETER,
        SymbolEntryType::Parameter => SemanticTokenType::PARAMETER,
    }
}

pub fn get_token_legend() -> &'static Vec<SemanticTokenType> {
    lazy_static! {
        static ref LEGEND_COMPRESSED: Vec<SemanticTokenType> = vec![
            SemanticTokenType::CLASS,
            SemanticTokenType::ENUM,
            SemanticTokenType::ENUM_MEMBER,
            SemanticTokenType::VARIABLE,
            SemanticTokenType::MACRO,
            SemanticTokenType::FUNCTION,
            SemanticTokenType::METHOD,
            SemanticTokenType::PROPERTY,
            SemanticTokenType::TYPE,
            SemanticTokenType::NAMESPACE,
            SemanticTokenType::STRUCT,
            SemanticTokenType::TYPE_PARAMETER,
            SemanticTokenType::PARAMETER,
        ];
    };
    &*LEGEND_COMPRESSED
}

fn get_token_legend_map() -> &'static HashMap<String, usize> {
    lazy_static! {
        static ref LEGEND: HashMap<String, usize> = {
            let mut items = HashMap::new();
            for (idx, item) in get_token_legend().iter().enumerate() {
                items.insert(item.as_str().to_owned(), idx);
            }
            items
        };
    };
    &*LEGEND
}

pub fn compress_semantic_tokens(tokens: &[SemanticToken]) -> Vec<LspSemanticToken> {
    let mut compressed = Vec::new();
    let mut prev_span = (0, 0);
    for token in tokens {
        let delta_line = token.span.begin.row - prev_span.0;
        let delta_char = if prev_span.0 == token.span.begin.row {
            token.span.begin.column - prev_span.1
        } else {
            token.span.begin.column
        };
        let length = token.span.end_byte - token.span.begin_byte;
        let token_type = get_token_type(&token.token_type);
        let compressed_type = *get_token_legend_map().get(token_type.as_str()).unwrap();

        compressed.push(LspSemanticToken {
            delta_line: delta_line as u32,
            delta_start: delta_char as u32,
            length: length as u32,
            token_type: compressed_type as u32,
            token_modifiers_bitset: 0,
        });
        prev_span = (token.span.begin.row, token.span.begin.column);
    }
    compressed
}
