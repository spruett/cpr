use std::sync::Arc;

use crate::{
    diagnostics::{Diagnostic, DiagnosticType},
    document::{Document, IncludedFile, ParsedFile},
    span::{SourceFile, SourceSpan},
    symbols::{IncludeFile, SymbolAtPoint, SymbolEntry, SymbolEntryType, SymbolTag},
    text_util::{identifier_before, LineIndex, LineOffsetIndex},
};
use lsp_types::Position;
use multimap::MultiMap;
use pest::Parser;
use pest::{
    error::LineColLocation,
    iterators::{Pair, Pairs},
};
use tree_sitter::{Node, Point};

#[allow(clippy::upper_case_acronyms)]
mod thrift_pest {
    #[derive(Parser)]
    #[grammar = "grammars/thrift.pest"]
    pub(crate) struct ThriftParser;
}

use crate::symbols::ParameterEntry;
use thrift_pest::{Rule, ThriftParser};

pub struct ThriftFile {
    doc: Document,
    line_index: LineIndex,

    namespace: Option<String>,
}

fn span_from_pair(line_index: &LineIndex, pair: &Pair<Rule>) -> SourceSpan {
    let span = pair.as_span();
    let start_pos = line_index.find_position(LineOffsetIndex::Byte(span.start_pos().pos()));
    let end_pos = line_index.find_position(LineOffsetIndex::Byte(span.end_pos().pos()));
    SourceSpan {
        begin: Point::new(start_pos.line as usize, start_pos.character as usize),
        end: Point::new(end_pos.line as usize, end_pos.character as usize),
        begin_byte: span.start(),
        end_byte: span.end(),
    }
}

fn join_scope(namespace: &Option<String>, scope: Option<&str>) -> Option<String> {
    match (namespace, scope) {
        (Some(ns), Some(s)) => Some(format!("{}::{}", ns, s)),
        (Some(ns), None) => Some(ns.to_owned()),
        (None, Some(s)) => Some(s.to_owned()),
        (None, None) => None,
    }
}

fn join_strings(a: Option<String>, b: Option<String>) -> String {
    let mut strings = Vec::new();
    strings.extend(a.into_iter());
    strings.extend(b.into_iter());
    strings.join(" ")
}

impl ThriftFile {
    fn add_symbol(&mut self, pair: Pair<Rule>, entry_type: SymbolEntryType) {
        self.add_scoped_symbol(
            pair,
            |name| vec![(name.to_owned(), entry_type, Vec::new(), Vec::new())],
            None,
            None,
        );
    }

    fn add_scoped_symbol<F>(
        &mut self,
        pair: Pair<Rule>,
        translate_name: F,
        scope: Option<&str>,
        docs: Option<String>,
    ) where
        F: FnOnce(&str) -> Vec<(String, SymbolEntryType, Vec<SymbolTag>, Vec<ParameterEntry>)>,
    {
        let symbol_entries = translate_name(pair.as_str());
        for (symbol, entry_type, tags, params) in symbol_entries {
            let entry = SymbolEntry {
                symbol: symbol.clone(),
                entry_type,
                span: span_from_pair(&self.line_index, &pair),
                docs: docs.clone(),
                scope: join_scope(&self.namespace, scope),
                span_with_params: None,
                signature: None,
                params,
                base_classes: Vec::new(),
                type_string: None,
                type_name: None,
                tags,
            };
            self.doc.symbols.insert(symbol, entry);
        }
    }

    fn process_thrift_file(&mut self, pairs: Pairs<Rule>) {
        for thrift_file in pairs {
            for child in thrift_file.into_inner() {
                match child.as_rule() {
                    Rule::header => {
                        self.process_header(child.into_inner().next().unwrap());
                    }
                    Rule::body_item => {
                        self.process_body_item(child.into_inner().next().unwrap());
                    }
                    _ => {}
                }
            }
        }
    }

    fn process_header(&mut self, pair: Pair<Rule>) {
        match pair.as_rule() {
            Rule::namespace_defn => {
                self.process_namespace_defn(pair);
            }
            Rule::include_defn => {
                let mut children = pair.into_inner();
                let filename = children.next().unwrap();

                self.process_include(
                    filename.as_str(),
                    span_from_pair(&self.line_index, &filename),
                );
            }
            Rule::cpp_include_defn => {
                let mut children = pair.into_inner();
                let filename = children.next().unwrap();
                self.process_include(
                    filename.as_str(),
                    span_from_pair(&self.line_index, &filename),
                );
            }
            _ => {
                log::error!(
                    "unknown header rule {:?}: {}",
                    pair.as_rule(),
                    pair.as_str()
                );
            }
        }
    }

    fn process_body_item(&mut self, pair: Pair<Rule>) {
        match pair.as_rule() {
            Rule::struct_defn | Rule::exception_defn => {
                self.process_struct_defn(pair);
            }
            Rule::union_defn => {
                self.process_union_defn(pair);
            }
            Rule::service_defn => {
                self.process_service_defn(pair);
            }
            Rule::enum_defn => {
                self.process_enum_defn(pair);
            }
            Rule::typedef => {
                // TODO: typedefs
            }
            _ => {}
        }
    }

    fn process_enum_defn(&mut self, pair: Pair<Rule>) {
        let mut inner = pair.into_inner();
        let name = inner.next().unwrap();
        let members = inner.next().unwrap();
        self.add_symbol(name.clone(), SymbolEntryType::EnumDefinition);

        for member in members.into_inner() {
            if member.as_rule() == Rule::enum_item {
                self.process_enum_member(member, name.as_str());
            }
        }
    }

    fn process_enum_member(&mut self, pair: Pair<Rule>, enum_name: &str) {
        let mut inner = pair.into_inner();
        let identifier = inner.next().unwrap();
        let value = inner.next().map(|pair| pair.as_str().to_owned());

        self.add_scoped_symbol(
            identifier,
            |name| {
                vec![(
                    name.to_owned(),
                    SymbolEntryType::EnumValue,
                    Vec::new(),
                    Vec::new(),
                )]
            },
            Some(enum_name),
            value,
        );
    }

    fn process_struct_defn(&mut self, pair: Pair<Rule>) {
        let mut inner = pair.into_inner();

        let name = inner.next().unwrap();
        self.add_symbol(name.clone(), SymbolEntryType::ClassDefinition);
        let fields = inner.next().unwrap();

        for field in fields.into_inner() {
            if field.as_rule() == Rule::field {
                self.process_struct_field(field, name.as_str());
            }
        }
    }

    fn process_struct_field(&mut self, pair: Pair<Rule>, struct_name: &str) {
        let mut type_name: Option<String> = None;
        let mut modifier: Option<String> = None;
        for child in pair.into_inner() {
            match child.as_rule() {
                Rule::type_name => {
                    type_name = Some(child.as_str().to_owned());
                }
                Rule::field_modifier => {
                    modifier = Some(child.as_str().to_owned());
                }
                Rule::identifier => {
                    self.add_scoped_symbol(
                        child,
                        |name| {
                            vec![
                                (
                                    format!("{}_ref", name),
                                    SymbolEntryType::MethodDefinition,
                                    Vec::new(),
                                    Vec::new(),
                                ),
                                (
                                    name.to_owned(),
                                    SymbolEntryType::MemberDeclaration,
                                    vec![SymbolTag::Deprecated],
                                    Vec::new(),
                                ),
                            ]
                        },
                        Some(struct_name),
                        Some(join_strings(modifier, type_name)),
                    );
                    break;
                }
                _ => {}
            }
        }
    }

    fn process_union_defn(&mut self, pair: Pair<Rule>) {
        let mut children = pair.into_inner();
        let name = children.next().unwrap();
        self.add_symbol(name.clone(), SymbolEntryType::ClassDefinition);

        let fields = children.next().unwrap();
        for field in fields.into_inner() {
            if field.as_rule() == Rule::field {
                self.process_union_field(field, name.as_str());
            }
        }
    }

    fn process_union_field(&mut self, pair: Pair<Rule>, union_name: &str) {
        let name = pair
            .into_inner()
            .find(|rule| rule.as_rule() == Rule::identifier)
            .unwrap();
        self.add_scoped_symbol(
            name.clone(),
            |name| {
                vec![
                    (
                        format!("get_{}", name),
                        SymbolEntryType::MethodDefinition,
                        Vec::new(),
                        Vec::new(),
                    ),
                    (
                        format!("set_{}", name),
                        SymbolEntryType::MethodDefinition,
                        Vec::new(),
                        vec![ParameterEntry {
                            content: format!("{} {}", union_name, name),
                            name: name.to_owned(),
                            param_type: union_name.to_owned(),
                        }],
                    ),
                    (
                        name.to_owned(),
                        SymbolEntryType::EnumValue,
                        Vec::new(),
                        Vec::new(),
                    ),
                    (
                        "Type".to_owned(),
                        SymbolEntryType::ClassDeclaration,
                        Vec::new(),
                        Vec::new(),
                    ),
                ]
            },
            Some(union_name),
            None,
        );
        let inner_type = format!("{}::Type", union_name);
        self.add_scoped_symbol(
            name,
            |name| {
                vec![(
                    name.to_owned(),
                    SymbolEntryType::EnumValue,
                    Vec::new(),
                    Vec::new(),
                )]
            },
            Some(&inner_type),
            None,
        );
    }

    fn process_service_defn(&mut self, pair: Pair<Rule>) {
        let mut children = pair.into_inner();
        let name = children.next().unwrap();
        self.add_scoped_symbol(
            name.clone(),
            |name| {
                vec![
                    (
                        name.to_owned(),
                        SymbolEntryType::ClassDefinition,
                        Vec::new(),
                        Vec::new(),
                    ),
                    (
                        format!("{}AsyncClient", name),
                        SymbolEntryType::ClassDefinition,
                        Vec::new(),
                        Vec::new(),
                    ),
                ]
            },
            None,
            None,
        );

        for child in children {
            if child.as_rule() == Rule::method_list {
                self.process_method_list(child, name.as_str());
            }
        }
    }

    fn process_method_list(&mut self, pair: Pair<Rule>, service_name: &str) {
        let children = pair.into_inner();
        for child in children {
            if child.as_rule() == Rule::method_defn {
                self.process_method_defn(child, service_name);
            }
        }
    }
    fn process_method_defn(&mut self, pair: Pair<Rule>, service_name: &str) {
        let children: Vec<Pair<Rule>> = pair.into_inner().collect();
        let return_type = children.first().unwrap();

        let method_name = children
            .iter()
            .find(|pair| pair.as_rule() == Rule::identifier)
            .unwrap();
        let param_list = children
            .iter()
            .find(|pair| pair.as_rule() == Rule::param_list)
            .unwrap();
        let params = self.parse_param_list(param_list.clone());
        let params_string = params
            .iter()
            .map(|p| p.content.clone())
            .collect::<Vec<String>>()
            .join(", ");
        let docs = format!(
            "{} {}({})",
            return_type.as_str(),
            method_name.as_str(),
            params_string
        );

        self.add_scoped_symbol(
            method_name.clone(),
            |name| {
                vec![
                    (
                        format!("semifuture_{}", name),
                        SymbolEntryType::MethodDefinition,
                        Vec::new(),
                        params.clone(),
                    ),
                    (
                        format!("future_{}", name),
                        SymbolEntryType::MethodDefinition,
                        Vec::new(),
                        params.clone(),
                    ),
                    (
                        format!("sync_{}", name),
                        SymbolEntryType::MethodDefinition,
                        Vec::new(),
                        params.clone(),
                    ),
                    (
                        format!("co_{}", name),
                        SymbolEntryType::MethodDefinition,
                        Vec::new(),
                        params.clone(),
                    ),
                ]
            },
            Some(service_name),
            Some(docs),
        );
    }
    fn parse_param_list(&self, param_list: Pair<Rule>) -> Vec<ParameterEntry> {
        let mut params = Vec::new();
        for field in param_list.into_inner() {
            if field.as_rule() != Rule::field {
                continue;
            }
            let children: Vec<Pair<Rule>> = field.into_inner().collect();
            let type_name = children
                .iter()
                .find(|pair| pair.as_rule() == Rule::type_name)
                .unwrap();
            let name = children
                .iter()
                .find(|pair| pair.as_rule() == Rule::identifier)
                .unwrap();

            params.push(ParameterEntry {
                content: format!("{} {}", type_name.as_str(), name.as_str()),
                name: name.as_str().to_owned(),
                param_type: type_name.as_str().to_owned(),
            });
        }
        params
    }

    fn process_namespace_defn(&mut self, pair: Pair<Rule>) {
        let span = span_from_pair(&self.line_index, &pair);
        let mut children = pair.into_inner();

        let language = children.next().unwrap().as_str();
        let namespace = children.next().unwrap().as_str().replace(".", "::");

        match language {
            "cpp2" => {
                self.namespace = Some(namespace);
            }
            "cpp" => {
                self.namespace = Some(format!("{}::cpp2", namespace));
            }
            _ => {}
        }

        if let Some(ns) = &self.namespace {
            let mut scope = None;
            for namespace in ns.split("::") {
                self.doc.symbols.insert(
                    namespace.to_owned(),
                    SymbolEntry {
                        symbol: namespace.to_owned(),
                        span: span.clone(),
                        span_with_params: None,
                        docs: None,
                        signature: None,
                        entry_type: SymbolEntryType::Namespace,
                        scope: scope.clone(),
                        params: Vec::new(),
                        base_classes: Vec::new(),
                        type_string: None,
                        type_name: None,
                        tags: Vec::new(),
                    },
                );
                scope = Some(if let Some(scope) = scope {
                    format!("{}::{}", scope, namespace)
                } else {
                    namespace.to_owned()
                })
            }
        }
    }

    fn process_include(&mut self, filename: &str, span: SourceSpan) {
        let sanitized_filename = filename
            .replace('"', "")
            .replace('\'', "")
            .replace('<', "")
            .replace('>', "");
        self.doc.includes.push(IncludedFile {
            include: IncludeFile::Source(sanitized_filename),
            span,
        });
    }
}

fn span_from_linecol(line_col: LineColLocation) -> SourceSpan {
    match line_col {
        LineColLocation::Pos((line, col)) => SourceSpan {
            begin: Point::new(line - 1, col - 1),
            end: Point::new(line - 1, col),
            begin_byte: 0,
            end_byte: 0,
        },
        LineColLocation::Span((begin_line, begin_col), (end_line, end_col)) => SourceSpan {
            begin: Point::new(begin_line - 1, begin_col - 1),
            end: Point::new(end_line - 1, end_col - 1),
            begin_byte: 0,
            end_byte: 0,
        },
    }
}

impl ParsedFile for ThriftFile {
    fn parse_from_file(content: Arc<SourceFile>) -> Arc<Self> {
        let doc = Document {
            source: content.clone(),
            includes: Vec::new(),
            symbols: MultiMap::new(),
            symbol_references: MultiMap::new(),
            local_symbols: MultiMap::new(),
            diagnostics: Vec::new(),
            scopes: Vec::new(),
        };
        let mut parser = Self {
            doc,
            namespace: None,
            line_index: LineIndex::new(&content.content),
        };
        let rules = match ThriftParser::parse(Rule::thrift_file, &content.content) {
            Ok(rules) => rules,
            Err(err) => {
                log::error!("failed to parse Thrift file {}: {}", content.filename, err);
                parser.doc.diagnostics.push(Diagnostic {
                    diagnostic_type: DiagnosticType::SyntaxError,
                    message: err.to_string(),
                    span: span_from_linecol(err.line_col),
                });
                return Arc::new(parser);
            }
        };

        parser.process_thrift_file(rules);
        log::info!("parsed thrift file: {}", parser.doc.source.filename);
        Arc::new(parser)
    }

    fn get_symbol_at_point(&self, line: usize, col: usize) -> SymbolAtPoint {
        let identifier = identifier_before(
            &self.doc.source.content,
            Position::new(line as u32, col as u32),
        );
        if identifier.full_identifier.is_empty() {
            SymbolAtPoint::Missing
        } else {
            SymbolAtPoint::Identifier(identifier.full_identifier)
        }
    }

    fn document(&self) -> &Document {
        &self.doc
    }

    fn ast(&self) -> Option<Node> {
        None
    }
}

#[cfg(test)]
mod test {
    use std::sync::Arc;

    use crate::{document::ParsedFile, span::SourceFile, symbols::SymbolEntryType};

    use super::ThriftFile;

    fn parse(content: &str) -> Arc<ThriftFile> {
        ThriftFile::parse_from_file(Arc::new(SourceFile::new(
            "test_struct.thrift".to_owned(),
            content.to_owned(),
        )))
    }

    #[test]
    fn test_headers() {
        let doc = parse(include_str!("test_files/test_struct.thrift"));
        assert_eq!(doc.doc.includes.len(), 2);
        assert_eq!(doc.namespace, Some("test::file".to_owned()));
    }

    #[test]
    fn test_struct() {
        let doc = parse(include_str!("test_files/test_struct.thrift"));

        let symbols = doc.doc.find_symbol("ThriftStruct");
        assert_eq!(symbols.len(), 1);
        assert_eq!(symbols[0].entry_type, SymbolEntryType::ClassDefinition);
        assert_eq!(symbols[0].span.begin.row, 7);

        let symbols = doc.doc.find_symbol("code_ref");
        assert_eq!(symbols.len(), 1);
        assert_eq!(symbols[0].entry_type, SymbolEntryType::MethodDefinition);
        assert_eq!(
            symbols[0].scope,
            Some("test::file::ThriftStruct".to_owned())
        );
        assert_eq!(symbols[0].span.begin.row, 9);
    }

    #[test]
    fn test_enum() {
        let doc = parse(include_str!("test_files/test_struct.thrift"));

        let symbols = doc.doc.find_symbol("BAD");
        assert_eq!(symbols.len(), 1);
        assert_eq!(symbols[0].entry_type, SymbolEntryType::EnumValue);
        assert_eq!(symbols[0].scope, Some("test::file::StatusCode".to_owned()));
    }
}
