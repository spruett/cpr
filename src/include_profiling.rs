use std::collections::{HashMap, HashSet};

use multimap::MultiMap;

use crate::{
    document::{Document, IncludedFile},
    indexer::{IncludeDepth, Indexer, RankedItem},
    language_server::good_results_by,
    progress::ProgressItem,
    symbols::{scope_match, IncludeFile, SymbolEntryType, SymbolReference},
    utils::unique_by,
};

pub async fn transitive_includes(
    document: &Document,
    include: &IncludedFile,
) -> HashSet<RankedItem<String, IncludeDepth>> {
    let mut resolved = HashSet::new();
    for resolved_include in
        Indexer::get().resolve_include(&document.source.filename, &include.include)
    {
        resolved.extend(Indexer::get().transitive_includes(resolved_include).await)
    }
    resolved
}

pub async fn reverse_include_mapping(
    document: &Document,
) -> MultiMap<String, RankedItem<IncludeFile, IncludeDepth>> {
    let mut mapping = MultiMap::new();
    for include in &document.includes {
        let transitive_files = transitive_includes(document, include).await;
        for file in transitive_files {
            mapping.insert(
                file.value,
                RankedItem::new(include.include.clone(), file.rank),
            );
        }
    }
    mapping
}

pub async fn owned_include_mapping(document: &Document) -> MultiMap<IncludeFile, String> {
    let reverse_mapping = reverse_include_mapping(document).await;
    let mut ownership_mapping = MultiMap::new();
    for (resolved, includes) in reverse_mapping.iter_all() {
        if let Some(min_rank) = includes
            .iter()
            .min_by_key(|include| (include.rank, &include.value))
        {
            ownership_mapping.insert(min_rank.value.clone(), resolved.clone());
        }
    }
    ownership_mapping
}

pub async fn document_references_by_resolved_include(
    document: &Document,
) -> MultiMap<String, SymbolReference> {
    let all_symbols = Indexer::get()
        .get_all_symbols(document.source.filename.clone(), ProgressItem::noop())
        .await;
    let mut symbols_by_name = MultiMap::new();
    for ranked_symbol in all_symbols {
        symbols_by_name.insert(ranked_symbol.value.1.symbol.clone(), ranked_symbol.value);
    }

    let mut refs_by_include = MultiMap::new();
    let mut dedup = HashSet::new();
    for (_, references) in document.symbol_references.iter_all() {
        for reference in references {
            if reference.local_reference {
                continue;
            }
            if !dedup.insert((reference.symbol.clone(), reference.scope.clone())) {
                continue;
            }
            if let Some(symbols) = symbols_by_name.get_vec(&reference.symbol) {
                let results = good_results_by(symbols.clone(), |(_, sym)| {
                    sym.entry_type.declaration_match_rank()
                })
                .into_iter()
                .filter(|(_file, symbol)| {
                    symbol.entry_type != SymbolEntryType::Namespace
                        && scope_match(&symbol.scope, &reference.scope)
                });
                for (file, _) in results {
                    refs_by_include.insert(file.filename.clone(), reference.clone());
                }
            }
        }
    }
    refs_by_include
}

pub async fn document_references_by_include(
    document: &Document,
) -> HashMap<IncludeFile, HashMap<String, Vec<SymbolReference>>> {
    let refs_by_resolved = document_references_by_resolved_include(document).await;
    let owned_includes = owned_include_mapping(document).await;

    let mut refs_by_include = HashMap::new();
    for include in &document.includes {
        let owned = match owned_includes.get_vec(&include.include) {
            Some(v) => v,
            None => continue,
        };

        let mut by_owned_file = HashMap::new();
        for filename in owned {
            if let Some(from_include) = refs_by_resolved.get_vec(filename) {
                let refs_from_include =
                    unique_by(from_include.clone().into_iter(), |r| r.with_scope());

                if !refs_from_include.is_empty() {
                    by_owned_file.insert(filename.clone(), refs_from_include);
                }
            }
        }
        if !by_owned_file.is_empty() {
            refs_by_include.insert(include.include.clone(), by_owned_file);
        }
    }
    refs_by_include
}
