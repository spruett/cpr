use std::{
    cmp::min,
    collections::HashMap,
    future::Future,
    pin::Pin,
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};

use tokio::time::sleep_until;

struct DebounceItem {
    deadline: Instant,
    latest_deadline: Instant,

    future: Box<dyn Future<Output = ()> + Send>,
}

pub struct Debouncer {
    events: Mutex<HashMap<String, DebounceItem>>,
}

impl Debouncer {
    pub fn new() -> Arc<Self> {
        Arc::new(Self {
            events: Mutex::new(HashMap::new()),
        })
    }

    fn maybe_execute(&self, key: &str) {
        let now = Instant::now();
        let mut locked = self.events.lock().unwrap();
        let entry = match locked.get(key) {
            Some(entry) => entry,
            None => return,
        };
        if entry.deadline <= now || entry.latest_deadline <= now {
            let entry = locked.remove(key).unwrap();
            tokio::spawn(async move {
                Pin::from(entry.future).await;
            });
        }
    }

    pub fn clear(&self, key: &str) {
        self.events.lock().unwrap().remove(key);
    }

    pub fn debounce<F>(
        self: &Arc<Self>,
        key: String,
        min_time: Duration,
        max_time: Duration,
        future: F,
    ) where
        F: Future<Output = ()> + 'static + Send,
    {
        let now = Instant::now();
        let earliest_deadline = now + min_time;
        let latest_deadline = now + max_time;

        let mut locked = self.events.lock().unwrap();
        let entry = locked.entry(key.clone()).or_insert_with(|| DebounceItem {
            deadline: earliest_deadline,
            latest_deadline,
            future: Box::new(future),
        });
        entry.deadline = earliest_deadline;

        let debouncer = self.clone();
        let deadline = min(entry.deadline, entry.latest_deadline);
        tokio::spawn(async move {
            sleep_until(tokio::time::Instant::from_std(deadline)).await;
            debouncer.maybe_execute(&key);
        });
    }
}
