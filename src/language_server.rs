// lsp_types forces you to fill out deprecated fields
#![allow(deprecated)]
use crate::call_hierarchy::{
    call_hierarchy_incoming, call_hierarchy_outgoing, prepare_call_hierarchy,
};
use crate::code_actions::get_code_actions;
use crate::code_lens::resolve_code_lens;
use crate::commands::{handle_command, implemented_commands};
use crate::config::Config;
use crate::debounce::Debouncer;
use crate::diagnostics::{all_diagnostics, Diagnostic};
use crate::indexer::{Indexer, RankedItem};
use crate::progress::ProgressItem;
use crate::progress::ProgressRegistry;
use crate::semantic_tokens::{
    compress_semantic_tokens, get_token_legend, semantic_tokens, semantic_tokens_for_range,
};
use crate::text_util::{apply_text_change, identifier_before, identifier_before_paren};
use crate::utils::filter_references_to_resolved;
use crate::workspace::{WorkspaceManager, WorkspaceSymbolQuery};

use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::time::Duration;

use crate::completions::{build_completions, CompletionCache};
use crate::formatting::{format_full, format_range, has_formatter};
use crate::include_resolver::matching_header_or_src;
use crate::reply_buffer::ReplyBuffer;
use crate::symbols::{
    scope_match, LocalSymbolEntry, ResolvedSymbol, SymbolAtPoint, SymbolEntryType, SymbolReference,
};
use anyhow::{anyhow, Result};
use lsp_server::{
    Connection, ErrorCode, Message, Notification as ServerNotification, Request as ServerRequest,
    Response, ResponseError,
};
use lsp_types::notification::{
    DidChangeTextDocument, DidCloseTextDocument, DidOpenTextDocument, DidSaveTextDocument,
    Initialized, Notification,
};
use lsp_types::request::{
    CallHierarchyIncomingCalls, CallHierarchyOutgoingCalls, CallHierarchyPrepare,
    CodeActionRequest, CodeLensRequest, Completion, DocumentSymbolRequest, ExecuteCommand,
    Formatting, GotoDeclaration, GotoDeclarationParams, GotoDeclarationResponse, HoverRequest,
    Initialize, OnTypeFormatting, RangeFormatting, References, Rename, Request,
    SemanticTokensFullRequest, SemanticTokensRangeRequest, SignatureHelpRequest, WorkspaceSymbol,
};
use lsp_types::{
    request::GotoDefinition, DeclarationCapability, DocumentSymbolResponse, GotoDefinitionResponse,
    Hover, HoverContents, HoverProviderCapability, LanguageString, Location, MarkedString, OneOf,
    Position, Range, ServerCapabilities, TextDocumentPositionParams, TextDocumentSyncCapability,
    TextDocumentSyncKind, TextEdit, Url,
};
use lsp_types::{
    CallHierarchyIncomingCall, CallHierarchyIncomingCallsParams, CallHierarchyItem,
    CallHierarchyOutgoingCall, CallHierarchyOutgoingCallsParams, CallHierarchyPrepareParams,
    CallHierarchyServerCapability, CompletionTriggerKind, WorkDoneProgressBegin,
};
use lsp_types::{
    CodeAction, CodeActionParams, CodeActionProviderCapability, CodeLens, CodeLensOptions,
    CodeLensParams, CompletionOptions, DocumentFormattingParams, DocumentRangeFormattingParams,
    DocumentSymbolParams, Documentation, ExecuteCommandOptions, ExecuteCommandParams,
    GotoDefinitionParams, HoverParams, ProgressParams, ReferenceParams, RenameParams,
    SemanticTokens, SemanticTokensFullOptions, SemanticTokensLegend, SemanticTokensOptions,
    SemanticTokensParams, SemanticTokensRangeParams, SemanticTokensServerCapabilities,
    SignatureHelpParams, SymbolInformation, WorkspaceEdit, WorkspaceSymbolParams,
};
use lsp_types::{CompletionParams, WorkDoneProgressReport};
use lsp_types::{CompletionResponse, NumberOrString};
use lsp_types::{DeclarationOptions, ProgressToken, WorkDoneProgressCreateParams};
use lsp_types::{
    DefinitionOptions, DocumentOnTypeFormattingOptions, DocumentOnTypeFormattingParams,
};
use lsp_types::{DocumentSymbol, WorkDoneProgressOptions};
use lsp_types::{
    DocumentSymbolOptions, SaveOptions, TextDocumentSyncOptions, TextDocumentSyncSaveOptions,
};
use lsp_types::{ParameterInformation, ParameterLabel, ProgressParamsValue, SignatureInformation};
use lsp_types::{PublishDiagnosticsParams, WorkDoneProgressEnd};
use lsp_types::{SignatureHelp, SignatureHelpOptions, WorkDoneProgress};
use serde::Serialize;
use serde_json::Value;
use std::cmp::Ordering;
use std::path::Path;
use std::time::Instant;

fn cast<R>(req: ServerRequest) -> Result<R::Params>
where
    R: Request,
    R::Params: serde::de::DeserializeOwned,
{
    req.extract(R::METHOD)
        .map(|(_, p)| p)
        .map_err(|_| anyhow::Error::msg(format!("Could not cast request: {}", R::METHOD)))
}

fn notif_cast<R>(notif: ServerNotification) -> Result<R::Params>
where
    R: Notification,
    R::Params: serde::de::DeserializeOwned,
{
    notif
        .extract(R::METHOD)
        .map_err(|_| anyhow::Error::msg(format!("Could not cast notification: {}", R::METHOD)))
}

fn resolved_entry_type(resolved: &ResolvedSymbol) -> &SymbolEntryType {
    match resolved {
        ResolvedSymbol::Global((_, global)) => &global.entry_type,
        ResolvedSymbol::Local((_, local)) => &local.entry_type,
        ResolvedSymbol::Include(_) => &SymbolEntryType::VariableDefinition,
    }
}

pub fn good_results_by<F, T>(results: Vec<T>, mapper: F) -> Vec<T>
where
    F: Fn(&T) -> i32,
{
    let mut best_results = Vec::new();
    let mut best_score = 0;
    for result in results {
        let score = mapper(&result);
        match score.cmp(&best_score) {
            Ordering::Greater => {
                best_score = score;
                best_results = vec![result];
            }
            Ordering::Equal => best_results.push(result),
            _ => {}
        }
    }
    best_results
}

#[derive(Serialize)]
struct ClangdStatusNotification {
    uri: String,
    state: String,
}

pub struct LanguageServer {
    connection: Connection,
    completion_cache: CompletionCache,
    progress: ProgressRegistry,
    reply_buffer: ReplyBuffer,
    workspaces: WorkspaceManager,

    change_debouncer: Arc<Debouncer>,
}

impl LanguageServer {
    pub fn init(connection: Connection) -> Arc<Self> {
        let server = Self {
            connection,
            completion_cache: CompletionCache::new(),
            progress: ProgressRegistry::new(),
            reply_buffer: ReplyBuffer::new(),
            workspaces: WorkspaceManager::new(),
            change_debouncer: Debouncer::new(),
        };
        Arc::new(server)
    }

    pub async fn main_loop(self: Arc<Self>) -> Result<()> {
        for msg in &self.connection.receiver {
            match msg {
                Message::Request(req) => {
                    if self.connection.handle_shutdown(&req)? {
                        return Ok(());
                    }

                    let server = self.clone();
                    let request_id = req.id.clone();
                    let start_time = Instant::now();
                    let method = req.method.clone();
                    tokio::spawn(async move {
                        let response = match server.handle_request(req).await {
                            Ok(result) => Response {
                                id: request_id,
                                result: Some(result),
                                error: None,
                            },
                            Err(e) => {
                                log::error!("failed to process request: {}", e);
                                Response {
                                    id: request_id,
                                    result: None,
                                    error: Some(ResponseError {
                                        code: ErrorCode::InternalError as i32,
                                        message: e.to_string(),
                                        data: None,
                                    }),
                                }
                            }
                        };
                        let duration = Instant::now() - start_time;
                        log::info!(
                            "finished {} request ({}ms), replying",
                            method,
                            duration.as_millis(),
                        );
                        log::debug!("reply content {:?}", response);
                        server
                            .connection
                            .sender
                            .send(Message::Response(response))
                            .unwrap();
                    });
                }
                Message::Response(resp) => {
                    self.reply_buffer.handle_response(resp);
                }
                Message::Notification(not) => {
                    let server = self.clone();
                    if let Err(e) = server.handle_notification(not).await {
                        log::error!("failed to process notification: {}", e);
                    }
                }
            }
        }
        Ok(())
    }

    async fn handle_request(self: &Arc<Self>, request: ServerRequest) -> Result<serde_json::Value> {
        log::info!("got request: {}", request.method);
        log::debug!("content: {:?}", request);
        match &request.method[..] {
            Initialize::METHOD => {
                let _request = cast::<Initialize>(request)?;
                log::debug!("received initialize request");
                let response = serde_json::json!({
                    "capabilities": Self::capabilities(),
                });
                Ok(response)
            }
            Completion::METHOD => {
                let request = cast::<Completion>(request)?;
                let response = self.handle_completion_request(request).await?;
                let response = serde_json::to_value(response)?;
                Ok(response)
            }
            SignatureHelpRequest::METHOD => {
                let request = cast::<SignatureHelpRequest>(request)?;
                let result = self.handle_signature_help(request).await?;
                Ok(serde_json::to_value(result)?)
            }
            "textDocument/switchSourceHeader" => {
                let uri = Url::parse(request.params.get("uri").unwrap().as_str().unwrap())?;
                let filename = uri
                    .to_file_path()
                    .map_err(|_| anyhow!("invalid uri: {}", uri))?;
                let matching =
                    Url::from_file_path(&matching_header_or_src(&filename).unwrap_or_default())
                        .map_err(|_| anyhow!("invalid file: {:?}", filename))?
                        .to_string();
                let value = serde_json::to_value(matching)?;
                Ok(value)
            }
            GotoDefinition::METHOD => {
                let request = cast::<GotoDefinition>(request)?;
                let response = self.handle_definition(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            GotoDeclaration::METHOD => {
                let request = cast::<GotoDeclaration>(request)?;
                let response = self.handle_declaration(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            HoverRequest::METHOD => {
                let request = cast::<HoverRequest>(request)?;
                let response = self.handle_hover(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            DocumentSymbolRequest::METHOD => {
                let request = cast::<DocumentSymbolRequest>(request)?;
                let response = self.handle_document_symbols(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            WorkspaceSymbol::METHOD => {
                let request = cast::<WorkspaceSymbol>(request)?;
                let response = self.handle_workspace_symbols(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            Formatting::METHOD => {
                let request = cast::<Formatting>(request)?;
                let response = self.handle_format(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            RangeFormatting::METHOD => {
                let request = cast::<RangeFormatting>(request)?;
                let response = self.handle_range_format(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            OnTypeFormatting::METHOD => {
                let request = cast::<OnTypeFormatting>(request)?;
                let response = self.handle_on_type_format(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            References::METHOD => {
                let request = cast::<References>(request)?;
                let locations = self.handle_references(request).await?;
                Ok(serde_json::to_value(locations)?)
            }
            Rename::METHOD => {
                let request = cast::<Rename>(request)?;
                let response = self.handle_rename(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            CodeActionRequest::METHOD => {
                let request = cast::<CodeActionRequest>(request)?;
                let response = self.handle_code_action(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            SemanticTokensFullRequest::METHOD => {
                let request = cast::<SemanticTokensFullRequest>(request)?;
                let response = self.handle_semantic_tokens(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            SemanticTokensRangeRequest::METHOD => {
                let request = cast::<SemanticTokensRangeRequest>(request)?;
                let response = self.handle_semantic_tokens_range(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            CodeLensRequest::METHOD => {
                let request = cast::<CodeLensRequest>(request)?;
                let response = self.handle_code_lens(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            CallHierarchyPrepare::METHOD => {
                let request = cast::<CallHierarchyPrepare>(request)?;
                let response = self.handle_prepare_call_hierarchy(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            CallHierarchyOutgoingCalls::METHOD => {
                let request = cast::<CallHierarchyOutgoingCalls>(request)?;
                let response = self.handle_call_hierarchy_outgoing(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            CallHierarchyIncomingCalls::METHOD => {
                let request = cast::<CallHierarchyIncomingCalls>(request)?;
                let response = self.handle_call_hierarchy_incoming(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            ExecuteCommand::METHOD => {
                let request = cast::<ExecuteCommand>(request)?;
                let response = self.handle_command(request).await?;
                Ok(serde_json::to_value(response)?)
            }
            _ => {
                anyhow::bail!("{}: not implemented", request.method);
            }
        }
    }

    async fn handle_completion_request(
        self: &Arc<Self>,
        request: CompletionParams,
    ) -> Result<CompletionResponse> {
        let filename = request
            .text_document_position
            .text_document
            .uri
            .path()
            .to_owned();
        if let Some(ctx) = &request.context {
            let is_trigger = matches!(
                ctx.trigger_kind,
                CompletionTriggerKind::Invoked | CompletionTriggerKind::TriggerCharacter
            );
            if Config::get().completion.rebuild_on_trigger.unwrap_or(true) && is_trigger {
                self.completion_cache.clear(&filename);
            }
        }
        let position = request.text_document_position.position;
        let progress = self
            .register_progress(&filename, "building completions")
            .await;
        build_completions(&self.completion_cache, filename, position, progress).await
    }

    async fn handle_signature_help(
        self: &Arc<Self>,
        request: SignatureHelpParams,
    ) -> Result<SignatureHelp> {
        let filename = request
            .text_document_position_params
            .text_document
            .uri
            .path()
            .to_owned();
        let position = request.text_document_position_params.position;

        let content = Indexer::get().get_file_content(filename.clone()).await?;
        let progress = self
            .register_progress(&filename, "building signature help")
            .await;

        let symbol_name = identifier_before_paren(&content.content, position);
        let active_param = symbol_name.commas;
        let symbols = if symbol_name.identifier.is_empty() {
            Vec::new()
        } else {
            self.completion_cache
                .find(&filename, symbol_name, 20, progress.clone())
                .await
                .into_iter()
                .filter(|sym| sym.value.entry_type.is_callable())
                .filter(|sym| sym.value.signature.is_some())
                .collect()
        };
        let symbols = good_results_by(symbols, |sym| sym.value.entry_type.declaration_match_rank());

        let mut helps = Vec::new();
        for RankedItem { value: sym, .. } in symbols {
            if sym.params.len() < active_param {
                continue;
            }
            helps.push(SignatureInformation {
                label: sym.type_string().unwrap_or_default(),
                documentation: sym.docs.map(Documentation::String),
                parameters: Some(
                    sym.params
                        .into_iter()
                        .map(|p| ParameterInformation {
                            label: ParameterLabel::Simple(p.name.clone()),
                            documentation: Some(Documentation::String(format!(
                                "{} {}",
                                p.param_type, p.name
                            ))),
                        })
                        .collect(),
                ),
                active_parameter: Some(active_param as u32),
            })
        }
        progress.complete();
        Ok(SignatureHelp {
            signatures: helps,
            active_signature: None,
            active_parameter: Some(active_param as u32),
        })
    }

    fn to_locations(&self, symbols: Vec<ResolvedSymbol>) -> Vec<Location> {
        symbols
            .into_iter()
            .flat_map(|symbol| match symbol {
                ResolvedSymbol::Global((file, entry)) => {
                    vec![Location::new(
                        Url::parse(&format!("file://{}", &file.filename)).unwrap(),
                        entry.span.to_range(),
                    )]
                }
                ResolvedSymbol::Local((file, entry)) => {
                    vec![Location::new(
                        Url::parse(&format!("file://{}", &file.filename)).unwrap(),
                        entry.span.to_range(),
                    )]
                }
                ResolvedSymbol::Include(include) => Indexer::get()
                    .resolve_include("", &include)
                    .into_iter()
                    .map(|resolved_filename| {
                        Location::new(
                            Url::from_file_path(Path::new(&resolved_filename)).unwrap(),
                            Range::new(Position::new(0, 0), Position::new(0, 1)),
                        )
                    })
                    .collect(),
            })
            .collect()
    }

    async fn handle_definition(
        self: &Arc<Self>,
        request: GotoDefinitionParams,
    ) -> Result<GotoDefinitionResponse> {
        let results = self
            .resolve_symbols(&request.text_document_position_params)
            .await?;
        let results = good_results_by(results, |symbol| {
            resolved_entry_type(symbol).definition_match_rank()
        });
        Ok(GotoDefinitionResponse::Array(self.to_locations(results)))
    }

    async fn handle_declaration(
        self: &Arc<Self>,
        request: GotoDeclarationParams,
    ) -> Result<GotoDeclarationResponse> {
        let results = self
            .resolve_symbols(&request.text_document_position_params)
            .await?;
        let results = good_results_by(results, |symbol| {
            resolved_entry_type(symbol).declaration_match_rank()
        });
        Ok(GotoDeclarationResponse::Array(self.to_locations(results)))
    }

    async fn handle_hover(self: &Arc<Self>, request: HoverParams) -> Result<Option<Hover>> {
        let results = self
            .resolve_symbols(&request.text_document_position_params)
            .await?;
        let globals: Vec<_> = good_results_by(results.clone(), |symbol| {
            resolved_entry_type(symbol).declaration_match_rank()
        })
        .into_iter()
        .filter_map(|resolved_symbol| {
            if let ResolvedSymbol::Global((_, entry)) = resolved_symbol {
                Some(entry)
            } else {
                None
            }
        })
        .collect();

        let with_docs = globals.iter().find(|global| global.docs.is_some());
        let selected_symbol = with_docs.or_else(|| globals.first());
        if let Some(symbol) = selected_symbol {
            let mut markup = Vec::new();
            if let Some(docs) = symbol.docs.clone() {
                markup.push(MarkedString::String(docs));
            }
            if let Some(scope) = &symbol.scope {
                markup.push(MarkedString::LanguageString(LanguageString {
                    language: "cpp".to_string(),
                    value: scope.to_owned(),
                }));
            }
            if let Some(type_string) = symbol.type_string() {
                markup.push(MarkedString::LanguageString(LanguageString {
                    language: "cpp".to_string(),
                    value: type_string,
                }));
            }
            return Ok(Some(Hover {
                contents: HoverContents::Array(markup),
                range: None,
            }));
        }

        let local = results.into_iter().find_map(|result| {
            if let ResolvedSymbol::Local((file, symbol)) = result {
                Some((file, symbol))
            } else {
                None
            }
        });
        if let Some((file, symbol)) = local {
            let types = self
                .completion_cache
                .get_inferred_types(&file.filename, &symbol)
                .await;
            if !types.is_empty() {
                return Ok(Some(Hover {
                    contents: HoverContents::Scalar(MarkedString::LanguageString(LanguageString {
                        language: "cpp".to_string(),
                        value: types
                            .iter()
                            .map(|ty| ty.type_string())
                            .collect::<Vec<_>>()
                            .join(" | "),
                    })),
                    range: None,
                }));
            } else if let Some(type_name) = symbol.type_name {
                return Ok(Some(Hover {
                    contents: HoverContents::Scalar(MarkedString::LanguageString(LanguageString {
                        language: "cpp".to_string(),
                        value: type_name.type_string(),
                    })),
                    range: None,
                }));
            }
        }
        Ok(None)
    }

    async fn handle_document_symbols(
        self: &Arc<Self>,
        request: DocumentSymbolParams,
    ) -> Result<DocumentSymbolResponse> {
        let filename = request.text_document.uri.path().to_owned();
        let mut file_symbols = Indexer::get()
            .resolve_file_symbols(filename.clone())
            .await?;
        file_symbols.sort_by_key(|symbol| (symbol.span.begin.row, symbol.span.begin.column));

        let mut symbol_entries = Vec::new();
        for entry in file_symbols {
            let span = entry.span.clone();
            let detail = entry.type_string();
            let symbol = DocumentSymbol {
                name: entry.symbol,
                kind: entry.entry_type.to_symbol_kind(),
                tags: Some(entry.tags.iter().map(|t| t.to_symbol_info_tag()).collect()),
                deprecated: None,
                detail,
                range: entry.span_with_params.unwrap_or(span).to_range(),
                selection_range: entry.span.to_range(),
                children: None,
            };
            symbol_entries.push(symbol);
        }
        Ok(DocumentSymbolResponse::Nested(symbol_entries))
    }

    async fn handle_workspace_symbols(
        self: &Arc<Self>,
        request: WorkspaceSymbolParams,
    ) -> Result<Vec<SymbolInformation>> {
        let (scope, symbol) = match request.query.rsplit_once("::") {
            Some((scope, symbol)) => (Some(scope.to_owned()), symbol.to_owned()),
            None => (None, request.query.clone()),
        };

        let mut symbols = Vec::new();
        let query = WorkspaceSymbolQuery {
            symbol,
            scope,
            prefix_match: true,
        };
        let max_results = 1000;
        let mut dedup = HashSet::new();
        for workspace in self.workspaces.all_workspaces() {
            let workspace_symbols = workspace.query_symbols(&query).await;
            for (filename, document_symbols) in workspace_symbols {
                for symbol in document_symbols {
                    if !dedup.insert((symbol.symbol.clone(), symbol.entry_type.clone())) {
                        continue;
                    }
                    if symbols.len() >= max_results {
                        break;
                    }

                    symbols.push(SymbolInformation {
                        name: symbol.symbol,
                        kind: symbol.entry_type.to_symbol_kind(),
                        tags: Some(
                            symbol
                                .tags
                                .into_iter()
                                .map(|t| t.to_symbol_info_tag())
                                .collect(),
                        ),
                        deprecated: None,
                        location: Location {
                            uri: Url::from_file_path(&filename).unwrap(),
                            range: symbol.span.to_range(),
                        },
                        container_name: symbol.scope,
                    });
                }
            }
        }
        Ok(symbols)
    }

    async fn handle_format(
        self: &Arc<Self>,
        request: DocumentFormattingParams,
    ) -> Result<Vec<TextEdit>> {
        let filename = request.text_document.uri.path().to_owned();
        let source_file = Indexer::get().get_file_content(filename).await?;
        let formatted = format_full(source_file.as_ref()).await?;

        let text_edit = TextEdit {
            range: Range::new(Position::new(0, 0), Position::new(10_000_000, 0)),
            new_text: formatted,
        };
        Ok(vec![text_edit])
    }

    async fn handle_range_format(
        self: &Arc<Self>,
        request: DocumentRangeFormattingParams,
    ) -> Result<Vec<TextEdit>> {
        let filename = request.text_document.uri.path().to_owned();
        let source_file = Indexer::get().get_file_content(filename).await?;
        let formatted = format_range(
            source_file.as_ref(),
            request.range.start.line as usize,
            request.range.end.line as usize,
        )
        .await?;

        let text_edit = TextEdit {
            range: Range::new(Position::new(0, 0), Position::new(10_000_000, 0)),
            new_text: formatted,
        };
        Ok(vec![text_edit])
    }

    async fn handle_on_type_format(
        self: &Arc<Self>,
        request: DocumentOnTypeFormattingParams,
    ) -> Result<Vec<TextEdit>> {
        let filename = request
            .text_document_position
            .text_document
            .uri
            .path()
            .to_owned();
        let line = request.text_document_position.position.line;
        let source_file = Indexer::get().get_file_content(filename).await?;
        let formatted =
            format_range(source_file.as_ref(), line as usize, (line + 1) as usize).await?;

        let text_edit = TextEdit {
            range: Range::new(Position::new(0, 0), Position::new(10_000_000, 0)),
            new_text: formatted,
        };
        Ok(vec![text_edit])
    }

    async fn find_all_references(
        self: &Arc<Self>,
        position: TextDocumentPositionParams,
    ) -> Result<Vec<(String, Vec<SymbolReference>)>> {
        let filename = position.text_document.uri.path().to_owned();
        let resolved_symbols = self.resolve_symbols(&position).await?;
        let resolved_symbols = good_results_by(resolved_symbols, |sym| {
            resolved_entry_type(sym).declaration_match_rank()
        });

        let symbol = match resolved_symbols.first() {
            Some(s) => s,
            None => return Ok(Vec::new()),
        };

        let refs = match symbol {
            ResolvedSymbol::Local((_file, local)) => {
                let references = Indexer::get()
                    .search_file_symbol_references(filename.to_owned(), &local.symbol)
                    .await
                    .into_iter()
                    .filter(|reference| {
                        reference.local_reference && local.valid_span.contains_span(&reference.span)
                    })
                    .collect();
                vec![(filename.clone(), references)]
            }
            ResolvedSymbol::Global((_file, global)) => {
                match self.workspaces.get_workspace(&filename) {
                    Some(workspace) => {
                        let query = WorkspaceSymbolQuery {
                            symbol: global.symbol.clone(),
                            // TODO: use scope intelligently
                            scope: None,
                            prefix_match: false,
                        };
                        let refs = workspace.find_all_references(&query).await;
                        filter_references_to_resolved(&resolved_symbols, refs).await
                    }
                    None => {
                        // Default to finding references in current file
                        let references = Indexer::get()
                            .search_file_symbol_references(filename.to_owned(), &global.symbol)
                            .await
                            .into_iter()
                            .filter(|reference| !reference.local_reference)
                            .collect();
                        vec![(filename.clone(), references)]
                    }
                }
            }
            _ => return Ok(Vec::new()),
        };
        Ok(refs)
    }

    async fn handle_references(
        self: &Arc<Self>,
        request: ReferenceParams,
    ) -> Result<Vec<Location>> {
        let refs = self
            .find_all_references(request.text_document_position)
            .await?;

        let locations = refs
            .into_iter()
            .flat_map(|(filename, references)| {
                references
                    .into_iter()
                    .map(|reference| {
                        Location::new(
                            Url::from_file_path(filename.clone()).unwrap(),
                            reference.span.to_range(),
                        )
                    })
                    .collect::<Vec<Location>>()
            })
            .collect();
        Ok(locations)
    }

    async fn handle_rename(self: &Arc<Self>, request: RenameParams) -> Result<WorkspaceEdit> {
        let new_name = request.new_name;
        let refs = self
            .find_all_references(request.text_document_position)
            .await?;

        let mut edits = HashMap::new();
        for (filename, refs) in refs {
            let text_edits = refs
                .into_iter()
                .map(|reference| TextEdit {
                    range: reference.span.to_range(),
                    new_text: new_name.clone(),
                })
                .collect();
            let uri = Url::from_file_path(Path::new(&filename)).unwrap();
            edits.insert(uri, text_edits);
        }

        Ok(WorkspaceEdit {
            changes: Some(edits),
            ..Default::default()
        })
    }

    async fn handle_code_action(
        self: &Arc<Self>,
        request: CodeActionParams,
    ) -> Result<Vec<CodeAction>> {
        let filename = request.text_document.uri.path().to_owned();
        let code_actions = get_code_actions(filename, request.range).await?;
        Ok(code_actions)
    }

    async fn handle_semantic_tokens(
        self: &Arc<Self>,
        request: SemanticTokensParams,
    ) -> Result<SemanticTokens> {
        let filename = request.text_document.uri.path().to_owned();
        let doc = Indexer::get().get_index(filename).await?;
        let tokens = semantic_tokens(doc.document()).await;
        let compressed = compress_semantic_tokens(&tokens);
        Ok(SemanticTokens {
            result_id: None,
            data: compressed,
        })
    }

    async fn handle_semantic_tokens_range(
        self: &Arc<Self>,
        request: SemanticTokensRangeParams,
    ) -> Result<SemanticTokens> {
        let filename = request.text_document.uri.path().to_owned();
        let doc = Indexer::get().get_index(filename).await?;
        let tokens = semantic_tokens_for_range(doc.document(), &request.range).await;
        let compressed = compress_semantic_tokens(&tokens);
        Ok(SemanticTokens {
            result_id: None,
            data: compressed,
        })
    }

    async fn handle_code_lens(self: &Arc<Self>, request: CodeLensParams) -> Result<Vec<CodeLens>> {
        let filename = request.text_document.uri.path().to_owned();
        let parsed_file = Indexer::get().get_index(filename).await?;
        let code_lens = resolve_code_lens(parsed_file.document()).await?;
        Ok(code_lens)
    }

    async fn handle_prepare_call_hierarchy(
        self: &Arc<Self>,
        request: CallHierarchyPrepareParams,
    ) -> Result<Vec<CallHierarchyItem>> {
        let filename = request
            .text_document_position_params
            .text_document
            .uri
            .path()
            .to_owned();
        prepare_call_hierarchy(filename, request.text_document_position_params.position).await
    }

    async fn handle_call_hierarchy_outgoing(
        self: &Arc<Self>,
        request: CallHierarchyOutgoingCallsParams,
    ) -> Result<Vec<CallHierarchyOutgoingCall>> {
        call_hierarchy_outgoing(request.item).await
    }

    async fn handle_call_hierarchy_incoming(
        self: &Arc<Self>,
        request: CallHierarchyIncomingCallsParams,
    ) -> Result<Vec<CallHierarchyIncomingCall>> {
        call_hierarchy_incoming(request.item).await
    }

    async fn handle_command(
        self: &Arc<Self>,
        request: ExecuteCommandParams,
    ) -> Result<Option<serde_json::Value>> {
        let args = request.arguments;
        if let Some(arg) = args.first() {
            handle_command(&*self, &request.command, arg.clone()).await?;
            Ok(None)
        } else {
            anyhow::bail!("missing argument")
        }
    }

    async fn handle_notification(self: &Arc<Self>, notification: ServerNotification) -> Result<()> {
        log::info!("got notification: {}", notification.method);
        log::debug!("notification content: {:?}", notification);
        match &notification.method[..] {
            Initialized::METHOD => {
                let _notif = notif_cast::<Initialized>(notification);
                log::info!("server initialized");
            }
            DidOpenTextDocument::METHOD => {
                let notif = notif_cast::<DidOpenTextDocument>(notification)?;
                let filename = notif.text_document.uri.path().to_owned();
                let content = notif.text_document.text;
                Indexer::get().update_from_editor(filename.clone(), content);
                self.warm_cache(filename.clone());
                self.warm_workspace(filename);
            }
            DidCloseTextDocument::METHOD => {
                let notif = notif_cast::<DidCloseTextDocument>(notification)?;
                let filename = notif.text_document.uri.path().to_owned();
                self.completion_cache.clear(&filename);
                self.clear_diagnostics(&filename);
                Indexer::get().release_from_editor(filename);
            }
            DidChangeTextDocument::METHOD => {
                let mut notif = notif_cast::<DidChangeTextDocument>(notification).unwrap();
                let filename = notif.text_document.uri.path().to_owned();
                let existing_content = Indexer::get().get_file_content(filename.clone()).await?;
                let new_content =
                    apply_text_change(&existing_content.content, &mut notif.content_changes);
                self.clear_diagnostics(&filename);
                Indexer::get().update_from_editor(filename.clone(), new_content);

                let server = self.clone();
                self.change_debouncer.debounce(
                    filename.clone(),
                    Duration::from_secs(
                        Config::get()
                            .completion
                            .cache_debounce_min_secs
                            .unwrap_or(3),
                    ),
                    Duration::from_secs(
                        Config::get()
                            .completion
                            .cache_debounce_max_secs
                            .unwrap_or(15),
                    ),
                    async move {
                        server.completion_cache.clear(&filename);
                        server.warm_cache(filename);
                    },
                );
            }
            DidSaveTextDocument::METHOD => {
                let notif = notif_cast::<DidSaveTextDocument>(notification)?;
                let filename = notif.text_document.uri.path().to_owned();
                self.completion_cache.clear(&filename);
                self.change_debouncer.clear(&filename);
                if let Some(content) = notif.text {
                    Indexer::get().update_from_editor(filename.clone(), content);
                    self.warm_cache(filename);
                }
            }
            _ => {}
        }
        Ok(())
    }

    fn warm_cache(self: &Arc<Self>, filename: String) {
        self.send_clangd_notification(&filename, "warming cache");
        self.clear_diagnostics(&filename);
        let server = self.clone();
        tokio::spawn(async move {
            let base_filename = Path::new(&filename)
                .file_name()
                .unwrap_or_default()
                .to_string_lossy();
            let progress = server.register_progress(&base_filename, "warming").await;
            let parsed_file = match Indexer::get().warm_cache(filename.clone(), progress).await {
                Ok(file) => file,
                Err(e) => {
                    log::error!("failed to process file {}: {}", filename, e);
                    return;
                }
            };
            let diagnostics = all_diagnostics(parsed_file.document()).await;
            server.send_diagnostics(&filename, diagnostics);
            server.send_clangd_notification(&filename, "idle");
        });
    }

    fn warm_workspace(self: &Arc<Self>, filename: String) {
        let server = self.clone();
        tokio::spawn(async move {
            let progress = server.register_progress("workspace", "warming").await;
            server
                .workspaces
                .warmup_workspace(&filename, progress)
                .await;
        });
    }

    async fn resolve_symbols(
        self: &Arc<Self>,
        text_document_position: &TextDocumentPositionParams,
    ) -> Result<Vec<ResolvedSymbol>> {
        let filename = text_document_position.text_document.uri.path().to_owned();
        let progress = self.register_progress(&filename, "searching").await;
        let position = text_document_position.position;
        let symbol = Indexer::get()
            .get_symbol_at_point(
                filename.clone(),
                position.line as usize,
                position.character as usize,
            )
            .await;

        let file_content = Indexer::get().get_file_content(filename.clone()).await?;
        let scope = identifier_before(&file_content.content, position);
        let result: Vec<ResolvedSymbol> = match symbol {
            SymbolAtPoint::Identifier(symbol) => {
                let local_results: Vec<LocalSymbolEntry> = Indexer::get()
                    .resolve_local_symbol(filename.clone(), &symbol)
                    .await
                    .into_iter()
                    .filter(|local| local.valid_span.contains(&position))
                    .collect();

                if scope.scope.is_none() && !local_results.is_empty() {
                    progress.complete();
                    local_results
                        .into_iter()
                        .map(|local| ResolvedSymbol::Local((file_content.clone(), local)))
                        .collect()
                } else {
                    let results = Indexer::get()
                        .resolve_symbol(filename, symbol.clone(), progress)
                        .await;
                    results
                        .into_iter()
                        .filter(|(_file, symbol)| scope_match(&symbol.scope, &scope.scope))
                        .map(|(file, symbol)| ResolvedSymbol::Global((file, symbol)))
                        .collect()
                }
            }
            SymbolAtPoint::Include(include) => {
                progress.complete();
                vec![ResolvedSymbol::Include(include)]
            }
            _ => {
                progress.complete();
                Vec::new()
            }
        };
        Ok(result)
    }

    async fn register_progress(
        self: &Arc<Self>,
        filename: &str,
        message: &str,
    ) -> Arc<ProgressItem> {
        let base_filename = Path::new(filename)
            .file_name()
            .unwrap_or_default()
            .to_string_lossy();
        let progress = self
            .progress
            .add_new("gels".to_owned(), format!("{} {}", message, base_filename));
        let request = WorkDoneProgressCreateParams {
            token: ProgressToken::String(progress.token().unwrap()),
        };
        self.send_request("window/workDoneProgress/create", request)
            .await
            .unwrap();
        self.clone().start_progress_report(progress.clone());
        progress
    }

    fn start_progress_report(self: Arc<Self>, progress: Arc<ProgressItem>) {
        tokio::spawn(async move {
            if let Some(token) = progress.token() {
                let begin = WorkDoneProgressBegin {
                    title: progress.title(),
                    cancellable: Some(false),
                    message: Some(progress.message()),
                    percentage: Some(progress.progress() as u32),
                };
                self.send_progress(&*progress, WorkDoneProgress::Begin(begin));

                while !progress.done() && !progress.canceled() {
                    let update = WorkDoneProgressReport {
                        cancellable: Some(false),
                        message: Some(progress.message()),
                        percentage: Some(progress.progress() as u32),
                    };
                    self.send_progress(&*progress, WorkDoneProgress::Report(update));
                    tokio::time::sleep(Duration::from_millis(100)).await;
                }
                if !progress.canceled() {
                    let end = WorkDoneProgressEnd { message: None };
                    self.send_progress(&*progress, WorkDoneProgress::End(end));
                }
                self.progress.clear(token);
            }
        });
    }

    fn send_clangd_notification(&self, filename: &str, state: &str) {
        if !Config::get().features.disable_clangd_status {
            self.send_notification(
                "textDocument/clangd.fileStatus",
                ClangdStatusNotification {
                    uri: format!("file://{}", filename),
                    state: state.to_owned(),
                },
            );
        }
    }

    fn clear_diagnostics(&self, filename: &str) {
        self.send_diagnostics(filename, Vec::new());
    }

    fn send_diagnostics(&self, filename: &str, diagnostics: Vec<Diagnostic>) {
        if Config::get().diagnostics.is_none() {
            return;
        }

        let diagnostics = diagnostics
            .into_iter()
            .map(|diagnostic| lsp_types::Diagnostic {
                range: diagnostic.span.to_range(),
                severity: Some(diagnostic.severity()),
                code: None,
                code_description: None,
                source: None,
                message: diagnostic.message,
                related_information: None,
                tags: None,
                data: None,
            })
            .collect();
        let notification = PublishDiagnosticsParams {
            uri: Url::from_file_path(filename).unwrap(),
            diagnostics,
            version: None,
        };
        self.send_notification("textDocument/publishDiagnostics", notification);
    }

    fn send_progress(&self, progress: &ProgressItem, value: WorkDoneProgress) {
        let notification = ProgressParams {
            token: NumberOrString::String(progress.token().unwrap()),
            value: ProgressParamsValue::WorkDone(value),
        };
        self.send_notification("$/progress", notification);
    }

    fn send_notification<P: Serialize>(&self, method: &str, notification: P) {
        let notification = ServerNotification {
            method: method.to_owned(),
            params: serde_json::to_value(notification).unwrap(),
        };
        self.connection
            .sender
            .send(Message::Notification(notification))
            .unwrap();
    }

    pub async fn send_request<R: Serialize>(
        &self,
        method: &str,
        params: R,
    ) -> Result<Value, ResponseError> {
        let waiter = self.reply_buffer.send();
        let request = ServerRequest {
            id: waiter.id(),
            method: method.to_string(),
            params: serde_json::to_value(params).unwrap(),
        };
        self.connection
            .sender
            .send(Message::Request(request))
            .unwrap();
        waiter.wait().await
    }

    fn capabilities() -> ServerCapabilities {
        let has_formatting = has_formatter();
        let semantic_options = if Config::get().features.semantic_tokens {
            Some(SemanticTokensServerCapabilities::SemanticTokensOptions(
                SemanticTokensOptions {
                    work_done_progress_options: WorkDoneProgressOptions {
                        work_done_progress: None,
                    },
                    legend: SemanticTokensLegend {
                        token_types: get_token_legend().clone(),
                        token_modifiers: Vec::new(),
                    },
                    range: Some(true),
                    full: Some(SemanticTokensFullOptions::Bool(true)),
                },
            ))
        } else {
            None
        };
        ServerCapabilities {
            declaration_provider: Some(DeclarationCapability::Options(DeclarationOptions {
                work_done_progress_options: WorkDoneProgressOptions {
                    work_done_progress: Some(true),
                },
            })),
            definition_provider: Some(OneOf::Right(DefinitionOptions {
                work_done_progress_options: WorkDoneProgressOptions {
                    work_done_progress: Some(true),
                },
            })),
            document_symbol_provider: Some(OneOf::Right(DocumentSymbolOptions {
                label: Some("Gels".to_owned()),
                work_done_progress_options: WorkDoneProgressOptions {
                    work_done_progress: Some(true),
                },
            })),
            text_document_sync: Some(TextDocumentSyncCapability::Options(
                TextDocumentSyncOptions {
                    open_close: Some(true),
                    change: Some(TextDocumentSyncKind::Incremental),
                    save: Some(TextDocumentSyncSaveOptions::SaveOptions(SaveOptions {
                        include_text: Some(true),
                    })),
                    ..Default::default()
                },
            )),
            document_formatting_provider: Some(OneOf::Left(has_formatting)),
            document_range_formatting_provider: Some(OneOf::Left(has_formatting)),
            document_on_type_formatting_provider: Some(DocumentOnTypeFormattingOptions {
                first_trigger_character: ";".to_owned(),
                more_trigger_character: Some(vec!["}".to_owned()]),
            }),
            hover_provider: Some(HoverProviderCapability::Simple(true)),
            completion_provider: Some(CompletionOptions {
                resolve_provider: None,
                trigger_characters: Some(vec![
                    ":".to_owned(),
                    ".".to_owned(),
                    "/".to_owned(),
                    ">".to_owned(),
                ]),
                all_commit_characters: None,
                work_done_progress_options: Default::default(),
            }),
            signature_help_provider: Some(SignatureHelpOptions {
                trigger_characters: Some(vec!["(".to_owned()]),
                retrigger_characters: Some(vec![",".to_owned()]),
                work_done_progress_options: Default::default(),
            }),
            references_provider: Some(OneOf::Left(true)),
            rename_provider: Some(OneOf::Left(true)),
            code_action_provider: Some(CodeActionProviderCapability::Simple(true)),
            workspace_symbol_provider: Some(OneOf::Left(true)),
            semantic_tokens_provider: semantic_options,
            code_lens_provider: Some(CodeLensOptions {
                resolve_provider: Some(false),
            }),
            execute_command_provider: Some(ExecuteCommandOptions {
                commands: implemented_commands(),
                work_done_progress_options: Default::default(),
            }),
            call_hierarchy_provider: Some(CallHierarchyServerCapability::Simple(true)),
            ..Default::default()
        }
    }
}
