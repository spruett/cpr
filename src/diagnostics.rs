use std::collections::HashSet;

use lsp_types::{DiagnosticSeverity, Position};
use multimap::MultiMap;

use crate::{
    completions::cpp_keywords,
    config::{Config, DiagnosticConfig},
    document::Document,
    indexer::Indexer,
    progress::ProgressItem,
    span::SourceSpan,
    symbols::{get_scope_suggestion, symbol_needs_scope},
};

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum DiagnosticType {
    SyntaxError,
    UndefinedReference,
    ReferenceNotInScope,
    MissingInclude,
    UnusedLocal,
}

#[derive(Clone)]
pub struct Diagnostic {
    pub diagnostic_type: DiagnosticType,
    pub message: String,
    pub span: SourceSpan,
}

impl Diagnostic {
    pub fn severity(&self) -> DiagnosticSeverity {
        match self.diagnostic_type {
            DiagnosticType::MissingInclude | DiagnosticType::UnusedLocal => {
                DiagnosticSeverity::Information
            }
            _ => DiagnosticSeverity::Warning,
        }
    }
}

async fn analyze_references(document: &Document, config: &DiagnosticConfig) -> Vec<Diagnostic> {
    let mut diagnostics = Vec::new();
    let all_symbols = Indexer::get()
        .get_all_symbols(document.source.filename.clone(), ProgressItem::noop())
        .await;
    let mut symbols_by_name = MultiMap::new();
    for ranked_symbol in all_symbols {
        symbols_by_name.insert(ranked_symbol.value.1.symbol.clone(), ranked_symbol.value);
    }
    let keywords: HashSet<_> = cpp_keywords().into_iter().map(str::to_owned).collect();
    for (_, references) in document.symbol_references.iter_all() {
        for reference in references {
            if reference.local_reference {
                continue;
            }
            if !symbols_by_name.contains_key(&reference.symbol)
                && !keywords.contains(&reference.symbol)
            {
                diagnostics.push(Diagnostic {
                    diagnostic_type: DiagnosticType::UndefinedReference,
                    message: format!("unknown reference to {}", reference.symbol),
                    span: reference.span.clone(),
                });
            } else if config.reference_scoping
                && !reference.is_field
                && !reference.is_namespace
                && reference.scope.is_none()
            {
                let all_symbols_need_scope = symbols_by_name
                    .get_vec(&reference.symbol)
                    .into_iter()
                    .flatten()
                    .all(|(_, symbol)| symbol_needs_scope(&symbol.entry_type));
                if all_symbols_need_scope {
                    let symbol_scopes: Vec<Option<String>> = symbols_by_name
                        .get_vec(&reference.symbol)
                        .into_iter()
                        .flatten()
                        .map(|(_, symbol)| symbol.scope.clone())
                        .collect();
                    let scopes = document.find_scopes(&Position::new(
                        reference.span.begin.row as u32,
                        reference.span.begin.column as u32,
                    ));
                    if let Some(scope_suggestion) = get_scope_suggestion(&scopes, &symbol_scopes) {
                        diagnostics.push(Diagnostic {
                            diagnostic_type: DiagnosticType::ReferenceNotInScope,
                            message: format!(
                                "{} is not in scope, did you mean {}::{}",
                                reference.symbol, scope_suggestion, reference.symbol
                            ),
                            span: reference.span.clone(),
                        })
                    }
                }
            }
        }
    }
    diagnostics
}

async fn analyze_includes(document: &Document) -> Vec<Diagnostic> {
    let mut diagnostics = Vec::new();
    for include in &document.includes {
        if Indexer::get()
            .resolve_include(&document.source.filename, &include.include)
            .is_empty()
        {
            diagnostics.push(Diagnostic {
                diagnostic_type: DiagnosticType::MissingInclude,
                message: format!("cannot resolve include: {:?}", include.include),
                span: include.span.clone(),
            });
        }
    }
    diagnostics
}

async fn analyze_unused_locals(document: &Document) -> Vec<Diagnostic> {
    let mut diagnostics = Vec::new();
    for (_, locals) in &document.local_symbols {
        for local in locals {
            let reference_count = document
                .find_symbol_references(&local.symbol)
                .into_iter()
                .filter(|reference| {
                    reference.local_reference && local.valid_span.contains_span(&reference.span)
                })
                .count();
            if reference_count <= 1
                && !local.is_param_declaration
                && !local.symbol.starts_with('_')
                && !local.symbol.to_ascii_lowercase().contains("guard")
            {
                diagnostics.push(Diagnostic {
                    diagnostic_type: DiagnosticType::UnusedLocal,
                    message: format!("unused local: {}", local.symbol),
                    span: local.span.clone(),
                })
            }
        }
    }
    diagnostics
}

pub async fn all_diagnostics(document: &Document) -> Vec<Diagnostic> {
    let mut diagnostics = Vec::new();
    if let Some(config) = &Config::get().diagnostics {
        if config.syntax {
            diagnostics.extend(document.diagnostics.clone());
        }
        if config.unknown_references {
            diagnostics.extend(analyze_references(document, config).await);
        }
        if config.includes {
            diagnostics.extend(analyze_includes(document).await);
        }
        if config.unused_locals {
            diagnostics.extend(analyze_unused_locals(document).await);
        }
    }
    diagnostics
}
