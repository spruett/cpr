use std::{
    collections::HashSet,
    path::{Path, PathBuf},
    str::FromStr,
    sync::{Arc, Mutex},
};

use glob::glob;
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver};

use crate::{
    config::{Config, ProjectName},
    document::ParsedFile,
    indexer::Indexer,
    progress::ProgressItem,
    symbols::{scope_match, SymbolEntry, SymbolReference},
};

pub struct Workspace {
    project_name: ProjectName,
    warmup: bool,
}

pub struct WorkspaceSymbolQuery {
    pub symbol: String,
    pub scope: Option<String>,
    pub prefix_match: bool,
}

impl Workspace {
    fn resolve_all_files(&self) -> HashSet<String> {
        let project_config = match Config::get().projects.get(&self.project_name) {
            Some(p) => p,
            None => return HashSet::new(),
        };

        let patterns = &project_config.workspace.indexed_files;
        let mut files = HashSet::new();
        for pattern in patterns {
            let path = if pattern.starts_with('/') {
                PathBuf::from_str(pattern).unwrap()
            } else {
                project_config.root.join(&pattern)
            };
            for file in glob(path.to_str().unwrap()).into_iter().flatten().flatten() {
                files.insert(file.to_string_lossy().to_string());
            }
        }
        files
    }

    pub fn project_root(&self) -> String {
        match Config::get().projects.get(&self.project_name) {
            Some(p) => p.root.to_string_lossy().to_string(),
            None => "".to_owned(),
        }
    }

    fn index_all_files(
        &self,
        progress: Arc<ProgressItem>,
    ) -> UnboundedReceiver<Arc<dyn ParsedFile>> {
        let files = self.resolve_all_files();
        let (tx, rx) = unbounded_channel();

        for file in files {
            progress.clone().add_task();

            let tx = tx.clone();
            let progress = progress.clone();
            tokio::spawn(async move {
                if let Ok(parsed) = Indexer::get().get_index(file).await {
                    progress.complete_task();
                    if tx.send(parsed).is_err() {
                        panic!("failed to send index to receiver");
                    }
                }
            });
        }
        rx
    }

    async fn warmup_workspace(&self, progress: Arc<ProgressItem>) {
        let mut indexes = self.index_all_files(progress);
        while indexes.recv().await.is_some() {}
    }

    pub async fn find_all_references(
        &self,
        symbol: &WorkspaceSymbolQuery,
    ) -> Vec<(String, Vec<SymbolReference>)> {
        let mut references = Vec::new();

        let mut indexes = self.index_all_files(ProgressItem::noop());

        while let Some(index) = indexes.recv().await {
            let symbol_references = index.document().find_symbol_references(&symbol.symbol);
            if !symbol_references.is_empty() {
                references.push((index.document().source.filename.clone(), symbol_references));
            }
        }
        references
    }

    pub async fn query_symbols(
        &self,
        symbol_query: &WorkspaceSymbolQuery,
    ) -> Vec<(String, Vec<SymbolEntry>)> {
        let mut symbol_entries = Vec::new();

        let mut indexes = self.index_all_files(ProgressItem::noop());

        while let Some(index) = indexes.recv().await {
            let document = index.document();
            let symbols = if symbol_query.prefix_match {
                document.scan_symbols(&symbol_query.symbol)
            } else {
                document.find_symbol(&symbol_query.symbol)
            };
            let symbols = symbols
                .into_iter()
                .filter(|symbol_entry| scope_match(&symbol_entry.scope, &symbol_query.scope))
                .collect::<Vec<SymbolEntry>>();
            if !symbols.is_empty() {
                symbol_entries.push((index.document().source.filename.clone(), symbols));
            }
        }
        symbol_entries
    }

    pub fn resolve_relative_filename(&self, filename: &str) -> Option<String> {
        let root = self.project_root();
        filename
            .strip_prefix(&root)
            .map(|filename| filename.trim_start_matches('/').to_owned())
    }

    pub fn is_library_filename(&self, filename: &str) -> bool {
        let include_config = Config::get()
            .projects
            .get(&self.project_name)
            .and_then(|cfg| cfg.includes.as_ref())
            .unwrap_or_else(|| &Config::get().include);
        let path_name = Path::new(filename);
        let is_library = include_config
            .libraries
            .iter()
            .any(|library_path| path_name.starts_with(library_path));
        let is_src = include_config
            .sources
            .iter()
            .any(|src_path| path_name.starts_with(src_path));
        if is_library && is_src {
            include_config
                .prefer_library_format
                .iter()
                .any(|library_path| filename.contains(library_path))
        } else {
            is_library
        }
    }
}

pub struct WorkspaceManager {
    warmed_workspaces: Mutex<HashSet<String>>,
}

impl WorkspaceManager {
    pub fn new() -> Self {
        Self {
            warmed_workspaces: Mutex::new(HashSet::new()),
        }
    }

    pub fn get_workspace(&self, filename: &str) -> Option<Workspace> {
        let (name, project_config) = Config::get().resolve_project(filename)?;
        Some(Workspace {
            project_name: name.clone(),
            warmup: project_config.workspace.warmup,
        })
    }

    pub fn all_workspaces(&self) -> Vec<Workspace> {
        Config::get()
            .projects
            .keys()
            .map(|name| Workspace {
                project_name: name.to_owned(),
                warmup: false,
            })
            .collect()
    }

    pub async fn warmup_workspace(&self, filename: &str, progress: Arc<ProgressItem>) {
        if let Some(workspace) = self.get_workspace(filename) {
            {
                if !self
                    .warmed_workspaces
                    .lock()
                    .unwrap()
                    .insert(workspace.project_name.clone())
                {
                    progress.complete();
                    return;
                }
            }
            if workspace.warmup {
                workspace.warmup_workspace(progress.clone()).await;
            }
        }
        progress.complete();
    }
}
