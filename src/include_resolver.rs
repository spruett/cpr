use crate::config::{Config, IncludeConfig, ProjectName, ThriftResolveConfig};
use crate::symbols::IncludeFile;
use glob::glob;
use regex::Regex;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
    sync::RwLock,
};

fn matching_file_with_extension(filename: &Path, extension: &str) -> Option<PathBuf> {
    let path = Path::new(filename);
    let with_extension = path.with_extension(extension);
    if with_extension.exists() {
        Some(with_extension)
    } else {
        None
    }
}

pub fn matching_src(header: &Path) -> Option<PathBuf> {
    matching_file_with_extension(header, "cpp")
}

pub fn matching_main_header(header: &Path) -> Option<PathBuf> {
    if header.to_string_lossy().ends_with("-inl.h") {
        let main_header = header.to_string_lossy().replace("-inl.h", ".h");
        let main_header_path = PathBuf::from(main_header);
        if main_header_path.exists() {
            Some(main_header_path)
        } else {
            None
        }
    } else {
        None
    }
}

pub fn matching_header(src: &Path) -> Option<PathBuf> {
    matching_file_with_extension(src, "h")
}

pub fn is_includable_header<P: AsRef<Path>>(src: P) -> bool {
    // TODO: fix -inl.h, etc
    is_header(src)
}

pub fn is_header<P: AsRef<Path>>(src: P) -> bool {
    let extension = src
        .as_ref()
        .extension()
        .map(|os_str| os_str.to_str().unwrap());
    extension == Some("h") || extension == Some("hpp") || extension.is_none()
}

pub fn matching_header_or_src(filename: &Path) -> Option<PathBuf> {
    if is_header(filename) {
        matching_src(filename)
    } else {
        matching_header(filename)
    }
}

struct PatternReplacer {
    from: Regex,
    to: String,
}

impl PatternReplacer {
    fn from_thrift_config(config: &ThriftResolveConfig) -> PatternReplacer {
        Self {
            from: Regex::new(&config.from).expect("invalid regex"),
            to: config.to.clone(),
        }
    }
}

pub struct IncludeResolver {
    patterns: Vec<PatternReplacer>,

    cache: RwLock<HashMap<(ProjectName, IncludeFile), Vec<String>>>,
}

fn is_valid_include(base: &Path, target: &str) -> Option<String> {
    let joined = base.join(Path::new(target));
    if joined.is_file() {
        Some(joined.to_string_lossy().into_owned())
    } else {
        None
    }
}

fn resolve_include_from_list(paths: &[PathBuf], target: &str) -> Option<String> {
    for path in paths {
        if let Some(result) = is_valid_include(path, target) {
            return Some(result);
        }
    }
    None
}

fn resolve_project(path: &str) -> (ProjectName, &'static IncludeConfig) {
    if let Some((name, project_config)) = Config::get().resolve_project(path) {
        return (
            name.clone(),
            project_config
                .includes
                .as_ref()
                .unwrap_or_else(|| &Config::get().include),
        );
    }
    ("".to_owned(), &Config::get().include)
}

enum ResolvedLocation {
    Absolute(String),
    AbsoluteMany(Vec<String>),
    Relative(String),
    Missing,
}

impl ResolvedLocation {
    fn from_absolute(path: Option<String>) -> Self {
        match path {
            Some(s) => ResolvedLocation::Absolute(s),
            None => ResolvedLocation::Missing,
        }
    }
}

impl IncludeResolver {
    pub fn from_config() -> Self {
        Self {
            patterns: Config::get()
                .include
                .thrift_resolve
                .iter()
                .map(PatternReplacer::from_thrift_config)
                .collect(),
            cache: RwLock::new(HashMap::new()),
        }
    }
    pub fn resolve(&self, from_file: &str, target: &IncludeFile) -> Vec<String> {
        let (project_name, includes) = resolve_project(from_file);
        let cache_key = (project_name, target.clone());
        {
            let read_locked = self.cache.read().unwrap();
            if let Some(cached) = read_locked.get(&cache_key) {
                return cached.clone();
            }
        }
        {
            let resolved = self.resolve_from_disk(from_file, target, includes);
            let mut locked = self.cache.write().unwrap();
            match resolved {
                ResolvedLocation::Absolute(s) => {
                    locked.insert(cache_key, vec![s.clone()]);
                    vec![s]
                }
                ResolvedLocation::AbsoluteMany(v) => {
                    locked.insert(cache_key, v.clone());
                    v
                }
                ResolvedLocation::Relative(s) => {
                    // no caching
                    vec![s]
                }
                ResolvedLocation::Missing => {
                    locked.insert(cache_key, Vec::new());
                    Vec::new()
                }
            }
        }
    }
    pub fn clear_cache(&self) {
        self.cache.write().unwrap().clear()
    }

    fn resolve_from_disk(
        &self,
        from_file: &str,
        target: &IncludeFile,
        include_config: &IncludeConfig,
    ) -> ResolvedLocation {
        match target {
            IncludeFile::System(filename) => {
                self.absolute_or_pattern(&include_config.libraries, filename)
            }
            IncludeFile::Source(filename) => {
                if let Some(base_dir) = Path::new(from_file).parent() {
                    if let Some(valid) = is_valid_include(base_dir, filename) {
                        return ResolvedLocation::Relative(valid);
                    }
                }
                self.absolute_or_pattern(&include_config.sources, filename)
            }
        }
    }

    fn absolute_or_pattern(&self, paths: &[PathBuf], filename: &str) -> ResolvedLocation {
        let absolute_resolve =
            ResolvedLocation::from_absolute(resolve_include_from_list(paths, filename));
        if let ResolvedLocation::Missing = absolute_resolve {
            self.try_patterns(paths, filename)
        } else {
            absolute_resolve
        }
    }

    fn try_patterns(&self, paths: &[PathBuf], filename: &str) -> ResolvedLocation {
        for pattern in &self.patterns {
            if let Some(captures) = pattern.from.captures(filename) {
                let mut glob_file = String::new();
                captures.expand(&pattern.to, &mut glob_file);

                let mut out_paths = Vec::new();
                for path in paths {
                    out_paths.extend(glob(path.join(&glob_file).to_str().unwrap()));
                }
                let out_paths = out_paths
                    .into_iter()
                    .flatten()
                    .filter_map(|p| p.ok().map(|s| s.to_str().unwrap().to_owned()))
                    .collect();
                return ResolvedLocation::AbsoluteMany(out_paths);
            }
        }
        ResolvedLocation::Missing
    }
}
