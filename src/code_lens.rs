use anyhow::Result;
use lsp_types::{CodeLens, Position, Range};

use crate::buck_query::{get_file_owner, get_target_deps};
use crate::commands::{
    ListBuckDepsCommand, ListIncludeReferencesCommand, ListTransitiveIncludesCommand,
};
use crate::include_profiling::{document_references_by_include, transitive_includes};
use crate::workspace::WorkspaceManager;
use crate::{config::Config, document::Document, indexer::Indexer};

async fn buck_deps_code_lens(document: &Document) -> Vec<CodeLens> {
    let workspaces = WorkspaceManager::new();
    let mut lenses = Vec::new();
    if let Some(workspace) = workspaces.get_workspace(&document.source.filename) {
        let root = workspace.project_root();
        for include in &document.includes {
            let resolved_include = match Indexer::get()
                .resolve_include(&document.source.filename, &include.include)
                .first()
            {
                Some(r) => r.clone(),
                None => continue,
            };
            if let Some(relative_include) = workspace.resolve_relative_filename(&resolved_include) {
                let target = get_file_owner(root.clone(), &relative_include).await;
                if let Some(target) = target {
                    let deps = get_target_deps(root.clone(), target.clone()).await;

                    lenses.push(CodeLens {
                        range: include.span.to_range(),
                        command: Some(
                            ListBuckDepsCommand {
                                workdir: root.clone(),
                                target,
                                deps: deps.len(),
                            }
                            .to_command(),
                        ),
                        data: None,
                    });
                }
            }
        }

        if let Some(relative_path) = workspace.resolve_relative_filename(&document.source.filename)
        {
            let target = get_file_owner(root.clone(), &relative_path).await;
            if let Some(target) = target {
                let deps = get_target_deps(root.clone(), target.clone()).await;

                lenses.push(CodeLens {
                    range: Range::new(Position::new(0, 0), Position::new(0, 0)),
                    command: Some(
                        ListBuckDepsCommand {
                            workdir: root.clone(),
                            target,
                            deps: deps.len(),
                        }
                        .to_command(),
                    ),
                    data: None,
                });
            }
        }
    }
    lenses
}

async fn include_references_used_code_lens(document: &Document) -> Vec<CodeLens> {
    let mut lens = Vec::new();

    let refs_by_include = document_references_by_include(document).await;
    for include in &document.includes {
        let refs = match refs_by_include.get(&include.include) {
            Some(refs) => refs,
            None => continue,
        };

        let mut ref_count = 0;
        for refs in refs.values() {
            ref_count += refs.len();
        }

        lens.push(CodeLens {
            range: include.span.to_range(),
            command: Some(
                ListIncludeReferencesCommand {
                    from: document.source.filename.clone(),
                    to: include.include.clone(),
                    references: ref_count,
                }
                .to_command(),
            ),
            data: None,
        });
    }
    lens
}

async fn transitive_includes_code_lens(document: &Document) -> Vec<CodeLens> {
    let mut lenses = Vec::new();
    for include in &document.includes {
        let resolved_includes =
            Indexer::get().resolve_include(&document.source.filename, &include.include);
        if resolved_includes.is_empty() {
            continue;
        }

        let transitive_files = transitive_includes(document, include).await;
        lenses.push(CodeLens {
            range: include.span.to_range(),
            command: Some(
                ListTransitiveIncludesCommand {
                    from: resolved_includes.first().unwrap().clone(),
                    total_includes: transitive_files.len(),
                }
                .to_command(),
            ),
            data: None,
        });
    }
    let top_level_transitive_includes = Indexer::get()
        .transitive_includes(document.source.filename.clone())
        .await;
    lenses.push(CodeLens {
        range: Range::new(Position::new(0, 0), Position::new(0, 0)),
        command: Some(
            ListTransitiveIncludesCommand {
                from: document.source.filename.clone(),
                total_includes: top_level_transitive_includes.len(),
            }
            .to_command(),
        ),
        data: None,
    });
    lenses
}

pub async fn resolve_code_lens(document: &Document) -> Result<Vec<CodeLens>> {
    let mut lenses = Vec::new();
    if Config::get().code_lens.transitive_includes {
        lenses.extend(transitive_includes_code_lens(document).await);
    }
    if Config::get().code_lens.include_refs {
        lenses.extend(include_references_used_code_lens(document).await);
    }
    if Config::get().code_lens.buck_deps {
        lenses.extend(buck_deps_code_lens(document).await);
    }
    Ok(lenses)
}
