use lsp_server::{RequestId, Response, ResponseError};
use std::collections::HashMap;
use std::sync::atomic::{AtomicI32, Ordering};
use std::sync::Mutex;
use tokio::sync::oneshot::{Receiver, Sender};

struct ReplySender {
    channel: Sender<Result<serde_json::Value, ResponseError>>,
}

pub struct ReplyWaiter {
    id: RequestId,
    channel: Receiver<Result<serde_json::Value, ResponseError>>,
}

impl ReplyWaiter {
    pub fn id(&self) -> RequestId {
        self.id.clone()
    }
    pub async fn wait(self) -> Result<serde_json::Value, ResponseError> {
        self.channel.await.unwrap()
    }
}

pub struct ReplyBuffer {
    items: Mutex<HashMap<RequestId, ReplySender>>,
    id_generator: AtomicI32,
}

impl ReplyBuffer {
    pub fn new() -> Self {
        Self {
            items: Mutex::new(HashMap::new()),
            id_generator: AtomicI32::new(10),
        }
    }

    pub fn send(&self) -> ReplyWaiter {
        let id = self.id_generator.fetch_add(1, Ordering::SeqCst);
        let id = RequestId::from(id);
        let (tx, rx) = tokio::sync::oneshot::channel();

        let waiter = ReplyWaiter {
            id: id.clone(),
            channel: rx,
        };
        let sender = ReplySender { channel: tx };
        {
            self.items.lock().unwrap().insert(id, sender);
        }
        waiter
    }
    pub fn handle_response(&self, message: Response) {
        let id = message.id.clone();
        let result = if let Some(value) = message.result {
            Ok(value)
        } else if let Some(err) = message.error {
            Err(err)
        } else {
            Ok(serde_json::Value::Null)
        };
        {
            let mut locked = self.items.lock().unwrap();
            if let Some(sender) = locked.remove(&id) {
                sender.channel.send(result).unwrap();
            }
        }
    }
}
