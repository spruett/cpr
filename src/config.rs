use serde::Deserialize;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

fn default_clang_format() -> String {
    "clang-format".to_owned()
}

fn default_max_include_depth() -> usize {
    20
}

#[derive(Debug, Deserialize, Default)]
pub struct ThriftResolveConfig {
    pub from: String,
    pub to: String,
}

#[derive(Debug, Deserialize, Default)]
pub struct WorkspaceConfig {
    #[serde(default = "Default::default")]
    pub indexed_files: Vec<String>,

    #[serde(default = "Default::default")]
    pub warmup: bool,
}

pub type ProjectName = String;
#[derive(Debug, Deserialize)]

pub struct ProjectConfig {
    pub root: PathBuf,
    pub includes: Option<IncludeConfig>,

    #[serde(default = "Default::default")]
    pub workspace: WorkspaceConfig,
}

#[derive(Debug, Deserialize, Default)]
pub struct IncludeConfig {
    #[serde(default = "Default::default")]
    pub sources: Vec<PathBuf>,
    #[serde(default = "Default::default")]
    pub libraries: Vec<PathBuf>,

    #[serde(default = "Default::default")]
    pub prefer_library_format: Vec<String>,

    #[serde(default = "Default::default")]
    pub thrift_resolve: Vec<ThriftResolveConfig>,

    #[serde(default = "default_max_include_depth")]
    pub max_include_depth: usize,
}
#[derive(Debug, Deserialize, Default)]
pub struct FormatConfig {
    #[serde(default = "default_clang_format")]
    pub clang_format_path: String,
}

#[derive(Debug, Deserialize, Default)]
pub struct MacroExpansionConfig {
    #[serde(default = "Default::default")]
    pub expand_to_definition: HashMap<String, Vec<String>>,

    #[serde(default = "Default::default")]
    pub ignore: Vec<String>,
}

#[derive(Debug, Deserialize, Default)]
pub struct CompletionConfig {
    pub truncate_after_rank_diff: Option<usize>,

    pub type_score: Option<usize>,
    pub scope_score: Option<usize>,

    pub local_score: Option<usize>,
    pub reference_score: Option<usize>,

    pub case_score: Option<usize>,
    pub fuzzy_score: Option<usize>,

    pub cache_debounce_min_secs: Option<u64>,
    pub cache_debounce_max_secs: Option<u64>,

    pub rebuild_on_trigger: Option<bool>,
}

#[derive(Debug, Deserialize, Default)]
pub struct DiagnosticConfig {
    #[serde(default = "Default::default")]
    pub syntax: bool,
    #[serde(default = "Default::default")]
    pub unknown_references: bool,
    #[serde(default = "Default::default")]
    pub reference_scoping: bool,
    #[serde(default = "Default::default")]
    pub includes: bool,

    #[serde(default = "Default::default")]
    pub unused_locals: bool,
}

#[derive(Debug, Deserialize, Default)]
pub struct CodeLensConfig {
    #[serde(default = "Default::default")]
    pub transitive_includes: bool,

    #[serde(default = "Default::default")]
    pub include_refs: bool,

    #[serde(default = "Default::default")]
    pub buck_deps: bool,
}

#[derive(Debug, Default, Deserialize)]
pub struct CodeActionConfig {
    #[serde(default = "Default::default")]
    pub insert_include: Option<bool>,

    #[serde(default = "Default::default")]
    pub show_ast: Option<bool>,
}

#[derive(Debug, Default, Deserialize)]
pub struct FeatureConfig {
    #[serde(default = "Default::default")]
    pub autocomplete_parameter_template: bool,

    #[serde(default = "Default::default")]
    pub autocomplete_scope: bool,

    #[serde(default = "Default::default")]
    pub semantic_tokens: bool,

    #[serde(default = "Default::default")]
    pub disable_clangd_status: bool,
}

#[derive(Debug, Deserialize, Default)]
pub struct BuckConfig {
    pub path: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    #[serde(default = "Default::default")]
    pub include: IncludeConfig,
    #[serde(default = "Default::default")]
    pub format: FormatConfig,

    #[serde(default = "Default::default")]
    pub buck: BuckConfig,

    #[serde(default = "Default::default")]
    pub projects: HashMap<ProjectName, ProjectConfig>,

    #[serde(default = "Default::default")]
    pub macros: MacroExpansionConfig,

    #[serde(default = "Default::default")]
    pub ignore_initialize: bool,

    pub diagnostics: Option<DiagnosticConfig>,

    #[serde(default = "Default::default")]
    pub features: FeatureConfig,

    #[serde(default = "Default::default")]
    pub code_lens: CodeLensConfig,

    #[serde(default = "Default::default")]
    pub code_actions: CodeActionConfig,

    #[serde(default = "Default::default")]
    pub completion: CompletionConfig,
}

fn parse_config() -> Config {
    let parsed_config = || -> anyhow::Result<Config> {
        let config_path = match std::env::var("CONFIG_PATH") {
            Ok(path) => Path::new(&path).to_owned(),
            Err(_) => Path::new(&std::env::var("HOME")?).join(".gels.conf"),
        };
        if !config_path.is_file() {
            anyhow::bail!("config does not exist: {:?}", config_path);
        }
        let config_content = std::fs::read_to_string(config_path)?;
        let config = hocon::de::from_str(&config_content)?;
        log::info!("config loaded: {:?}", config);
        Ok(config)
    }();

    match parsed_config {
        Ok(config) => config,
        Err(e) => {
            eprintln!("failed to load config: {}", e);
            hocon::de::from_str(include_str!("configs/default.conf"))
                .expect("failed to parse default config")
        }
    }
}

impl Config {
    pub fn get() -> &'static Self {
        lazy_static::lazy_static! {
            static ref CONFIG: Config = parse_config();
        }
        &*CONFIG
    }

    pub fn resolve_project<P: AsRef<Path>>(
        &self,
        filename: P,
    ) -> Option<(&ProjectName, &ProjectConfig)> {
        let path = filename.as_ref();
        for (name, project) in &self.projects {
            if path.starts_with(&project.root) {
                return Some((name, project));
            }
        }
        None
    }
}
