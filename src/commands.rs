use std::{
    collections::{HashMap, HashSet},
    fmt::Write,
    time::Duration,
};

use crate::{
    buck_query::get_target_deps,
    include_profiling::{document_references_by_include, reverse_include_mapping},
    indexer::{IncludeDepth, Indexer, RankedItem},
    language_server::LanguageServer,
    symbols::IncludeFile,
};
use anyhow::{bail, Result};
use lsp_types::{
    request::{Request, ShowDocument},
    Command, Range, ShowDocumentParams, Url,
};
use rand::random;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use tree_sitter::Point;

const LIST_TRANSITIVE_INCLUDES: &str = "gels/transitiveIncludes";
const LIST_INCLUDE_REFERENCES: &str = "gels/listIncludeReferences";
const LIST_BUCK_DEPS: &str = "gels/buckDeps";
const SHOW_AST: &str = "gels/showAst";

pub fn implemented_commands() -> Vec<String> {
    vec![
        LIST_TRANSITIVE_INCLUDES,
        LIST_INCLUDE_REFERENCES,
        LIST_BUCK_DEPS,
        SHOW_AST,
    ]
    .into_iter()
    .map(|c| c.to_owned())
    .collect()
}

fn make_command<C: Serialize>(title: String, method: &str, arg: C) -> Command {
    Command {
        title,
        command: method.to_owned(),
        arguments: Some(vec![serde_json::to_value(arg).unwrap()]),
    }
}

async fn show_document(server: &LanguageServer, filename: String) {
    let _ = server
        .send_request(
            ShowDocument::METHOD,
            ShowDocumentParams {
                uri: Url::from_file_path(filename).unwrap(),
                external: None,
                take_focus: None,
                selection: None,
            },
        )
        .await;
}

fn write_to_temporary_file(content: String) -> Result<String> {
    let tmp_path = std::env::temp_dir();
    let random_number: u32 = random();
    let tmp_file = tmp_path.join(format!("gels.out.{}", random_number));
    std::fs::write(&tmp_file, content)?;
    Ok(tmp_file.to_string_lossy().to_string())
}

async fn show_in_temporary_file(server: &LanguageServer, content: String) {
    let filename = match write_to_temporary_file(content) {
        Ok(filename) => filename,
        Err(_) => return,
    };
    show_document(server, filename.clone()).await;
    tokio::spawn(async move {
        tokio::time::sleep(Duration::from_secs(180)).await;
        let _ = std::fs::remove_file(filename);
    });
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ListTransitiveIncludesCommand {
    pub from: String,
    pub total_includes: usize,
}

impl ListTransitiveIncludesCommand {
    pub fn to_command(&self) -> Command {
        make_command(
            format!("{} includes", self.total_includes),
            LIST_TRANSITIVE_INCLUDES,
            self,
        )
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ListIncludeReferencesCommand {
    pub from: String,
    pub to: IncludeFile,
    pub references: usize,
}

impl ListIncludeReferencesCommand {
    pub fn to_command(&self) -> Command {
        make_command(
            format!("{} references", self.references),
            LIST_INCLUDE_REFERENCES,
            self,
        )
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ListBuckDepsCommand {
    pub workdir: String,
    pub target: String,
    pub deps: usize,
}

impl ListBuckDepsCommand {
    pub fn to_command(&self) -> Command {
        make_command(format!("{} deps", self.deps), LIST_BUCK_DEPS, self)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ShowAstCommand {
    pub filename: String,
    pub range: Range,
}

impl ShowAstCommand {
    pub fn to_command(&self) -> Command {
        make_command("show syntax tree".to_owned(), SHOW_AST, self)
    }
}

async fn handle_show_ast(server: &LanguageServer, argument: Value) -> Result<()> {
    let argument = serde_json::from_value::<ShowAstCommand>(argument)?;
    let parsed_file = Indexer::get().get_index(argument.filename.clone()).await?;
    let content = {
        let ast = match parsed_file.ast() {
            Some(ast) => ast,
            None => bail!("no AST for given file: {}", argument.filename),
        };

        let ast = ast.descendant_for_point_range(
            Point::new(
                argument.range.start.line as usize,
                argument.range.start.character as usize,
            ),
            Point::new(
                argument.range.end.line as usize,
                argument.range.end.character as usize,
            ),
        );
        let ast = match ast {
            Some(ast) => ast,
            None => bail!("no AST at given range"),
        };
        ast.to_sexp()
    };

    show_in_temporary_file(server, content).await;
    Ok(())
}

async fn total_lines(includes: &HashSet<RankedItem<String, IncludeDepth>>) -> usize {
    let mut total = 0;
    for file in includes {
        if let Ok(content) = Indexer::get().get_file_content(file.value.clone()).await {
            total += content.content.lines().count();
        }
    }
    total
}

async fn handle_list_transitive_includes(server: &LanguageServer, argument: Value) -> Result<()> {
    let argument = serde_json::from_value::<ListTransitiveIncludesCommand>(argument)?;
    let document = Indexer::get().get_index(argument.from.clone()).await?;
    let transitive_includes = Indexer::get()
        .transitive_includes(argument.from.clone())
        .await;
    let reverse_include_mapping = reverse_include_mapping(document.document()).await;

    let mut formatted = String::new();
    writeln!(&mut formatted, "Transitive includes of {}", argument.from).unwrap();
    writeln!(&mut formatted, "=========================================").unwrap();
    writeln!(&mut formatted).unwrap();
    writeln!(
        &mut formatted,
        "{} files, {} total lines",
        transitive_includes.len(),
        total_lines(&transitive_includes).await
    )
    .unwrap();
    writeln!(&mut formatted, "=========================================").unwrap();
    for include in &transitive_includes {
        let sub_transitive_includes = Indexer::get()
            .transitive_includes(include.value.clone())
            .await;
        let lines = Indexer::get()
            .get_file_content(include.value.clone())
            .await?
            .content
            .lines()
            .count();
        let transitive_lines = total_lines(&sub_transitive_includes).await;

        writeln!(
            &mut formatted,
            " + {} ({} lines, {} transitive includes, {} transitive lines)",
            include.value,
            lines,
            sub_transitive_includes.len(),
            transitive_lines
        )
        .unwrap();

        let included_by = reverse_include_mapping
            .get_vec(&include.value)
            .cloned()
            .unwrap_or_default();
        for filename in included_by {
            writeln!(
                &mut formatted,
                "   + included by {}",
                filename.value.as_include_str()
            )
            .unwrap();
        }
    }

    show_in_temporary_file(server, formatted).await;
    Ok(())
}

async fn handle_list_include_references(server: &LanguageServer, argument: Value) -> Result<()> {
    let argument = serde_json::from_value::<ListIncludeReferencesCommand>(argument)?;
    let document = Indexer::get().get_index(argument.from).await?;
    let refs_by_include = document_references_by_include(document.document()).await;

    let mut buffer = String::new();
    let refs = match refs_by_include.get(&argument.to) {
        Some(refs) => refs.clone(),
        None => HashMap::new(),
    };

    for (filename, refs) in refs {
        writeln!(&mut buffer, "{}", filename).unwrap();
        for reference in refs {
            writeln!(&mut buffer, " + {}", reference.with_scope()).unwrap();
        }
    }
    show_in_temporary_file(server, buffer).await;
    Ok(())
}

async fn handle_list_buck_deps(server: &LanguageServer, argument: Value) -> Result<()> {
    let argument = serde_json::from_value::<ListBuckDepsCommand>(argument)?;
    let deps = get_target_deps(argument.workdir, argument.target.clone()).await;

    let mut buffer = String::new();
    writeln!(&mut buffer, "Transitive deps of {}", argument.target).unwrap();
    for dep in deps {
        writeln!(&mut buffer, " + {}", dep).unwrap();
    }
    show_in_temporary_file(server, buffer).await;
    Ok(())
}

pub async fn handle_command(server: &LanguageServer, command: &str, argument: Value) -> Result<()> {
    match command {
        LIST_TRANSITIVE_INCLUDES => handle_list_transitive_includes(server, argument).await,
        LIST_INCLUDE_REFERENCES => handle_list_include_references(server, argument).await,
        LIST_BUCK_DEPS => handle_list_buck_deps(server, argument).await,
        SHOW_AST => handle_show_ast(server, argument).await,
        _ => anyhow::bail!("unknown command: {}", command),
    }
}
