use crate::config::Config;
use crate::span::SourceFile;
use anyhow::Result;
use std::path::{Path, PathBuf};
use std::process::Stdio;
use tokio::io::AsyncWriteExt;

fn get_clang_format_executable() -> String {
    Config::get().format.clang_format_path.clone()
}

fn find_clang_format_executable() -> Option<PathBuf> {
    let exe_name = get_clang_format_executable();
    let exe_path = Path::new(&exe_name);
    if exe_path.is_file() {
        return Some(exe_path.to_owned());
    }

    let env_path = std::env::var_os("PATH")?;
    for path in std::env::split_paths(&env_path) {
        let full_path = path.join(exe_path);
        if full_path.is_file() {
            return Some(full_path);
        }
    }
    None
}

async fn execute_clang_format(args: &[String], content: String) -> Result<String> {
    let executable = find_clang_format_executable()
        .ok_or_else(|| anyhow::anyhow!("could not find clang-format executable"))?;
    let mut subprocess = tokio::process::Command::new(executable)
        .args(args)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;
    let mut stdin = subprocess.stdin.take().expect("no opened stdin");
    let write_handle = tokio::spawn(async move { stdin.write_all(content.as_bytes()).await });
    write_handle.await??;
    let output = subprocess.wait_with_output().await?;
    anyhow::ensure!(output.status.success(), "clang-format failed");
    Ok(String::from_utf8(output.stdout)?)
}

pub fn has_formatter() -> bool {
    find_clang_format_executable().is_some()
}

pub async fn format_full(file: &SourceFile) -> Result<String> {
    let args = vec!["--assume-filename".to_owned(), file.filename.clone()];
    execute_clang_format(&args, file.content.clone()).await
}

pub async fn format_range(file: &SourceFile, begin_line: usize, end_line: usize) -> Result<String> {
    let args = vec![
        "--assume-filename".to_owned(),
        file.filename.clone(),
        "--lines".to_owned(),
        format!("{}:{}", begin_line + 1, end_line + 1),
    ];
    execute_clang_format(&args, file.content.clone()).await
}
