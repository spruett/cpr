use std::collections::HashMap;

use anyhow::Result;
use lsp_types::{CodeAction, CodeActionKind, Position, Range, TextEdit, Url, WorkspaceEdit};

use crate::{
    commands::ShowAstCommand,
    config::Config,
    document::IncludedFile,
    include_resolver::is_includable_header,
    indexer::Indexer,
    symbols::IncludeFile,
    text_util::identifier_before,
    workspace::{WorkspaceManager, WorkspaceSymbolQuery},
};

async fn insert_include_edits(filename: String, to_insert: String) -> Result<WorkspaceEdit> {
    let document = Indexer::get().get_index(filename).await?;

    let mut insert_position = Position::new(0, 0);
    let mut prev: Option<&IncludedFile> = None;
    for include in &document.document().includes {
        if to_insert < include.include.as_include_str() {
            if let Some(p) = prev {
                if to_insert > p.include.as_include_str() {
                    insert_position = Position::new(include.span.begin.row as u32, 0);
                }
            } else {
                insert_position = Position::new(include.span.begin.row as u32, 0);
            }
        }
        prev = Some(include);
    }

    let edits = vec![TextEdit {
        range: Range {
            start: insert_position,
            end: insert_position,
        },
        new_text: format!("#include {}\n", to_insert),
    }];
    let uri = Url::from_file_path(&document.document().source.filename).unwrap();
    let mut edits_per_doc = HashMap::new();
    edits_per_doc.insert(uri, edits);
    let edit = WorkspaceEdit {
        changes: Some(edits_per_doc),
        ..Default::default()
    };
    Ok(edit)
}

async fn insert_includes(filename: String, range: Range) -> Result<Vec<CodeAction>> {
    let mut code_actions = Vec::new();
    if let Some(workspace) = WorkspaceManager::new().get_workspace(&filename) {
        let file_content = Indexer::get().get_file_content(filename.clone()).await?;
        let identifier_result = identifier_before(
            &file_content.content,
            Position::new(range.end.line, range.end.character + 1),
        );
        let symbol_query = WorkspaceSymbolQuery {
            symbol: identifier_result.full_identifier,
            scope: identifier_result.scope,
            prefix_match: false,
        };
        let mut existing_includes = Indexer::get().direct_includes(filename.clone()).await;
        existing_includes.insert(filename.clone());
        let potential_new_includes = workspace
            .query_symbols(&symbol_query)
            .await
            .into_iter()
            .map(|(filename, _)| filename)
            .filter(|filename| {
                !existing_includes.contains(filename) && is_includable_header(filename)
            })
            .collect::<Vec<String>>();

        for potential_new_include in &potential_new_includes {
            if let Some(resolved_include) =
                workspace.resolve_relative_filename(potential_new_include)
            {
                let include_file = if workspace.is_library_filename(potential_new_include) {
                    IncludeFile::System(resolved_include.to_owned())
                } else {
                    IncludeFile::Source(resolved_include.to_owned())
                };
                code_actions.push(CodeAction {
                    title: format!("insert include: {}", resolved_include),
                    kind: Some(CodeActionKind::QUICKFIX),
                    edit: Some(
                        insert_include_edits(filename.clone(), include_file.as_include_str())
                            .await?,
                    ),
                    is_preferred: Some(potential_new_includes.len() == 1),
                    ..Default::default()
                });
            }
        }
    }
    Ok(code_actions)
}

pub async fn show_ast(filename: String, range: Range) -> Result<Vec<CodeAction>> {
    let mut code_actions = Vec::new();
    if range.start != range.end {
        let command = ShowAstCommand { filename, range }.to_command();
        code_actions.push(CodeAction {
            title: "show AST".to_owned(),
            command: Some(command),
            ..Default::default()
        });
    }
    Ok(code_actions)
}

pub async fn get_code_actions(filename: String, range: Range) -> Result<Vec<CodeAction>> {
    let mut code_actions = Vec::new();
    if Config::get().code_actions.insert_include.unwrap_or(true) {
        code_actions.extend(insert_includes(filename.clone(), range).await?);
    }
    if Config::get().code_actions.show_ast.unwrap_or(false) {
        code_actions.extend(show_ast(filename.clone(), range).await?);
    }
    Ok(code_actions)
}
