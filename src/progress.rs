use std::{
    collections::HashMap,
    sync::{
        atomic::{AtomicBool, AtomicUsize, Ordering},
        Arc, Mutex,
    },
};

use rand::Rng;

#[derive(Default)]
pub struct ProgressItem {
    token: Option<String>,
    title: String,
    message: String,
    finished_items: AtomicUsize,
    total_items: AtomicUsize,
    done: AtomicBool,
    canceled: AtomicBool,
}

impl ProgressItem {
    pub fn noop() -> Arc<Self> {
        Arc::new(Self::new("".to_owned(), "".to_owned()))
    }

    pub fn new(title: String, message: String) -> Self {
        let token_int: usize = rand::thread_rng().gen();
        Self {
            token: Some(token_int.to_string()),
            title,
            message,
            ..Default::default()
        }
    }

    pub fn token(&self) -> Option<String> {
        self.token.clone()
    }

    pub fn add_task(&self) {
        self.total_items.fetch_add(1, Ordering::SeqCst);
    }
    pub fn complete_task(&self) {
        self.finished_items.fetch_add(1, Ordering::SeqCst);
    }
    pub fn complete(&self) {
        self.done.store(true, Ordering::Relaxed)
    }

    pub fn done(&self) -> bool {
        self.done.load(Ordering::Relaxed)
    }
    pub fn canceled(&self) -> bool {
        self.canceled.load(Ordering::Relaxed)
    }

    pub fn title(&self) -> String {
        self.title.clone()
    }
    pub fn message(&self) -> String {
        if self.done() {
            format!("{}: (done)", self.message)
        } else {
            format!(
                "{}: ({}/{})",
                self.message,
                self.finished_items.load(Ordering::Relaxed),
                self.total_items.load(Ordering::Relaxed)
            )
        }
    }
    pub fn progress(&self) -> usize {
        let total = self.total_items.load(Ordering::Relaxed);
        if total == 0 {
            0
        } else {
            (100 * self.finished_items.load(Ordering::Relaxed)) / total
        }
    }
}

pub struct ProgressRegistry {
    items: Mutex<HashMap<String, Arc<ProgressItem>>>,
}

impl ProgressRegistry {
    pub fn new() -> Self {
        Self {
            items: Mutex::new(HashMap::new()),
        }
    }

    pub fn add_new(&self, title: String, message: String) -> Arc<ProgressItem> {
        self.add(ProgressItem::new(title, message))
    }

    pub fn clear(&self, id: String) {
        self.items.lock().unwrap().remove(&id);
    }

    fn add(&self, item: ProgressItem) -> Arc<ProgressItem> {
        let item = Arc::new(item);
        if let Some(token) = item.token() {
            let mut locked = self.items.lock().unwrap();
            locked.insert(token, item.clone());
        }
        item
    }
}
