use crate::completions::symbol_filter_text;
use crate::cpp_file::CppFile;
use crate::diagnostics::Diagnostic;
use crate::span::{SourceFile, SourceSpan};
use crate::symbols::{IncludeFile, LocalSymbolEntry, SymbolAtPoint, SymbolEntry, SymbolReference};
use crate::thrift_file::ThriftFile;
use lsp_types::Position;
use multimap::MultiMap;
use std::path::Path;
use std::sync::Arc;
use tree_sitter::Node;

pub struct IncludedFile {
    pub include: IncludeFile,
    pub span: SourceSpan,
}

#[derive(Clone)]
pub struct ScopeSpan {
    pub scope: String,
    pub span: SourceSpan,
}

pub struct Document {
    pub source: Arc<SourceFile>,
    pub includes: Vec<IncludedFile>,
    pub symbols: MultiMap<String, SymbolEntry>,
    pub symbol_references: MultiMap<String, SymbolReference>,
    pub local_symbols: MultiMap<String, LocalSymbolEntry>,
    pub diagnostics: Vec<Diagnostic>,
    pub scopes: Vec<ScopeSpan>,
}

impl Document {
    pub fn find_symbol(&self, symbol: &str) -> Vec<SymbolEntry> {
        if let Some(entries) = self.symbols.get_vec(symbol) {
            entries.clone()
        } else {
            Vec::new()
        }
    }

    pub fn scan_symbols(&self, symbol: &str) -> Vec<SymbolEntry> {
        self.symbols
            .iter_all()
            .filter(|(name, _symbols)| {
                let name_filter = symbol_filter_text(name);
                name_filter.starts_with(&symbol_filter_text(symbol))
            })
            .flat_map(|(_symbol, symbols)| symbols.clone())
            .collect()
    }

    pub fn find_symbol_references(&self, symbol: &str) -> Vec<SymbolReference> {
        if let Some(refs) = self.symbol_references.get_vec(symbol) {
            refs.clone()
        } else {
            Vec::new()
        }
    }

    pub fn find_scopes(&self, position: &Position) -> Vec<&ScopeSpan> {
        self.scopes
            .iter()
            .filter(|scope| scope.span.contains(position))
            .collect()
    }

    pub fn find_local_symbol(&self, symbol: &str) -> Vec<LocalSymbolEntry> {
        if let Some(locals) = self.local_symbols.get_vec(symbol) {
            locals.clone()
        } else {
            Vec::new()
        }
    }
}

pub trait ParsedFile: Send + Sync {
    fn parse_from_file(content: Arc<SourceFile>) -> Arc<Self>
    where
        Self: Sized;
    fn get_symbol_at_point(&self, line: usize, col: usize) -> SymbolAtPoint;
    fn document(&self) -> &Document;
    fn ast(&self) -> Option<Node>;
}

pub struct NoopFile {
    doc: Document,
}

impl ParsedFile for NoopFile {
    fn parse_from_file(content: Arc<SourceFile>) -> Arc<Self> {
        Arc::new(Self {
            doc: Document {
                source: content,
                includes: Vec::new(),
                symbols: MultiMap::new(),
                symbol_references: MultiMap::new(),
                local_symbols: MultiMap::new(),
                diagnostics: Vec::new(),
                scopes: Vec::new(),
            },
        })
    }

    fn get_symbol_at_point(&self, _line: usize, _col: usize) -> SymbolAtPoint {
        SymbolAtPoint::Missing
    }

    fn document(&self) -> &Document {
        &self.doc
    }

    fn ast(&self) -> Option<Node> {
        None
    }
}

pub fn parse_file(content: Arc<SourceFile>) -> Arc<dyn ParsedFile> {
    let extension = Path::new(&content.filename)
        .extension()
        .and_then(|e| e.to_str());
    match extension {
        Some("thrift") => ThriftFile::parse_from_file(content),
        Some("hpp") | Some("h") | Some("cpp") | Some("c") | None => {
            CppFile::parse_from_file(content)
        }
        _ => {
            log::info!("unknown extension {:?}", extension);
            NoopFile::parse_from_file(content)
        }
    }
}
