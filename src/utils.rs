use std::{collections::HashSet, hash::Hash};

use crate::{
    indexer::Indexer,
    symbols::{ResolvedSymbol, SymbolReference},
};

pub fn unique_by<I, T, F, K>(iterator: I, key_fn: F) -> Vec<T>
where
    I: Iterator<Item = T>,
    F: Fn(&T) -> K,
    K: Eq + Hash,
{
    let mut dedup = HashSet::new();
    iterator
        .filter(|item| dedup.insert(key_fn(item)))
        .collect::<Vec<_>>()
}

pub async fn filter_references_to_resolved(
    resolved_symbols: &[ResolvedSymbol],
    files_to_refs: Vec<(String, Vec<SymbolReference>)>,
) -> Vec<(String, Vec<SymbolReference>)> {
    let resolved_files: HashSet<_> = resolved_symbols
        .iter()
        .filter_map(|resolved| match resolved {
            ResolvedSymbol::Global((file, _)) => Some(file.filename.clone()),
            ResolvedSymbol::Local(_) => None,
            ResolvedSymbol::Include(_) => None,
        })
        .collect();

    let mut filtered_refs = Vec::new();
    for (filename, refs) in files_to_refs {
        let transitive_includes: HashSet<_> = Indexer::get()
            .transitive_includes(filename.clone())
            .await
            .into_iter()
            .map(|ranked_filename| ranked_filename.value)
            .collect();
        if !transitive_includes.is_disjoint(&resolved_files) {
            filtered_refs.push((filename, refs))
        }
    }
    filtered_refs
}
