use std::{
    collections::HashMap,
    future::Future,
    hash::Hash,
    sync::{Arc, Mutex},
    time::Duration,
};

use tokio::sync::OnceCell;

#[derive(Default)]
struct CachedItem<T> {
    cell: OnceCell<T>,
}
impl<T> CachedItem<T> {
    pub fn new() -> Arc<Self> {
        Arc::new(Self {
            cell: OnceCell::new(),
        })
    }
}

pub struct AsyncCache<K, V> {
    items: Mutex<HashMap<K, Arc<CachedItem<V>>>>,
}

impl<K, V> AsyncCache<K, V>
where
    K: Hash + Eq + PartialEq + Clone,
    V: Clone,
{
    pub fn new() -> Self {
        Self {
            items: Default::default(),
        }
    }

    pub fn invalidate(&self, key: &K) {
        self.items.lock().unwrap().remove(key);
    }

    pub async fn get<F: Future<Output = V>>(&self, key: &K, fut: F) -> V {
        let cached_item = {
            let mut items = self.items.lock().unwrap();
            let item = items.entry(key.clone()).or_insert_with(CachedItem::new);
            item.clone()
        };
        cached_item.cell.get_or_init(|| fut).await.clone()
    }
}

pub struct TimedAsyncCache<K, V> {
    cache: Arc<AsyncCache<K, V>>,
}

impl<K, V> TimedAsyncCache<K, V>
where
    K: 'static + Hash + Eq + PartialEq + Clone + Send + Sync,
    V: 'static + Clone + Send + Sync,
{
    pub fn new() -> Self {
        Self {
            cache: Arc::new(AsyncCache::new()),
        }
    }

    pub fn invalidate(&self, key: &K) {
        self.cache.invalidate(key);
    }

    pub async fn get<F: Future<Output = V>>(&self, key: &K, fut: F, ttl: Duration) -> V {
        let value = self.cache.get(key, fut).await;

        let cache = self.cache.clone();
        let key = key.clone();
        tokio::spawn(async move {
            tokio::time::sleep(ttl).await;
            cache.invalidate(&key);
        });
        value
    }
}
