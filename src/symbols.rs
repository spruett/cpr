use std::sync::Arc;

use crate::{
    document::ScopeSpan,
    span::{SourceFile, SourceSpan},
};
use lsp_types::{CompletionItemKind, CompletionItemTag, SymbolKind};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, PartialOrd, Ord, Eq, PartialEq, Hash, Clone)]
pub enum IncludeFile {
    System(String),
    Source(String),
}

impl IncludeFile {
    pub fn as_include_str(&self) -> String {
        match self {
            IncludeFile::System(s) => format!("<{}>", s),
            IncludeFile::Source(s) => format!("\"{}\"", s),
        }
    }
}

pub enum SymbolAtPoint {
    Include(IncludeFile),
    Identifier(String),
    Missing,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum SymbolTag {
    Deprecated,
}

impl SymbolTag {
    pub fn to_completion_tag(&self) -> CompletionItemTag {
        CompletionItemTag::Deprecated
    }
    pub fn to_symbol_info_tag(&self) -> lsp_types::SymbolTag {
        lsp_types::SymbolTag::Deprecated
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum SymbolEntryType {
    ClassDeclaration,
    ClassDefinition,
    ClassConstructorDeclaration,
    ClassConstructorDefinition,
    // TODO: destructors
    // ClassDestructorDeclaration,
    // ClassDestructionDefinition,
    EnumDeclaration,
    EnumDefinition,
    EnumValue,
    VariableDeclaration,
    VariableDefinition,
    MacroDefinition,
    FunctionDeclaration,
    FunctionDefinition,
    MethodDeclaration,
    MethodDefinition,
    MemberDeclaration,
    TypeAlias,
    Namespace,
    TemplateType,
    Parameter,
}

impl SymbolEntryType {
    pub fn definition_match_rank(&self) -> i32 {
        match self {
            SymbolEntryType::ClassDefinition | SymbolEntryType::EnumDefinition => 100,
            SymbolEntryType::FunctionDefinition | SymbolEntryType::MethodDefinition => 90,
            SymbolEntryType::MacroDefinition => 80,
            SymbolEntryType::VariableDefinition
            | SymbolEntryType::VariableDeclaration
            | SymbolEntryType::MemberDeclaration
            | SymbolEntryType::EnumValue => 70,
            SymbolEntryType::ClassConstructorDefinition => 60,
            _ => 0,
        }
    }

    pub fn declaration_match_rank(&self) -> i32 {
        match self {
            SymbolEntryType::ClassDefinition | SymbolEntryType::EnumDefinition => 100,
            SymbolEntryType::MemberDeclaration
            | SymbolEntryType::MethodDeclaration
            | SymbolEntryType::FunctionDeclaration => 90,
            SymbolEntryType::ClassDeclaration | SymbolEntryType::EnumDeclaration => 80,
            _ => 0,
        }
    }

    pub fn to_symbol_kind(&self) -> SymbolKind {
        match self {
            SymbolEntryType::ClassDeclaration | SymbolEntryType::ClassDefinition => {
                SymbolKind::Class
            }
            SymbolEntryType::MethodDeclaration | SymbolEntryType::MethodDefinition => {
                SymbolKind::Method
            }
            SymbolEntryType::FunctionDeclaration | SymbolEntryType::FunctionDefinition => {
                SymbolKind::Function
            }
            SymbolEntryType::EnumDeclaration | SymbolEntryType::EnumDefinition => SymbolKind::Enum,
            SymbolEntryType::MemberDeclaration => SymbolKind::Field,
            SymbolEntryType::VariableDeclaration | SymbolEntryType::VariableDefinition => {
                SymbolKind::Variable
            }
            SymbolEntryType::ClassConstructorDeclaration
            | SymbolEntryType::ClassConstructorDefinition => SymbolKind::Constructor,
            SymbolEntryType::MacroDefinition => SymbolKind::Constant,
            SymbolEntryType::TypeAlias => SymbolKind::TypeParameter,
            SymbolEntryType::EnumValue => SymbolKind::Constant,
            SymbolEntryType::Namespace => SymbolKind::Namespace,
            SymbolEntryType::Parameter => SymbolKind::Variable,
            SymbolEntryType::TemplateType => SymbolKind::TypeParameter,
        }
    }

    pub fn to_completion_kind(&self) -> CompletionItemKind {
        match self {
            SymbolEntryType::ClassDeclaration | SymbolEntryType::ClassDefinition => {
                CompletionItemKind::Class
            }
            SymbolEntryType::MethodDeclaration | SymbolEntryType::MethodDefinition => {
                CompletionItemKind::Method
            }
            SymbolEntryType::FunctionDeclaration | SymbolEntryType::FunctionDefinition => {
                CompletionItemKind::Function
            }
            SymbolEntryType::EnumDeclaration | SymbolEntryType::EnumDefinition => {
                CompletionItemKind::Enum
            }
            SymbolEntryType::MemberDeclaration => CompletionItemKind::Field,
            SymbolEntryType::VariableDeclaration | SymbolEntryType::VariableDefinition => {
                CompletionItemKind::Variable
            }
            SymbolEntryType::ClassConstructorDeclaration
            | SymbolEntryType::ClassConstructorDefinition => CompletionItemKind::Constructor,
            SymbolEntryType::MacroDefinition => CompletionItemKind::Constant,
            SymbolEntryType::TypeAlias => CompletionItemKind::TypeParameter,
            SymbolEntryType::EnumValue => CompletionItemKind::EnumMember,
            SymbolEntryType::Namespace => CompletionItemKind::Module,
            SymbolEntryType::Parameter => CompletionItemKind::Value,
            SymbolEntryType::TemplateType => CompletionItemKind::TypeParameter,
        }
    }

    pub fn is_member_or_method(&self) -> bool {
        matches!(
            self,
            SymbolEntryType::MethodDeclaration
                | SymbolEntryType::MethodDefinition
                | SymbolEntryType::MemberDeclaration
        )
    }
    pub fn is_callable(&self) -> bool {
        matches!(
            self,
            SymbolEntryType::MethodDeclaration
                | SymbolEntryType::MethodDefinition
                | SymbolEntryType::ClassConstructorDeclaration
                | SymbolEntryType::ClassConstructorDefinition
                | SymbolEntryType::FunctionDefinition
                | SymbolEntryType::FunctionDeclaration
        )
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ParameterEntry {
    pub content: String,
    pub name: String,
    pub param_type: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct TypeReference {
    pub scope: Option<Box<TypeReference>>,
    pub name: String,
    pub params: Vec<TypeReference>,
}

impl TypeReference {
    pub fn new(name: String) -> Self {
        Self {
            name,
            params: Vec::new(),
            scope: None,
        }
    }

    pub fn expand(&self) -> Vec<String> {
        let mut items = vec![self.name.clone()];
        for param in &self.params {
            items.extend(param.expand().into_iter())
        }
        items
    }

    pub fn type_string(&self) -> String {
        let mut buffer = String::new();
        if let Some(scope) = &self.scope {
            buffer += &scope.type_string();
            buffer += "::";
        }
        buffer += &self.name;
        if !self.params.is_empty() {
            buffer += "<";
            buffer += &self
                .params
                .iter()
                .map(|ty| ty.type_string())
                .collect::<Vec<_>>()
                .join(", ");
            buffer += ">";
        }
        buffer
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct SymbolEntry {
    pub symbol: String,
    pub span: SourceSpan,
    pub span_with_params: Option<SourceSpan>,
    pub docs: Option<String>,
    pub signature: Option<String>,
    pub entry_type: SymbolEntryType,
    pub scope: Option<String>,
    pub params: Vec<ParameterEntry>,
    pub base_classes: Vec<SymbolReference>,
    pub type_string: Option<String>,
    pub type_name: Option<TypeReference>,
    pub tags: Vec<SymbolTag>,
}

impl SymbolEntry {
    pub fn type_string(&self) -> Option<String> {
        if self.entry_type.is_callable() {
            let params_string = self
                .params
                .iter()
                .map(|param| format!("{} {}", param.param_type, param.name))
                .collect::<Vec<String>>()
                .join(", ");
            let return_string = match &self.type_string {
                Some(name) => format!(" -> {}", name),
                None => "".to_owned(),
            };
            Some(format!(
                "{}({}){}",
                self.symbol, params_string, return_string
            ))
        } else if matches!(
            self.entry_type,
            SymbolEntryType::ClassDefinition | SymbolEntryType::ClassDeclaration
        ) {
            Some(format!("class {}", self.symbol))
        } else if matches!(
            self.entry_type,
            SymbolEntryType::EnumDeclaration | SymbolEntryType::EnumDefinition
        ) {
            Some(format!("enum {}", self.symbol))
        } else {
            self.type_string.clone()
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct LocalSymbolEntry {
    pub symbol: String,
    pub span: SourceSpan,
    pub valid_span: SourceSpan,
    pub type_string: Option<String>,
    pub type_name: Option<TypeReference>,
    pub infer_types_from: Vec<String>,
    pub entry_type: SymbolEntryType,
    pub is_param_declaration: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SymbolReference {
    pub symbol: String,
    pub span: SourceSpan,
    pub scope: Option<String>,
    pub local_reference: bool,
    pub is_call: bool,
    pub is_field: bool,
    pub is_namespace: bool,
}

impl SymbolReference {
    pub fn with_scope(&self) -> String {
        match &self.scope {
            Some(s) => format!("{}::{}", s, self.symbol),
            None => self.symbol.clone(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum ResolvedSymbol {
    Global((Arc<SourceFile>, SymbolEntry)),
    Local((Arc<SourceFile>, LocalSymbolEntry)),
    Include(IncludeFile),
}

pub fn scope_match(symbol_scope: &Option<String>, query_scope: &Option<String>) -> bool {
    if let Some(query_scope) = query_scope {
        if let Some(symbol_scope) = symbol_scope {
            symbol_scope == query_scope || symbol_scope.ends_with(&("::".to_owned() + query_scope))
        } else {
            false
        }
    } else {
        true
    }
}

pub fn symbol_needs_scope(entry_type: &SymbolEntryType) -> bool {
    return matches!(
        entry_type,
        SymbolEntryType::ClassDeclaration
            | SymbolEntryType::ClassDefinition
            | SymbolEntryType::FunctionDeclaration
            | SymbolEntryType::FunctionDefinition
            | SymbolEntryType::EnumDeclaration
            | SymbolEntryType::EnumDefinition
            | SymbolEntryType::EnumValue
            | SymbolEntryType::VariableDeclaration
            | SymbolEntryType::VariableDefinition
            | SymbolEntryType::TypeAlias
    );
}

pub fn get_scope_suggestion(
    visible_scopes: &[&ScopeSpan],
    symbol_scopes: &[Option<String>],
) -> Option<String> {
    let mut best_suggestion: Option<String> = None;
    for symbol_scope in symbol_scopes {
        if let Some(symbol_scope) = symbol_scope {
            for visible_scope in visible_scopes {
                if &visible_scope.scope == symbol_scope {
                    return None;
                }
                let scope_prefix = visible_scope.scope.clone() + "::";
                let suggestion = symbol_scope
                    .strip_prefix(&scope_prefix)
                    .unwrap_or(symbol_scope);
                if let Some(current_suggestion) = &best_suggestion {
                    if suggestion.len() < current_suggestion.len() {
                        best_suggestion = Some(suggestion.to_owned())
                    }
                } else {
                    best_suggestion = Some(suggestion.to_owned())
                }
            }

            if visible_scopes.is_empty() && best_suggestion.is_none() {
                best_suggestion = Some(symbol_scope.clone());
            }
        } else {
            return None;
        }
    }
    best_suggestion
}
