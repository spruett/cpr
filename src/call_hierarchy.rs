use std::collections::HashSet;

use anyhow::{bail, Result};
use lsp_types::{
    CallHierarchyIncomingCall, CallHierarchyItem, CallHierarchyOutgoingCall, Position, Range,
    SymbolKind, Url,
};

use crate::indexer::Indexer;
use crate::progress::ProgressItem;
use crate::span::SourceSpan;
use crate::symbols::{SymbolEntry, SymbolEntryType, SymbolReference};
use crate::workspace::{WorkspaceManager, WorkspaceSymbolQuery};

fn to_call_hierarchy_item(filename: &str, symbol: &SymbolEntry) -> CallHierarchyItem {
    CallHierarchyItem {
        name: symbol.symbol.clone(),
        kind: symbol.entry_type.to_symbol_kind(),
        detail: symbol.type_string(),
        uri: Url::from_file_path(filename).unwrap(),
        range: symbol.span_with_params.as_ref().unwrap().to_range(),
        selection_range: symbol.span.to_range(),
        tags: None,
        data: None,
    }
}

fn range_contains(range: &Range, span: &SourceSpan) -> bool {
    ((range.start.line as usize, range.start.character as usize)
        <= (span.begin.row, span.begin.column))
        && ((range.end.line as usize, range.end.character as usize)
            >= (span.end.row, span.end.column))
}

fn is_call_defn(entry: &SymbolEntry) -> bool {
    matches!(
        entry.entry_type,
        SymbolEntryType::ClassConstructorDefinition
            | SymbolEntryType::FunctionDefinition
            | SymbolEntryType::MethodDefinition
    )
}

fn is_call_decl(entry: &SymbolEntry) -> bool {
    matches!(
        entry.entry_type,
        SymbolEntryType::ClassConstructorDeclaration
            | SymbolEntryType::FunctionDeclaration
            | SymbolEntryType::MethodDeclaration
    )
}

async fn defn_containing_ref(filename: String, reference: &SymbolReference) -> Option<SymbolEntry> {
    let index = Indexer::get().get_index(filename).await.ok()?;

    for (_, symbols) in index.document().symbols.iter_all() {
        for symbol in symbols {
            if is_call_defn(symbol) {
                if let Some(span) = &symbol.span_with_params {
                    if span.contains_span(&reference.span) {
                        return Some(symbol.clone());
                    }
                }
            }
        }
    }
    None
}

async fn class_bases(item: CallHierarchyItem) -> Result<Vec<CallHierarchyOutgoingCall>> {
    let filename = item.uri.path();
    let symbols = Indexer::get()
        .get_index(filename.to_owned())
        .await?
        .document()
        .find_symbol(&item.name)
        .into_iter()
        .find(|symbol| range_contains(&item.range, &symbol.span));

    let mut bases = Vec::new();
    if let Some(symbol) = symbols {
        for base_class in symbol.base_classes {
            let resolved = Indexer::get()
                .resolve_symbol(filename.to_owned(), base_class.symbol, ProgressItem::noop())
                .await;
            for (file, resolved) in resolved {
                if matches!(
                    resolved.entry_type,
                    SymbolEntryType::ClassDefinition | SymbolEntryType::ClassDeclaration
                ) {
                    bases.push(CallHierarchyOutgoingCall {
                        to: to_call_hierarchy_item(&file.filename, &resolved),
                        from_ranges: vec![base_class.span.to_range()],
                    });
                }
            }
        }
    }

    Ok(bases)
}

pub async fn prepare_call_hierarchy(
    filename: String,
    position: Position,
) -> Result<Vec<CallHierarchyItem>> {
    let index = Indexer::get().get_index(filename.clone()).await?;

    let mut items = Vec::new();
    for (_, symbols) in index.document().symbols.iter_all() {
        for symbol in symbols {
            if is_call_defn(symbol) {
                if let Some(span) = &symbol.span_with_params {
                    if span.contains(&position) {
                        items.push(to_call_hierarchy_item(&filename, symbol));
                    }
                }
            } else if symbol.entry_type == SymbolEntryType::ClassDefinition
                && symbol.span.contains(&position)
            {
                items.push(to_call_hierarchy_item(&filename, symbol));
            }
        }
    }

    Ok(items)
}

pub async fn call_hierarchy_outgoing(
    from: CallHierarchyItem,
) -> Result<Vec<CallHierarchyOutgoingCall>> {
    if from.kind == SymbolKind::Class {
        return class_bases(from).await;
    }

    let filename = from.uri.path().to_owned();
    let index = Indexer::get().get_index(filename.clone()).await?;

    let mut refs_within_range = Vec::new();
    for (_, refs) in index.document().symbol_references.iter_all() {
        for reference in refs {
            if reference.local_reference || !reference.is_call {
                continue;
            }
            if range_contains(&from.range, &reference.span) {
                refs_within_range.push(reference.clone());
            }
        }
    }

    let mut outgoing_calls = Vec::new();
    let mut dedup = HashSet::new();
    for reference in refs_within_range {
        if !dedup.insert(reference.with_scope()) {
            continue;
        }
        let resolved = Indexer::get()
            .resolve_symbol(
                filename.clone(),
                reference.symbol.clone(),
                ProgressItem::noop(),
            )
            .await;
        let mut defns = resolved
            .iter()
            .filter(|(_, entry)| is_call_defn(entry))
            .cloned()
            .collect::<Vec<_>>();
        if defns.is_empty() {
            let decls = resolved
                .iter()
                .filter(|(_, entry)| is_call_decl(entry))
                .collect::<Vec<_>>();

            for (file, decl) in decls {
                defns.extend(
                    Indexer::get()
                        .resolve_symbol(
                            file.filename.clone(),
                            decl.symbol.clone(),
                            ProgressItem::noop(),
                        )
                        .await
                        .into_iter(),
                );
            }
        }
        for (file, symbol) in defns {
            if symbol.span_with_params.is_some() {
                outgoing_calls.push(CallHierarchyOutgoingCall {
                    to: to_call_hierarchy_item(&file.filename, &symbol),
                    from_ranges: vec![reference.span.to_range()],
                });
            }
        }
    }
    Ok(outgoing_calls)
}

pub async fn call_hierarchy_incoming(
    to: CallHierarchyItem,
) -> Result<Vec<CallHierarchyIncomingCall>> {
    let filename = to.uri.path().to_owned();
    let workspace = match WorkspaceManager::new().get_workspace(&filename) {
        Some(w) => w,
        None => bail!("no workspace found that indexes {}", filename),
    };

    let refs = workspace
        .find_all_references(&WorkspaceSymbolQuery {
            symbol: to.name,
            scope: None,
            prefix_match: false,
        })
        .await;

    let mut incoming_calls = Vec::new();
    for (filename, refs) in refs {
        for reference in refs {
            if reference.is_call {
                if let Some(defn) = defn_containing_ref(filename.clone(), &reference).await {
                    incoming_calls.push(CallHierarchyIncomingCall {
                        from: to_call_hierarchy_item(&filename, &defn),
                        from_ranges: vec![reference.span.to_range()],
                    });
                }
            }
        }
    }

    Ok(incoming_calls)
}
