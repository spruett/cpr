use lsp_types::Position;
use lsp_types::Range;
use std::borrow::Cow;
use tree_sitter::{Node, Point};

#[derive(Default, Clone, Debug, PartialEq, Eq, Hash)]
pub struct SourceFile {
    pub filename: String,
    pub content: String,
}

impl SourceFile {
    pub fn new(filename: String, content: String) -> Self {
        Self { filename, content }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct SourceSpan {
    pub begin: Point,
    pub end: Point,
    pub begin_byte: usize,
    pub end_byte: usize,
}

impl SourceSpan {
    pub fn to_range(&self) -> Range {
        Range::new(
            Position::new(self.begin.row as u32, self.begin.column as u32),
            Position::new(self.end.row as u32, self.end.column as u32),
        )
    }

    pub fn from_node(node: &Node) -> Self {
        Self {
            begin: node.range().start_point,
            end: node.range().end_point,
            begin_byte: node.range().start_byte,
            end_byte: node.range().end_byte,
        }
    }

    pub fn content<'a>(&self, source: &'a str) -> Cow<'a, str> {
        let bytes = &source.as_bytes()[self.begin_byte..self.end_byte];
        String::from_utf8_lossy(bytes)
    }

    pub fn contains(&self, position: &Position) -> bool {
        (self.begin.row, self.begin.column) <= (position.line as usize, position.character as usize)
            && (self.end.row, self.end.column)
                >= (position.line as usize, position.character as usize)
    }

    pub fn contains_point(&self, point: Point) -> bool {
        (self.begin.row, self.begin.column) <= (point.row, point.column)
            && (self.end.row, self.end.column) >= (point.row, point.column)
    }

    pub fn contains_span(&self, span: &SourceSpan) -> bool {
        self.contains_point(span.begin) && self.contains_point(span.end)
    }

    pub fn in_range(&self, range: &Range) -> bool {
        fn point_in_range(point: &Point, range: &Range) -> bool {
            let point = (point.row, point.column);
            let begin = (range.start.line as usize, range.start.character as usize);
            let end = (range.end.line as usize, range.end.character as usize);
            point >= begin && point <= end
        }
        point_in_range(&self.begin, range) && point_in_range(&self.end, range)
    }
}
