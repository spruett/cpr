use crate::async_cache::{AsyncCache, TimedAsyncCache};
use crate::config::Config;
use crate::document::{parse_file, ParsedFile};
use crate::include_resolver::{is_header, matching_main_header, matching_src};
use crate::progress::ProgressItem;
use crate::symbols::{IncludeFile, LocalSymbolEntry, SymbolAtPoint, SymbolEntry, SymbolReference};
use crate::{
    file_cache::FileCache, include_resolver::IncludeResolver, span::SourceFile,
    task_tracker::TaskTracker,
};
use anyhow::Result;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use std::cmp::min;
use std::collections::{HashMap, HashSet};
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use tokio::sync::oneshot::channel;

async fn run_cpu<R: 'static + Send, F: 'static + FnOnce() -> R + Send>(func: F) -> R {
    let (tx, rx) = channel();
    rayon::spawn_fifo(|| {
        let _result = tx.send(func());
    });
    rx.await.unwrap()
}

pub type IncludeDepth = usize;

#[derive(Hash, PartialEq, Eq, Clone)]
pub struct RankedItem<T, R> {
    pub value: T,
    pub rank: R,
}
impl<T, R> RankedItem<T, R> {
    pub fn new(value: T, rank: R) -> RankedItem<T, R> {
        Self { value, rank }
    }
}

pub struct Indexer {
    include_resolver: IncludeResolver,
    file_cache: FileCache,
    items: AsyncCache<String, Arc<dyn ParsedFile>>,
    transitive_include_cache: TimedAsyncCache<String, HashSet<RankedItem<String, IncludeDepth>>>,
}

impl Indexer {
    pub fn new() -> Self {
        Self {
            include_resolver: IncludeResolver::from_config(),
            file_cache: FileCache::new(),
            items: AsyncCache::new(),
            transitive_include_cache: TimedAsyncCache::new(),
        }
    }

    pub fn update_from_editor(&self, filename: String, content: String) {
        self.transitive_include_cache.invalidate(&filename);
        self.file_cache.set_from_editor(filename.clone(), content);
        self.items.invalidate(&filename);
        self.include_resolver.clear_cache();
    }
    pub fn release_from_editor(&self, filename: String) {
        self.transitive_include_cache.invalidate(&filename);
        self.file_cache.release_from_editor(&filename);
        self.items.invalidate(&filename);
        self.include_resolver.clear_cache();
    }

    pub async fn get_file_content(&self, filename: String) -> Result<Arc<SourceFile>> {
        self.file_cache.get_or_fetch(filename).await
    }

    pub async fn get_symbol_at_point(
        &self,
        filename: String,
        line: usize,
        column: usize,
    ) -> SymbolAtPoint {
        let index = match self.get_index(filename).await {
            Ok(s) => s,
            Err(_) => return SymbolAtPoint::Missing,
        };
        index.get_symbol_at_point(line, column)
    }

    pub async fn resolve_file_symbols(&self, filename: String) -> Result<Vec<SymbolEntry>> {
        let index = self.get_index(filename).await?;
        Ok(index
            .document()
            .symbols
            .iter_all()
            .flat_map(|(_name, entries)| entries.iter().cloned())
            .collect())
    }

    pub async fn resolve_symbol(
        self: &Arc<Self>,
        filename: String,
        symbol: String,
        progress: Arc<ProgressItem>,
    ) -> Vec<(Arc<SourceFile>, SymbolEntry)> {
        let indices = self.resolve_all_indexes(filename, progress).await;

        let mut results = Vec::new();
        for ranked_index in indices {
            let symbols = ranked_index.value.document().find_symbol(&symbol);
            for symbol in symbols {
                results.push((ranked_index.value.document().source.clone(), symbol));
            }
        }
        results
    }

    pub async fn resolve_local_symbol(
        self: &Arc<Self>,
        filename: String,
        symbol: &str,
    ) -> Vec<LocalSymbolEntry> {
        let index = match self.get_index(filename).await {
            Ok(index) => index,
            Err(_) => return Vec::new(),
        };
        index.document().find_local_symbol(symbol)
    }

    pub async fn warm_cache(
        self: &Arc<Self>,
        filename: String,
        progress: Arc<ProgressItem>,
    ) -> Result<Arc<dyn ParsedFile>> {
        let _ = self.resolve_all_indexes(filename.clone(), progress).await;
        self.get_index(filename).await
    }

    pub fn resolve_include(&self, from: &str, include: &IncludeFile) -> Vec<String> {
        self.include_resolver.resolve(from, include)
    }

    pub async fn get_all_symbols(
        self: &Arc<Self>,
        filename: String,
        progress: Arc<ProgressItem>,
    ) -> Vec<RankedItem<(Arc<SourceFile>, SymbolEntry), IncludeDepth>> {
        let indexes = self.resolve_all_indexes(filename, progress).await;
        indexes
            .into_par_iter()
            .flat_map(|RankedItem { rank, value: index }| {
                index
                    .document()
                    .symbols
                    .iter_all()
                    .flat_map(|(_name, entries)| entries.clone())
                    .map(|entry| RankedItem::new((index.document().source.clone(), entry), rank))
                    .collect::<Vec<_>>()
            })
            .collect()
    }

    pub async fn get_file_symbol_references(
        self: &Arc<Self>,
        filename: String,
    ) -> HashMap<String, usize> {
        let index = match self.get_index(filename).await {
            Ok(index) => index,
            Err(_) => return HashMap::new(),
        };
        index
            .document()
            .symbol_references
            .iter_all()
            .map(|(symbol, refs)| (symbol.clone(), refs.len()))
            .collect()
    }

    pub async fn direct_includes(self: &Arc<Self>, filename: String) -> HashSet<String> {
        let index = match self.get_index(filename.clone()).await {
            Ok(index) => index,
            Err(_) => return HashSet::new(),
        };
        let included_files = &index.document().includes;

        let mut includes = HashSet::new();
        for include in included_files {
            for resolved in self.include_resolver.resolve(&filename, &include.include) {
                includes.insert(resolved);
            }
        }
        includes
    }

    pub async fn search_file_symbol_references(
        self: &Arc<Self>,
        filename: String,
        symbol: &str,
    ) -> Vec<SymbolReference> {
        let index = match self.get_index(filename).await {
            Ok(index) => index,
            Err(_) => return Vec::new(),
        };
        index.document().find_symbol_references(symbol)
    }

    pub async fn resolve_all_indexes(
        self: &Arc<Self>,
        filename: String,
        progress: Arc<ProgressItem>,
    ) -> Vec<RankedItem<Arc<dyn ParsedFile>, IncludeDepth>> {
        let task_tracker = TaskTracker::new();
        #[derive(Default)]
        struct State {
            result: Vec<Arc<dyn ParsedFile>>,
            include_depths: HashMap<String, usize>,
        }
        let state = Arc::new(Mutex::new(State::default()));

        fn submit_task(
            indexer: Arc<Indexer>,
            filename: String,
            task_tracker: Arc<TaskTracker>,
            state: Arc<Mutex<State>>,
            progress: Arc<ProgressItem>,
            depth_remaining: usize,
        ) {
            let guard = task_tracker.start_task();
            {
                let mut locked = state.lock().unwrap();
                let depth = Config::get().include.max_include_depth - depth_remaining;
                match locked.include_depths.get_mut(&filename) {
                    Some(existing_depth) => {
                        *existing_depth = min(depth, *existing_depth);
                        return;
                    }
                    None => {
                        locked.include_depths.insert(filename.clone(), depth);
                    }
                }
            }
            progress.add_task();
            tokio::spawn(async move {
                let _guard = guard;
                let result = indexer.get_index(filename.clone()).await;
                let result = match result {
                    Ok(r) => r,
                    Err(e) => {
                        // TODO: better error handling
                        log::warn!("could not find include file: {}: {}", filename, e);
                        progress.complete_task();
                        return;
                    }
                };

                if depth_remaining > 0 {
                    for include in &result.document().includes {
                        let includes = indexer
                            .include_resolver
                            .resolve(&filename, &include.include);
                        if includes.is_empty() {
                            log::debug!(
                                "could not resolve include file: {:?} from {}",
                                include.include,
                                filename
                            );
                        }
                        for include in includes {
                            submit_task(
                                indexer.clone(),
                                include,
                                task_tracker.clone(),
                                state.clone(),
                                progress.clone(),
                                depth_remaining - 1,
                            );
                        }
                    }
                }
                progress.complete_task();
                {
                    let mut locked = state.lock().unwrap();
                    locked.result.push(result);
                }
            });
        }
        if is_header(&filename) {
            if let Some(matching_src) = matching_src(Path::new(&filename)) {
                submit_task(
                    self.clone(),
                    matching_src.to_str().unwrap().to_owned(),
                    task_tracker.clone(),
                    state.clone(),
                    progress.clone(),
                    0,
                );
            }
            if let Some(main_header) = matching_main_header(Path::new(&filename)) {
                submit_task(
                    self.clone(),
                    main_header.to_str().unwrap().to_owned(),
                    task_tracker.clone(),
                    state.clone(),
                    progress.clone(),
                    Config::get().include.max_include_depth,
                );
            }
        }

        submit_task(
            self.clone(),
            filename,
            task_tracker.clone(),
            state.clone(),
            progress.clone(),
            Config::get().include.max_include_depth,
        );
        task_tracker.wait().await;
        progress.complete();

        let locked = state.lock().unwrap();
        let result = locked.result.clone();
        result
            .into_iter()
            .map(|parsed| {
                let rank = locked
                    .include_depths
                    .get(&parsed.document().source.filename)
                    .cloned()
                    .unwrap_or_default();
                RankedItem::new(parsed, rank)
            })
            .collect()
    }

    pub async fn get_index(&self, filename: String) -> Result<Arc<dyn ParsedFile>> {
        let source_file = self.file_cache.get_or_fetch(filename.clone()).await?;
        let index = self
            .items
            .get(&filename, async {
                run_cpu(move || parse_file(source_file)).await
            })
            .await;
        Ok(index)
    }

    pub async fn transitive_includes(
        self: &Arc<Self>,
        filename: String,
    ) -> HashSet<RankedItem<String, IncludeDepth>> {
        let filename_clone = filename.clone();
        let fut = async move {
            let all_indices = self
                .resolve_all_indexes(filename_clone, ProgressItem::noop())
                .await;
            all_indices
                .into_iter()
                .map(|ranked_index| {
                    RankedItem::new(
                        ranked_index.value.document().source.filename.clone(),
                        ranked_index.rank,
                    )
                })
                .collect()
        };
        self.transitive_include_cache
            .get(&filename, fut, Duration::from_secs(60))
            .await
    }

    pub fn get() -> &'static Arc<Indexer> {
        lazy_static::lazy_static! {
            static ref INDEXER: Arc<Indexer> = Arc::new(Indexer::new());
        }
        &*INDEXER
    }
}
