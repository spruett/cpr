use std::{collections::HashSet, process::Stdio, sync::Arc, time::Duration};

use anyhow::Result;
use lazy_static::lazy_static;

use crate::{async_cache::TimedAsyncCache, config::Config};

fn query_cache() -> &'static TimedAsyncCache<(String, String), Arc<Result<String>>> {
    lazy_static! {
        static ref CACHE: TimedAsyncCache<(String, String), Arc<Result<String>>> =
            TimedAsyncCache::new();
    }
    &*CACHE
}

async fn run_buck_query(workdir: String, query: String) -> Result<String> {
    let executable = Config::get()
        .buck
        .path
        .as_ref()
        .cloned()
        .unwrap_or_else(|| "buck".to_owned());
    let subprocess = tokio::process::Command::new(executable)
        .args(vec!["query".to_owned(), query])
        .stdout(Stdio::piped())
        .current_dir(&workdir)
        .spawn()?;
    let output = subprocess.wait_with_output().await?;
    anyhow::ensure!(output.status.success(), "buck query failed");
    Ok(String::from_utf8(output.stdout)?)
}

async fn buck_query(workdir: String, query: String) -> Arc<Result<String>> {
    let cache_key = (workdir.clone(), query.clone());
    let fut = async { Arc::new(run_buck_query(workdir, query).await) };
    query_cache()
        .get(&cache_key, fut, Duration::from_secs(300))
        .await
}

pub async fn get_file_owner(workdir: String, relative_filename: &str) -> Option<String> {
    match &*buck_query(workdir, format!("owner({})", relative_filename)).await {
        Ok(owner) => {
            if owner.is_empty() {
                None
            } else {
                Some(owner.trim().to_owned())
            }
        }
        Err(e) => {
            log::error!("failed buck query: {}", e);
            None
        }
    }
}

pub async fn get_target_deps(workdir: String, target: String) -> HashSet<String> {
    let output = &*buck_query(workdir, format!("deps({})", target)).await;
    let output = match output {
        Ok(output) => {
            if output.is_empty() {
                return HashSet::new();
            } else {
                output
            }
        }
        Err(e) => {
            log::error!("failed buck query: {}", e);
            return HashSet::new();
        }
    };
    output.lines().map(|line| line.to_owned()).collect()
}
