# "Good Enough" Language Server

This is an experimental implementation of the 
[Language Server Protocol](https://microsoft.github.io/language-server-protocol/)
for C++ projects. It does not try to fully understand C++ semantics, but rather
tries to do as much as possible as quickly as possible with some heuristics.

Internally it uses `tree-sitter` to parse individual files, extracts symbol
information from the (potentially incorrect, given C++'s parsing complexity)
AST into a per-file table.

This project was developed mostly to work on codebases where typical C++
language servers (such as `clangd`) struggle to remain interactive due
to code size. By parsing headers separately instead of preproccessing
into one translation unit a lot of time can be saved. Similarly,
using `tree-sitter` parses of a single file can be much quicker than
a full, correct C++ parser.

## Features

Features are heavily driven by things I wanted to use on a large C++ project,
but the following work (up to the limits of not being a full C++ compiler/parser):
 - Go to declaration and go to definition
 - Find all references
 - Auto completion, with heuristics for scoping and types
 - Simple renaming
 - Documentation help (hover info and parameter hints)
 - Formatting (via `clang-format`)
 - Semantic highlighting
 - Symbol search
 - Call hierarchy

## Development

`cpr` is a typical Rust CLI which runs the LSP over stdin and stdout.
Unit tests can be run with `cargo test` and the application can be
run with `cargo run`.
